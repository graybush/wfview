[33mcommit 5f6cb47fdfe5a5e7235376783c42f9343163bbb8[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Merge: 8c68031 39dacf5
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jan 2 14:42:10 2023 +0100

    Merge branch 'QT6.2'

[33mcommit 39dacf54f2dabc30da73c5049a56ea31d9f29421[m[33m ([m[1;31morigin/QT6.2[m[33m, [m[1;32mQT6.2[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jan 2 13:39:59 2023 +0000

    Fix wfserver device detection

[33mcommit 8c680316f15c3f01d4df84d194b9b036225819ab[m
Merge: c490ec0 e6a5152
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jan 2 13:43:23 2023 +0100

    Merge remote-tracking branch 'origin/QT6.2'

[33mcommit c490ec07312777abbfcb86a1a1794fc58ac6f807[m
Merge: b3f76fc aabe690
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jan 2 13:43:15 2023 +0100

    Merge remote-tracking branch 'origin/audio-fixes'

[33mcommit e6a515273428f6905d066d31479fa394009010a5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jan 2 12:38:05 2023 +0000

    Prevent memory leak on exit

[33mcommit b3f76fce67e25898ea1a371baf531f5bee8f5989[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 0d2d9ba 8ead34b
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Mon Jan 2 08:12:00 2023 +0000

    Merge branch 'standardHFfix' into 'master'
    
    fixed missing 160m band
    
    See merge request eliggett/wfview!14

[33mcommit 436428b3a9d31d4e8d100e8de16885bacdf2df55[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jan 2 01:11:25 2023 +0000

    Support hotplug audio on QT6+

[33mcommit a327b2690e12297a68828d3baa792334bb9acf40[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jan 2 00:21:37 2023 +0000

    Fix resampler compiler warnings.

[33mcommit 7348fc2ed0b973c4c32bbd300419c0f15b3700e0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 21:40:46 2023 +0000

    Remove Exception from RT audio as it doesn't exist in newer versions of RTAudio

[33mcommit ff6dd19e5336382d1533a453cd6dacbd80797d08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 20:56:13 2023 +0000

    Tidy-up device enumeration for Qt audio

[33mcommit db0112c8a355e6a61ed39d7c4f1c1765cbff1a52[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 20:51:52 2023 +0000

    Log enumeration of audio devices

[33mcommit 6199c648c172584de76c76ea2b169f83713c9f5c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 20:28:59 2023 +0000

    Fix some old warnings

[33mcommit 32528a02b8cf424070734d1227729082443542a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 16:35:47 2023 +0000

    Fix codec selection issue in both QT5 and 6

[33mcommit 91ce6040e0d22d4930dfc34b02c2a475acd2e91c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 1 16:17:48 2023 +0000

    Make sure compilation with QT5 still works

[33mcommit 8ead34b253d5885ec3aa54a1b6bed8c4b363ba5a[m
Author: Dawid SQ6EMM <dawszy@arhea.pl>
Date:   Sat Dec 31 02:02:16 2022 +0100

    fixed missing 160m band

[33mcommit 891fd68a524e656792133eb3482ecf53c1cf66c6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 29 16:06:42 2022 +0000

    Fixed after merge

[33mcommit f4c455396a486fbcfb0eb36784f40f88109196da[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 29 15:26:41 2022 +0000

    Merge branch 'audio-fixes' into QT6.2

[33mcommit a89af116e952f81d90c54c0972a20df274198574[m
Merge: d1ffbe8 aabe690
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 29 14:16:22 2022 +0000

    Merge updated

[33mcommit aabe690bae6aa8f9e1f7e0f427aea674a15c08a8[m[33m ([m[1;31morigin/audio-fixes[m[33m, [m[1;32maudio-fixes[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 29 09:21:25 2022 +0000

    Fix compile errors/warnings in Linux

[33mcommit 7dfdc5e3e2434766a787214cba273a35be42bc62[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 28 16:57:05 2022 +0000

    Move audio device selection to dedicated class

[33mcommit 0d2d9ba23ee002e44b778a22c17b6965bad91221[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Dec 28 09:57:09 2022 +0100

    bumped to v1.54, see WHATSNEW

[33mcommit 28d3c55e2fb30ded348e636b56683ceef828ca68[m
Merge: ac3894f 093e6e7
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Dec 28 09:54:37 2022 +0100

    Merge branch 'termbinfail'

[33mcommit 093e6e799cbf5d6a6c85e02fef9a691af769619a[m[33m ([m[1;31morigin/termbinfail[m[33m, [m[1;32mtermbinfail[m[33m)[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Dec 27 19:37:12 2022 -0800

    Fixed termbin button issue when connection fails. Cleaned up some
    logging.

[33mcommit ac3894f897322f9fbec23833aca6334175b9211d[m
Merge: 8d3406a e8142a1
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Dec 20 11:49:07 2022 +0100

    Merge branch 'server-fixes'

[33mcommit e8142a11bab43197039abc0b38f7d454064e4577[m[33m ([m[1;31morigin/server-fixes[m[33m, [m[1;32mserver-fixes[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Dec 19 20:04:07 2022 +0000

    DIsable tcpserver on wfserver

[33mcommit 3bf095020c31f9a3ce96d58386b8ccbe0e0ab921[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Dec 19 19:41:23 2022 +0000

    Fix serial port detection.

[33mcommit e56294406a55ccf86413a6006f7e38ff4b141822[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Dec 19 19:26:51 2022 +0000

    Swap logfile/settings file on servermain

[33mcommit 8d3406a8ae33ee5b994a1cf5ab90089fe5dbaf12[m
Merge: 0b39b91 f6b1428
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Dec 3 12:32:41 2022 +0100

    Merge branch 'resetpeaks'

[33mcommit f6b1428267ce8da15e9c42ff6e205e6038e7f3a2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Dec 2 13:32:02 2022 -0800

    Now tracking the plasma buffer user-desired size properly.

[33mcommit 82dfb9982239c8762b8e9475cb4b88dca0d4d7e6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Dec 2 12:50:05 2022 -0800

    A little cleaner on the plasma mod.

[33mcommit 8cfb3f70e5d6008024332f4c5804f60da7f45e70[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Dec 2 12:38:08 2022 -0800

    Added clearing to the plasma underlay display.

[33mcommit 0b39b91665edea66105f937b2ccc3263e2e8f33b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 1 13:16:05 2022 +0000

    PA stop stream if latency exceeded and start again when data is received.

[33mcommit 65ba69822a3d548b000d7729e14d2ef9b3ba3362[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 1 13:08:29 2022 +0000

    Attempt to fix PortAudio issue

[33mcommit f91dea47b518462deafac02991fbc8e2b3375e20[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 1 12:32:10 2022 +0000

    Stop log warning on wfserver

[33mcommit 9367d605f4ba0fa07ab2e9eced0f10271634c893[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Nov 29 10:30:53 2022 +0100

    fixed WHATSNEW formatting

[33mcommit 508f8700623661d42d8febc76bf8c2872e8cd08b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Nov 29 10:29:10 2022 +0100

    CHANGELOG and WHATSNEW updated for 1.53

[33mcommit 06e4567b124964d56e2b8310f56cd490e378eff6[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Nov 29 09:40:07 2022 +0100

    bumped 1.53

[33mcommit 4c76cf6320750095ce34feb17bd7633a65972556[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 28 09:44:42 2022 +0000

    Add missing prefs

[33mcommit 0a1a7b23361dd84481cdda62ce55a0950feb13ce[m
Merge: 81f9e9b 8a1347f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 28 09:27:53 2022 +0000

    Merge remote-tracking branch 'origin/pollprefs' into various-fixes

[33mcommit 8a1347f17c69f1665a0fffb4c21ec1becfaad0be[m[33m ([m[1;31morigin/pollprefs[m[33m, [m[1;32mpollprefs[m[33m)[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 26 21:45:27 2022 -0800

    Added some tooltips that nobody will notice.

[33mcommit 4f6da002064970928305efc86a90f68424599bd0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 26 21:43:47 2022 -0800

    Added polling preferences and changed UI elements to radio buttons with
    spin box.

[33mcommit 39caf4190543abb737798492515e825158feae2c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 26 20:53:12 2022 -0800

    Added program version number to the settings file. Currently it is merely
    written to and not used.

[33mcommit b25c8a93f3f0c7fbb860ed645bf62e0f159bc2f3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 26 20:45:32 2022 -0800

    Moved preferences to prefs.h, which should make it easier to add new
    preferences. Moved custom types (as it seemed useful here and there) to
    wfviewtypes.h. Let's use the wfviewtypes.h file for any kind of datatype
    which more than one module can make use of.

[33mcommit 81f9e9bfcf6dbc56a86aeb6d729b0957638f05e9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 26 19:10:59 2022 +0000

    Update some rig definitions to work with new band struct.

[33mcommit 7138ef69ea8d2dbc8c7054c8afd57dc8b10c1487[m
Merge: 1fe1916 afcf087
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 26 19:02:11 2022 +0000

    Merge branch 'master' into various-fixes

[33mcommit 1fe19166f34c29c51fc51050570a1ae2b3204e38[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 26 19:01:16 2022 +0000

    Add some more commands

[33mcommit afcf087a70ea2fa224294b0bc170271bc109e1d6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 24 11:43:27 2022 -0800

    Fixed color preset zero issue.

[33mcommit e1e9167622d98b78224f82f19cc4eb9e90b5973a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 23:26:11 2022 -0800

    Fixed unusual set of conditions in which manual CI-V address would fail
    to identify the connected radio. The fix is to continually poll the RigID
    until one is received. Radios that don't support RigID will need to
    check both boxes in wfview (use rig as model number) to manually ID the
    radio.

[33mcommit 9a63a6c4c439d46d089e8ddc74caa0bb64f28e22[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 22:45:29 2022 -0800

    Fixed minor typo in debug text.

[33mcommit bb5ae0323527e7d5f398d95b4113b78e1101d16e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 21:56:36 2022 -0800

    Cleaned up rigidentities

[33mcommit 45244c7c640b50d7e03055107df442f8fb12172d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 20:44:32 2022 -0800

    Fixed annoying indentation issue.

[33mcommit a5ac42fe57bc1860614ffa5d655fc2b02425de5b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 16:59:10 2022 -0800

    Fixed broken RTS preference. Added preliminary (and untested) support
    for the IC-703, 737, 738, and 756.

[33mcommit 9da6f770780d578c3f742990e00e53e3ec817ac3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Nov 23 13:36:13 2022 -0800

    Fixed issue causing the current color preset to not be load (previously
    preset zero was always used).

[33mcommit b57e4ebd769bf551372e0432da02ea8bcbb13a82[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Nov 1 12:16:55 2022 +0100

    added libs for suse builds; it also builds on leap 15.5

[33mcommit a1252eec1ed1be0df10379fe37ecb30443249bba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 31 13:33:47 2022 +0000

    Fix passband warnings

[33mcommit 29060eb43e8ab0b962fc53c7832775e1a30dd02b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 31 12:39:27 2022 +0000

    Add skimmer support (setting not saved)

[33mcommit 209c5a9b0bd5bf29499517cfac37ac8c1c659db2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 31 12:15:56 2022 +0000

    Key KEYSPD

[33mcommit b46dfb8f3e8163682673a5320e141ddf83c88cd8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 31 12:15:42 2022 +0000

    Add AR Cluster support

[33mcommit d47b9be7e1dfbd2e18fb7a0fc11344f2dae68331[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 31 12:03:04 2022 +0000

    Add more commands to rigctld

[33mcommit f7c532007ea3f4c1c19b434537fc82e961c3fa99[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Oct 29 15:12:30 2022 +0100

    Make bandType a struct containing frequency and default SSB mode

[33mcommit 429aba7d68bee1fd8b02306f294b25e5e6284736[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Oct 29 00:20:27 2022 +0100

    Remove spot display processing timer

[33mcommit e49e00c3adf6b28ff518e48c529e503fa5fcbaeb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Oct 29 00:18:04 2022 +0100

    Silly error in setting default cluster

[33mcommit bf340e7790a7bebf481ee0696c14d081c97e64a2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Oct 28 20:04:32 2022 +0100

    Send bye when disconnecting from cluster

[33mcommit 3a07830d47b0b8d8a3b2035e0a54a936c17187d6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Oct 28 20:04:14 2022 +0100

    Add passband to rigctld and allow setting

[33mcommit 103dc9c86adff640722557f2bf770cab06ac1575[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Oct 25 08:25:02 2022 +0200

    combobox resizing fix

[33mcommit 034ec9066236c2fbd00df3ef0ba2704d51d853aa[m[33m ([m[1;31morigin/combobox[m[33m, [m[1;32mcombobox[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 24 00:09:18 2022 +0100

    Stop it cutting off some devices

[33mcommit ce9ab6d7ff4c5a5a1d052c9f3496cf5c56508268[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 23 23:45:53 2022 +0100

    Resize according to boundingrect

[33mcommit e2338edde6ee75243d4cc8b64b4f624e50c51eba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 23 22:58:23 2022 +0100

    Resize based on font size

[33mcommit 16dd595f52ef122e694b89d6822e2a2a6e6f8baa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 23 22:45:11 2022 +0100

    Try new universal resize code

[33mcommit 6609aa58d9fadbc6d4b6df2689a1d509069a5d88[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Oct 22 14:48:44 2022 +0200

    added qt xml dev lib for suse

[33mcommit ce12135c4a89fe347c5cc4b05fbb41b24d042726[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Oct 14 17:48:35 2022 +0200

    bands page reorganized

[33mcommit faf5caefa3a47a4d5b79618f80198e1f3f29c302[m
Merge: f8cef53 e15496e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Oct 11 15:04:22 2022 +0200

    Merge branch 'cluster'

[33mcommit e15496e0b67093dde01e1b90083f28c9d15ccd8a[m[33m ([m[1;31morigin/cluster[m[33m, [m[1;32mcluster[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Oct 11 13:17:23 2022 +0100

    Disable click/drag tuning by default

[33mcommit f8cef53a7dc1edcd658ab18128c9d893014eefc8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Oct 10 17:11:05 2022 +0200

    added cluster support

[33mcommit f4e25d22739a45e3d98f8cf9f2bdf15ff3ddef9a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Oct 10 12:55:12 2022 +0100

    Bit more tidying of spotting code

[33mcommit 3ec54f1ebb2ad4e295d2e035f970a8fa3870f700[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 9 20:24:01 2022 +0100

    make spots appear at the actual top of wf

[33mcommit 6862009e940e0580b47b734c4eadb87bf76a067d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 9 14:35:24 2022 +0100

    Add sendOutput() back to tcp cluster

[33mcommit b7bcd58578715490fd97116e89ac7a8add8611ff[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 9 14:32:09 2022 +0100

    Add Pop-Out button for cluster screen

[33mcommit 22abc65849cbd20efdb16842f98de3f67d65343e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 9 14:23:07 2022 +0100

    Add cluster spot color picker

[33mcommit cc6e4d042a42084a6418a8d9e3e0293f22fc3a70[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Oct 8 01:09:41 2022 +0100

    Left click on spot to change freq

[33mcommit f7e2631ee3ef7f0514037854e34b7e874b92b4a0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Oct 8 00:55:07 2022 +0100

    Update wfmain.cpp

[33mcommit 62d9ec2442f3fbedbd7153fb5da3da3d8143b8d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Oct 6 11:17:46 2022 +0100

    Update wfmain.cpp

[33mcommit fa9cab1374cb7a454ced27aea58c15a1e9b4dd76[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Oct 6 11:12:55 2022 +0100

    Make spot window size to fit content.

[33mcommit 880a9c3da031a32f40d2ecf170f26bdc7d60c349[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Oct 6 11:02:22 2022 +0100

    Add right-click on spot to popup dialog

[33mcommit b110ce7fbbf050fd73b8525828cda679d47303f8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 14:26:42 2022 +0100

    Bit of tidying

[33mcommit 4d158f5078e34aac351bd0ec89467f0f4c18b465[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 13:43:07 2022 +0100

    Remove calls for QSqlDatabase when USESQL is not configured

[33mcommit f31aadd991aaa8cd918c465edcaadbdc848a8388[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 12:45:25 2022 +0100

    Add optional SQL in wfview.pro

[33mcommit b011165d5b08ec91f6932c78609297c412b514a8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 12:43:39 2022 +0100

    Make SQL db optional

[33mcommit 9fdc09c5be818a7ec36f8f475fa80bc61cd622b6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 11:38:24 2022 +0100

    Remove old code

[33mcommit 855438e8433988ce3b9f754b97e6698235c3cc39[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 11:05:29 2022 +0100

    Update query

[33mcommit 711b86e91b25985ccdd3d487400b03019a33bd04[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 5 11:03:15 2022 +0100

    Add memory sqlite db for cluster spots.

[33mcommit 9db695cc6302eabdbb719426916d138f693509a5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 19:06:17 2022 +0100

    Delete all spots if cluster disabled

[33mcommit 2d58ea2afbb23635f82d3b81d1cb0d2928d4cb08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 18:41:57 2022 +0100

    Fix crash when adding cluster server

[33mcommit 4db451050130f4f07aa7b9626c40442d3a61742c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 17:29:10 2022 +0100

    Comment unused line

[33mcommit 3b1efbd69ee3794213f0d4019733eabdd7829b12[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 17:26:21 2022 +0100

    Change default to isdefault!

[33mcommit e6713dc516a82dedeee4867133b08be8316f03c3[m
Merge: 7d14160 d5aeec6
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 17:07:26 2022 +0100

    Merge branch 'master' into cluster

[33mcommit 7d141608bf7b4603e489cbe33d9f17d20b72a363[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 30 17:05:42 2022 +0100

    Add TCP spot client

[33mcommit d5aeec61baf6f871a2218d8670f9c8eb8b763c3d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Sep 29 18:45:13 2022 +0200

    v1.51 with fixes,passband stuff

[33mcommit f48e65d1476ae20c2a714cd93ea0e7beb0c17fc3[m[33m ([m[1;31morigin/passband[m[33m, [m[1;32mpassband[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Sep 29 17:22:14 2022 +0100

    Fix for squished screen and bump version to 1.51

[33mcommit e7e7821f2b77cc59d6589f54eb42f661ea8ed6fe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Sep 29 17:18:48 2022 +0100

    Comment out some unused variables

[33mcommit dbc13a05335e7599285f4903da53020d3be3cb88[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Sep 29 17:17:51 2022 +0100

    Initial work on cluster spots.

[33mcommit cef827f7f82e28042e1cd9068aa865ec076b4721[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 28 17:53:28 2022 +0100

    Only request passband when there is a scope available

[33mcommit f7bde0fe649ddd606fabd72d60f4e8ffd706f5fc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 28 17:42:12 2022 +0100

    Change default passband colors.

[33mcommit bbc2613942a330cbc6af34d0457b501ec0ec1cc6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 28 17:34:15 2022 +0100

    Fix passband colorswatch

[33mcommit 0bb57a859da9ac94b9f72a76ba05b7f67c0dc996[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 27 10:22:27 2022 +0100

    See if this works better for color pickers?

[33mcommit 1193841103b54a1e1e1a21bcf0b4c79f2e268723[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 27 09:37:24 2022 +0100

    Ignore second VFO scope data (for now)

[33mcommit b3756391c13d9e4440b9fa5237dbb36910eaf722[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 27 08:57:22 2022 +0100

    Silently ignore server audio issues if server is not enabled.

[33mcommit 242d39ce39d2f0e0c00792066e41936aa47815cc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 27 08:44:55 2022 +0100

    Always use 8bit encoding for audio device names

[33mcommit 51878e5a3dcb741072910fe504377efa7f9bcd8c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 27 08:30:17 2022 +0100

    Make multimedia-plugins message only for Linux

[33mcommit b7af0d4eddd23ac6b77bfd0b264733cebdbd74ae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 25 18:22:48 2022 +0100

    Remove obsolete code

[33mcommit 518e1700ae3d24356c4d6dd21238441aa3e561ff[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 25 11:32:43 2022 +0100

    Remove logging of audio device realm

[33mcommit 87ebd0cdcc98c70f2e653cdfd3a5ca1082c9492a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 25 11:15:45 2022 +0100

    Fix default audio device selection and hopefully fix device language issue

[33mcommit 08b62244718d3fad731b034c2ef65c3fa338c8c8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Sep 26 21:22:02 2022 +0100

    Add PSK modes to IC-7610 and to passband.

[33mcommit ac0771f03418230a9389e00d07e967ebc901c853[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Sep 26 20:46:25 2022 +0100

    Add passband for FM mode

[33mcommit 65b4a9cc17e9e644793d2468577538ea8de68232[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Sep 26 11:53:56 2022 -0700

    Added click-drag tuning. Needs refinement but it's a start.

[33mcommit 0c530303c7409dab2150f2804d81032e102c0771[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Sep 26 12:10:41 2022 +0100

    Move to 3 columns

[33mcommit 9d9a817f978cecf367799419d7958e8dd7d60ae4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Sep 26 11:59:16 2022 +0100

    Allow changing of passband color

[33mcommit d8e244f33a6f420224b8e618c963b15beb4510a1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Sep 26 01:44:49 2022 +0100

    First look at a passband indicator

[33mcommit 70101487a3a9cc6938134df17704adfab83bda0e[m[33m ([m[1;33mtag: v1.50[m[33m)[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 22:14:49 2022 +0200

    ready for v1.50

[33mcommit e93086f6ac1e83bf027160f8759f7916ea5209d0[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 19:48:42 2022 +0200

    Delete quit confirmation checkbox

[33mcommit fe459fa7363c6457234e96e90281631227888e04[m
Merge: 32d214b 6e7d006
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 19:47:32 2022 +0200

    Merge branch 'logfix'

[33mcommit 6e7d006b0e7735ab4bcc4e59ede4f4198735e0a4[m[33m ([m[1;31morigin/logfix[m[33m, [m[1;32mlogfix[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 18:40:01 2022 +0100

    Delete quit confirmation checkbox

[33mcommit 32d214b24fcb82c70628348c05e8f692200ecc56[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 18:48:41 2022 +0200

    Remove redundant CL args

[33mcommit dfcbc3b2f6965dda7fbeeb0d78b4abff4d32de2b[m
Merge: 70fbd98 f838e22
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 18:47:17 2022 +0200

    Merge branch 'logfix'

[33mcommit f838e224981128fd7b5f14d150937cf107388cf4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 17:42:36 2022 +0100

    Remove redundant CL args

[33mcommit 70fbd989f125c130d53f16676f20457d5466e542[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 18:33:40 2022 +0200

    additional logging fixes

[33mcommit d9190a5060ae35ccff3bd824c3c606202f982331[m
Author: M0VSE <phil@m0vse.uk>
Date:   Fri Sep 23 17:18:42 2022 +0100

    Finally fix compile warning!

[33mcommit b6d9c26ebb78718aac9ca10311e02542b0876f01[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 17:11:25 2022 +0100

    another warning fix

[33mcommit 5216e783909a65dc8b34d581c133150ae54341ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 17:07:30 2022 +0100

    Another compile warning fix attempt

[33mcommit c62d8832f1ad5f4b204b4dc8bd79705172ba62b1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 17:02:06 2022 +0100

    Hopefully fix compile warning on linux

[33mcommit 7fc3a7bf79a6e740d149b7b02c69b6acdbe79e66[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 16:49:32 2022 +0100

    Quick fix to remove a debug

[33mcommit 26510c41663b24eeb33a0654984b420bd70f4af5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 16:46:33 2022 +0100

    Use date/time for log name if none specified

[33mcommit 3ee237e736169307409c545a3e64d6859bc69ce2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 23 15:45:39 2022 +0100

    Fix logfile/directory opening in Windows

[33mcommit 1aff6b9cb65e7f3b1c9c74f9cd9ec83565d4f4c8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 23 07:55:07 2022 +0200

    resize fixes for non-spectrum rigs, bug fixing users, updated to v1.48

[33mcommit 79531712b0644443c1e478bd366878bf8d14739f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 22 21:14:05 2022 -0700

    Fixed glitch when tabs are changed prior to rigcaps.

[33mcommit 9c49395fa0700901f122d20dcefbb5e02a9a84dd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 22 21:06:49 2022 -0700

    Additional resize fixes for non-spectrum rigs.

[33mcommit 11c1d2f5df44b329fe390e3dc2d47130e6daab38[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 22 20:53:57 2022 -0700

    Hide UI elements not needed for non-spectrum radios.

[33mcommit 71537c21e179100713c123179b1334d0fa546c54[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Sep 21 17:03:36 2022 +0200

    updated CHANGELOG and WHATSNEW

[33mcommit 12ea418f1e3fa85dd234bf1e935af5dfaba73e61[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Sep 21 17:01:44 2022 +0200

    updated CHANGELOG and WHATSNEW

[33mcommit 3dbfc94419a3048f381579c1a9494088e68f86c9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 21 15:38:28 2022 +0100

    Fix that was stopping users being created when none existed

[33mcommit eda1f9fcd3fe3becd9d019ae2919632edb79e876[m
Merge: ae6f1f2 9ecdd02
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 20 18:57:09 2022 +0100

    Merge branch 'master' of https://gitlab.com/eliggett/wfview

[33mcommit ae6f1f2ca35ab0eaeb79d2c25f7fb1e98f250a6d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 20 18:56:36 2022 +0100

    Add quick fix for rigctld fmv issue

[33mcommit 9ecdd02246059943dbe7e798b9bc4e1d96c3883c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Sep 20 19:06:12 2022 +0200

    Added dialog box to the toFixed button where an edge can be selected; bumped to v1.47

[33mcommit cd3320ec9ab12f7ad74e9efaf54f5087e2682e46[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Sep 20 08:58:12 2022 -0700

    Added dialog box to the toFixed button where an edge can be selected.

[33mcommit 6b44b81a2a6d0a70d8e112a1edaecc52e2773333[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Sep 19 17:08:50 2022 +0200

    removed .swp

[33mcommit 520b1d051a344f6ff94a20e801e1e570fe3557a8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Sep 19 17:06:00 2022 +0200

    Added controls for custom scope edges, hide/show scope controls
            depending upon the scope mode the radio reports.
            Fixed clear peaks button to work with the plasma underlay.
            both audio system button are disabled when connected to radio and enabled when not.
            Remove password from log
            Fix server user handling

[33mcommit 881823a8392a60b4eb2e775e26e264396da11c83[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Sep 18 13:13:33 2022 -0700

    Added controls for custom scope edges, hide/show scope controls
    depending upon the scope mode the radio reports.

[33mcommit bb2593be6f1d80447726d5cfbedda585b3ffa93e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Sep 18 12:22:11 2022 -0700

    Fixed clear peaks button to work with the plasma underlay.

[33mcommit 4bd80cb9e5b579d13478c3b25798d6af27182ed8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 18 15:23:37 2022 +0100

    Make sure both audio system button are disabled when connected to radio and enabled when not.

[33mcommit 079f03886f3d832ffec8aacc61c3c30c3f5bbe16[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 18 15:02:31 2022 +0100

    Remove password from log!

[33mcommit a04f7f57d363988ac7e508509f7b36046816bf22[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Sep 18 14:59:03 2022 +0100

    Fix server user handling

[33mcommit 8638086f3f7e1ad8aaf30c6882afa7d2ad48d022[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Sep 18 13:05:08 2022 +0200

    removed lingering files

[33mcommit c09c548eb0df5bcb804d7667138c17730fa61662[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Sep 18 13:04:13 2022 +0200

    log, connect and dial changed/fixed; bumped to v1.45

[33mcommit 6dd7cc02479b421fb05aafad5bdf4fd142d45ec9[m
Merge: b13de1d a7ba906
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Sep 18 12:54:15 2022 +0200

    Merge branch 'log'

[33mcommit a7ba90656902e3a5e3968765aeeca17329c8e131[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Sep 17 15:02:21 2022 -0700

    Log will automatically scroll to bottom on show.

[33mcommit 0c22f81690d01bc7577dc33a2dacb646483a700b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Sep 17 22:43:40 2022 +0100

    Remove focus from connect button once clicked.

[33mcommit 4dcc7c47537139e12277a3ec98bbd95f1545f954[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Sep 17 22:31:43 2022 +0100

    Move connect button

[33mcommit 77f48f5f0c7298407844b3b20c758b7f4925b897[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Sep 17 21:04:46 2022 +0100

    Update wfmain.cpp
    
    Add enable/disable audioSystemServerCombo

[33mcommit da0644f1bae265666659bcaf7089e18c40a1bae7[m
Merge: fca463f 0735c35
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Sep 17 20:58:41 2022 +0100

    Merge branch 'log' of https://gitlab.com/eliggett/wfview into log

[33mcommit fca463f346aac30f8dba7b71c06d150d79644ed0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Sep 17 20:57:49 2022 +0100

    Add "Cancel" to connect button

[33mcommit 0735c35cecda40952794bdd13c5ad3819f33b1fe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Sep 17 12:05:32 2022 -0700

    Fixed bug where the frequency dial skipped extra when crossing zero.

[33mcommit b13de1d9eee1d53b7b812429c16f08157da1d2f0[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Sep 17 10:36:07 2022 +0200

    missing space and lowercased Termbin.com to termbin.com

[33mcommit 81607a80699541cefc3ac9013d196ae787b25fee[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Sep 17 10:26:13 2022 +0200

    merged log branch and updated to 1.44

[33mcommit cf50e9d18a271b55040d580118a69e3803319420[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Sep 16 13:43:01 2022 -0700

    Added an additional combo box for the audio system selection under the
    Radio Server page of settings.

[33mcommit aba2928d42218c8c612654dd7f4a77613b1e0e1e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Sep 16 13:22:58 2022 -0700

    Added dedication in the about box.

[33mcommit 7e2682170f5443fdd575896d8703e650817b03ee[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Sep 16 20:51:16 2022 +0100

    Make Windows expand audio combo when clicked

[33mcommit 221086ed4f341b1e264151534e53651e7579e2a9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 17:35:29 2022 -0700

    Minor edit to margins

[33mcommit b54f724ce9860d9994224a0f59b34ed2c13f021c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 17:04:01 2022 -0700

    Removed some additional padding that was left in parts of the UI.

[33mcommit 6ed1a8e80e96d00793ae298bed6118bde0f07ea2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 16:31:27 2022 -0700

    Changed windows 'open log' command to launch notepad.

[33mcommit de46d62346f30c8d3df5c37a80dd73a2b426633b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 14:13:14 2022 -0700

    Removed extra disconnect message from logging host.

[33mcommit d90104a79ec07513123786cf7c8b17e45d6d65b1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 14:09:16 2022 -0700

    Added flush to the text stream to termbin.

[33mcommit b849df9f7f5d38d6a1a8d24137c248c50dd70910[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 11:59:04 2022 -0700

    Better scrolling behavior. The log is now aware if you are not at the
    bottom and will not force scroll if so. Otherwise, it will bring the
    horizontal scroll back to the left-edge.

[33mcommit 940f5fb9615ef5370a5f22ea0629f7e995b58fdc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 11:55:45 2022 -0700

    Renamed URL message box in case we add an additional message box later.

[33mcommit d4ae4181ed94aa4964c903a620dacb29879441ad[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 11:52:52 2022 -0700

    Clarified control label to Scroll Down versus To Bottom.

[33mcommit 4e3af7cccd08a880ab74b69a60a1c9ef55416f30[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 11:52:08 2022 -0700

    Changed font for better compatibility. Added some controls for scrolling
    behavior.

[33mcommit c5f626065aa6a0fcd8bc2d0e7f699f6a2c154ff1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 09:53:50 2022 -0700

    Debug mode can now be toggled from the logging window.

[33mcommit d25c9ca2459f399fdc8f6df11f742bfe367736e8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 09:49:03 2022 -0700

    Removed extra output on non-server builds.

[33mcommit 2225953d039c7d06723750d0c1ca85b3bdf3d606[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 09:42:19 2022 -0700

    Added debug logging button, however, it doesn't propagate the debug
    signal through all the modules yet. So, for now, it is hidden.

[33mcommit 95994ed737fa553d40566b87ee23afb99cdb7abe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Sep 15 09:33:09 2022 -0700

    Restored version to log file, fixed reversed log entries.

[33mcommit 42e3ee444dd755d9009d82b63435db2038de4fe1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Sep 14 18:35:45 2022 -0700

    Removed word wrapping from the log

[33mcommit 73b9c88e5e84f09b131d78b1b3987dacec8edf4f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Sep 14 18:32:28 2022 -0700

    Cut log window polling rate in half for lower CPU. 100ms is plenty fast.

[33mcommit 1b6cff85f4ce04178c2d6a4c4b6d2ba79ba1b42c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Sep 14 18:15:20 2022 -0700

    Removed audio volume logging.

[33mcommit 8b688548c20aaad0916712be616bc75bb239741a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Sep 14 17:53:22 2022 -0700

    Keep the logging window up after the message box. Added "log" logging
    category.

[33mcommit 3290b64a0b765a70d82039755770f77735c1326d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Sep 14 17:07:23 2022 -0700

    Added logging window capability with termbin support.

[33mcommit c97f0f78261e331846a2b3bd010f319bd0781849[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Sep 9 19:54:16 2022 +0200

    v1.43: resize bug fixed

[33mcommit 2198e3ca19694c8f3fc90d66ab3e6dd55247dd2f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Sep 9 09:41:32 2022 -0700

    Found an extra 10 pixels due to some layout padding I left in
    accidently.

[33mcommit a1d2f57d8b20db0f8bf28742c08de031789c7e17[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Sep 9 09:34:31 2022 -0700

    Main window may now be made smaller. Settings combo box sizes now
    limited.

[33mcommit d658e212200f790e56462514b0d2656f56aefe2c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Sep 9 09:28:52 2022 -0700

    Fixed issue where build paths with a space in the name would cause the
    git hash to fail capture.

[33mcommit 7145de132f87f4ef3bdb8f8f0994be64861710b9[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 26 09:08:23 2022 +0200

    audiometer support and changes to WHATSNEW/CHANGELOG

[33mcommit b91c033ef6d3fb40f3dc36652d7e730854bb493e[m[33m ([m[1;31morigin/audiometer[m[33m, [m[1;32maudiometer[m[33m)[m
Merge: 39cf34a 76389c0
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Aug 25 10:38:13 2022 +0200

    Merge branch 'master' into audiometer

[33mcommit 76389c0fbddc5d5ab527df45e5d57e7c6c1f4bb8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Aug 25 10:37:46 2022 +0200

    added color picker support

[33mcommit 39cf34af23636043fa85cf338b481ece66e87d92[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 22:23:32 2022 -0700

    Fixed dB meter, it needed the scale multiplied by 2.

[33mcommit 4f4abfc41a9b8071fb3983b553d2bbe224939490[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 22:02:20 2022 -0700

    Cleaner COMP label without the "0" position

[33mcommit 0f226fdca5e209710a1718300481fab400bc4315[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 21:52:36 2022 -0700

    Added tic marks, removed audio RMS calculation for now at least.

[33mcommit 9623226b0c2000335cfa88ce79e2fd4ff7bee60c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 16:06:41 2022 -0700

    Added average metering.

[33mcommit fbf1c4bce9924c61074fd32e1e7d60ff963f58c0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 15:21:29 2022 -0700

    Removed annoying debug message.

[33mcommit 4ea7ec80905c697bf908aaf07acb0e60e221f283[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 15:02:00 2022 -0700

    Fixed up signals and slots related to audio levels, removed extra junk
    code, added preliminary RMS and Peak audio availablility.

[33mcommit d90eb6f11f157c512c498235982ff19261b16446[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 24 10:17:11 2022 -0700

    Using peak scale color now

[33mcommit be95896205550ececef837ce12f5ef7fc4291ffb[m
Merge: 2afcef5 25d3d40
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Aug 24 18:17:57 2022 +0200

    Merge branch 'master' into audiometer

[33mcommit 25d3d4054868daacb813e84c1860df19da0e6dab[m
Merge: 173a311 d349398
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Aug 24 09:42:43 2022 +0200

    Merge branch 'color'

[33mcommit 2afcef5ef000eb0dfec338dbabc5f16b34da95aa[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 23:17:57 2022 -0700

    log scale, incomplete, needs tickmarks, etc.

[33mcommit f7e787e09791a3602f1bb2335fd07c83a6534fee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 23:10:58 2022 -0700

    Log scale for audio meters

[33mcommit 9fb74ed6e13ff70bac4482859db45174edf8eee8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 22:24:05 2022 -0700

    Audio metering initial. Very messy but you can select TxRxAudio as the
    meter type and wfview will show you TX or RX audio depending upon if you
    are transmitting or not. You can also select only TxAudio or RxAudio.
    This is nice for looking at Tx audio levels prior to transmitting, for
    example, or metering the radio's "monitor" audio while transmitting.

[33mcommit d3493988af20ca9f3fbf550af757f7ff327680c8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 17:01:11 2022 -0700

    Minor ui adjustment

[33mcommit c989dba9f9fab81407cf8a937acb2e5843adea11[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 10:56:16 2022 -0700

    Color preset UI now expands to fit. Finally.

[33mcommit 31f62fde671eb1c4fd9cda8876e5f843d8d0e579[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 10:47:19 2022 -0700

    Moved some preset controls.

[33mcommit 147156780828b16bf9077a32cdf65524a7170bb8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 10:41:26 2022 -0700

    Clarified what some controls do in the UI related to color and underlay.

[33mcommit b8ba9a16dc9db063144a27edc3c49451e7ee694d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 23 10:35:59 2022 -0700

    Added protection against transparent colors. Added revert feature to
    revert to default color preset for the selected preset. Added save
    single preset capability.

[33mcommit 173a311c7adb96d1d03e57fcff2d53c69beedb59[m
Merge: 105675a 7e80eb0
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 23 08:05:55 2022 +0200

    Merge branch 'color'

[33mcommit 7e80eb0ed7e4bf29119c7c2aff33491ef1fb99d6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 21:31:31 2022 -0700

    Cleaned up lots of debug info messages.

[33mcommit d49b6cd082151278f6231072fb8e148994a3a72d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 21:22:45 2022 -0700

    Oops, forgot to clarify the button name.

[33mcommit 8012a58bd9ceb469ecdbe870d80a2857da1b3430[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 21:22:01 2022 -0700

    Added separate colors for the high meter scale and the peak indicator.

[33mcommit 2a456b2adb4d3f8133a0edc0811e9c71f589b4cd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 20:44:58 2022 -0700

    Small cleanup.

[33mcommit fa0752518d5e73af3e72d15a20af6c3dd83a2e35[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 19:39:20 2022 -0700

    More color tuning!

[33mcommit 9afc44ce42cbef685f83bf6ffdf5272e7d92838b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 18:09:20 2022 -0700

    Refresh the meter when updating colors immediately.

[33mcommit da1cd8b14fac9d4caced6750b701b73cfcfb2a3c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 17:57:54 2022 -0700

    Added our "classic" color schemes back in. More tuning needed but a good
    start.

[33mcommit 6f7f1f84ec80579407cc4f2a8a53ff75f302f165[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 16:49:35 2022 -0700

    Added presets and preset naming to the settings file.

[33mcommit d1ffbe84f5ac598b85f2ddc046586713cefc775e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Aug 22 23:09:09 2022 +0100

    Still more work required

[33mcommit 59d2d735e6e55174090ef90c9fa5febc7a5c61e0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 09:44:00 2022 -0700

    Added preset names

[33mcommit 7e4d3f76d5ef3bbfac1c23c0d4a93244d82ba4ab[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 22 09:04:19 2022 -0700

    Fixed stretched grid color swatch. This has to be a qt uic bug.

[33mcommit 92904f8ae585b4447065e5acbe98b83e937d894e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 23:59:48 2022 -0700

    Forgot the axis color from default.

[33mcommit 952585261c123134a290295b3456ff45d37d6e35[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 23:54:37 2022 -0700

    Forgot text color from default load-in

[33mcommit d11ae30b1ac11c664a4655025ba2e518395a3b92[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 23:50:02 2022 -0700

    Fixed bug relate to pressing cancel in the color picker.

[33mcommit 1388c756c43d954f047afd14ea9068cea9808c7f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 21:51:50 2022 -0700

    Fixed alpha channel issue

[33mcommit 16237a47f54afcb089a6fdc321e08ed6820ecade[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 21:32:49 2022 -0700

    Slightly better closing for settings

[33mcommit a84fb6d09131abaf8ea3d93b53d3393d1f995efd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 21:19:08 2022 -0700

    Cleaner tab add/remove code for the settings tab.

[33mcommit 93b1af6eb60637c69d93b03aa31c28411a847152[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 18:05:20 2022 -0700

    Removed unused function

[33mcommit ecb9b8fbb4c166900a634c1f8616019c134e10b3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 17:52:52 2022 -0700

    Now all colors can be edited.

[33mcommit 2a509a61774305206c78b2bcb73ed0a0fb568465[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 16:47:17 2022 -0700

    Color editing for a few parameters is working. Lots of new helper
    functions introduced to make coding simpler.

[33mcommit ac8a98049e936bba064713f4c315952647803676[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 00:33:59 2022 -0700

    More connections from picker to UI.

[33mcommit b77715e0d8440b59dc640b7e1024e4db6e772432[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Aug 21 00:17:44 2022 -0700

    Color preferences. We can now set a single color and also detach the
    settings tab.

[33mcommit 4574e2b7b2259495f2bb0a89637c38497ee29675[m
Merge: 360cecc 105675a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 20 19:43:58 2022 +0100

    Staged merge of master

[33mcommit 4b0ce8537e242b6dcd37be151d91c3e21cadea31[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Aug 19 10:32:06 2022 -0700

    Added line input to color functions

[33mcommit 1423e7c3007192649b6ae81eeb505b678b6d1991[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Aug 19 09:41:13 2022 -0700

    Added color picker functions and demonstration UI elements

[33mcommit 105675a128aaccb778e45953e75fb8fc6b938dc3[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 19 08:12:22 2022 +0200

    modified version numbers, WHATSNEW and CHANGELOG

[33mcommit bd097dc3a5bbfb7fa224d01713d7841fe0c5320a[m
Merge: 9a77baa f1f58a1
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 19 07:34:38 2022 +0200

    Merge branch 'plasma'

[33mcommit ab65f8e38dc7dd053601c4ae682c34ad27d30e42[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 20:56:24 2022 -0700

    Finally remembered to remove annoying commented-out code.

[33mcommit 088d34af8be7d6759e75627ae2f765629c7ab203[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 20:54:50 2022 -0700

    Removed checkbox from prior peak hold mode.

[33mcommit d8dc9daa1501841afec6002bd204deacf39330ac[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 16:45:58 2022 -0700

    One more thing for qcp 1.3

[33mcommit a947ba6b189f663fbe506e5aea577165bb2b643a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 16:40:10 2022 -0700

    Added support for qcp 1.3

[33mcommit 0e15e8ec1e58b5ad09057ae46530ea0a013bdd5e[m
Merge: 9a77baa 1d14d95
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 14:31:22 2022 -0700

    Merge branch 'plasma' into audiofix

[33mcommit 1d14d9520e296b252f77fa91930dd6137113981c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 11:52:41 2022 -0700

    Fixed issue where spectrum greater than 128 was negative.

[33mcommit ebb2c59a45e023ee7ec2fae1852297c4863b090f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 11:11:57 2022 -0700

    Added "pre-sorted" flag to plasma average mode plotting.

[33mcommit a7587963b07eccb1e5129bbe13ad15b3a7414ee9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 11:07:29 2022 -0700

    Fixed issue where the waterfall colors re-scaled to min-max versus user
    preference upon wf length change.

[33mcommit c59050e632c9172f361707c49cdecd60c3a29dcc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 10:54:54 2022 -0700

    Removed issue with plasma buffer resizing with the waterfall.

[33mcommit 7093ac104a6781798893887227b5465a3b23796e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 10:24:07 2022 -0700

    Removed unused variables, corrected spelling.

[33mcommit f1f58a10cf602f87e94dce703f1b1a38f6c883e6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Aug 18 09:56:06 2022 -0700

    Slight reduction in CPU usage with regards to wf, more to come.

[33mcommit 76398808612b7e77822d277b8c20c2fae8c749f4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 23:30:02 2022 -0700

    Fixed issue with the floor and ceiling not updating initially. The
    waterfall is now ranged prior to replot each time.

[33mcommit b2204b35193f45685491a70d44ddfe7479f935f1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 22:38:44 2022 -0700

    Fixed resize; added mutex.

[33mcommit 5215984de82e912d7b6f6997752c63498f5a7596[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 21:46:47 2022 -0700

    Minor change for slightly faster averages.

[33mcommit 123084c7794000049413b07858c279ded7bbefb9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 19:58:04 2022 -0700

    Slightly faster plotting due to data being already sorted.

[33mcommit 03a279087e2ea6c18f213f5668254c02bcac846e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 18:46:00 2022 -0700

    Added underlayBufferSize to the preferences.

[33mcommit 6ebb3b7680161275f8bd7e02acf686e566e0ad04[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 18:41:35 2022 -0700

    Added some preferences for the plasma.

[33mcommit 32d55ef490ed99296b21137ebc4d48f55dbdf2bb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 16:34:26 2022 -0700

    Fixed spectrum line length

[33mcommit 53bed16b45782c978bea99b40a8b5f6f0ba1c446[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 16:21:33 2022 -0700

    Added floor and ceiling adjustments for the plots.

[33mcommit 02e6733cddebcde2d478e7179053a902db68f1dc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 15:51:26 2022 -0700

    Fixed ancient issue with layering of the plot data

[33mcommit c753089ac7b56f5af67f23e758555e213d636aad[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Aug 17 15:10:07 2022 -0700

    Added spectrum plasma metering.

[33mcommit 9a77baaead6b1687b80447b28cf08b511bd141b4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 4 20:46:15 2022 +0100

    Shutdown audio when radio is turned off.

[33mcommit ac2c104209700a5236ab8375ed89e057b591dd72[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 5 10:37:10 2022 +0100

    Re-order audio handler destructors

[33mcommit bcc16b8d2945da0dc15366a4f1c1fda1da8b6fba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 5 10:31:45 2022 +0100

    Send blank audio packet on disconnct

[33mcommit a9e9c807088c75ff76b10bd02dc3b7306f62371b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 14 10:31:29 2022 +0100

    Fix silly bug introduced in server audio TX combo

[33mcommit 5dcb0819a8e3bd527875ec07bf39c536464f4207[m
Merge: d626322 38274cc
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Thu May 26 15:16:40 2022 +0000

    Merge branch 'master' into 'master'
    
    Added IC-746
    
    See merge request eliggett/wfview!9
    
    Thanks to Philip Kubat

[33mcommit d6263223af112ace31e66f921317660ff6e8e4ed[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 24 17:55:03 2022 +0200

    dropped openSUSE 15.2, out of support

[33mcommit 485d012b4294ccbdf29476e487bbc5b20c60cf15[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 24 17:37:19 2022 +0200

    WHATSNEW

[33mcommit b4f379d52dcbfd552dad6d21113fbf191cea10df[m
Merge: 227ea8f 0601714
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 24 11:18:41 2022 +0200

    Merge branch 'wfserver'

[33mcommit 06017148dd7d5ed3bc548b9af2fbdc3d27d0bb50[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 24 10:13:44 2022 +0100

    Update main.cpp

[33mcommit 5cb81b412f328f0315ef158db72e67cf229f8eb7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 24 10:08:18 2022 +0100

    Try to quit wfserver on ctrl-c

[33mcommit 227ea8fc348db38cf426ce3b459ba31b4911f9d2[m[33m ([m[1;33mtag: 1.2e[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 20 18:24:28 2022 +0100

    Small .pro file update and cleanup

[33mcommit c807c40bd25a7acc6d12ef6175e523606c09e220[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 20 18:24:28 2022 +0100

    Small .pro file update and cleanup

[33mcommit f2e9a9000021e58fb87c09ea7c7b77b964d9df4f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 20 15:50:47 2022 +0100

    Support newer versions of rtaudio that don't generate exceptions

[33mcommit cd03ac40d552d35d3e6b8fb7992ad60db2e53240[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 19 16:03:06 2022 +0000

    Update INSTALL.md

[33mcommit 49773706d70495f6632281c396fa8a2bbfba0e58[m
Merge: 2b9d163 079a509
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 15 13:11:35 2022 +0200

    Merge branch 'wfserver'

[33mcommit 079a50980d91b1792699885ed44387e8aca65d08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 15:05:48 2022 +0100

    Fix RT/PA builds on MacOs

[33mcommit 5ef50c97069aa71f5a40fe2e3891b81126c1e394[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 14:02:31 2022 +0100

    Update rthandler.cpp

[33mcommit 8f5ba2efb197f2f6edfca3834559b8cf5980b966[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 13:54:09 2022 +0100

    Convert audioHandler functions to virtual

[33mcommit b3f611543c23679a593e9bffb5bf5e51c6205cd1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 13:45:56 2022 +0100

    Make RT/PA a subclass of audioHandler

[33mcommit bcde69c92b3f1b7f7ce21a11e802837694f5b308[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:48:58 2022 +0100

    Update commhandler.cpp

[33mcommit 2e1d88c19490195aa1273da4ed381901d589e7b3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:40:58 2022 +0100

    Update commhandler.cpp

[33mcommit d468e01d20beaa830f7a3bc289da7cf41e739675[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:39:52 2022 +0100

    Update commhandler.cpp

[33mcommit 1305f06eabe8886c37cf319edc7eb87435a7c7ad[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:38:35 2022 +0100

    Update commhandler.cpp

[33mcommit b049704dd2cdcbaf78ec3ed897fd8635aee804a5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:35:41 2022 +0100

    Use qtimer to signal a reconnect

[33mcommit 6078a31a3c4b78262806f44546d6c8f4499f2f7c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:24:09 2022 +0100

    Add timeout if no data received on serial port for 2s

[33mcommit f003a8a1b877e80eaf4f63ce1dcdb74c8ae89927[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:13:20 2022 +0100

    Update commhandler.cpp

[33mcommit 630d2eaba3ba7a6d3e629dca0878ec2ed3727a5f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:10:27 2022 +0100

    Update commhandler.cpp

[33mcommit e248882e31c627240e6925da1f2ad6a86aa57282[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 11:07:05 2022 +0100

    Add reconnect on failure to commHandler()

[33mcommit 466571a7584705872bbd4decc7ea5b5c4e7b247c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 10:41:44 2022 +0100

    Use goto statement for error handling.

[33mcommit b95cf793186f988d6e19b71900a6fe5e4beab98e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 10:33:31 2022 +0100

    Update rthandler.cpp

[33mcommit 50eb498026763907f2e3294b86badf576925e08a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 10:30:26 2022 +0100

    Another error check

[33mcommit 28a3209c2148072bfc20f26c85d6d742077a999a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 01:37:03 2022 +0100

    Update rthandler.cpp

[33mcommit 35d54468cd43760bb70d7f5ea0587eb73cf56acd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 01:31:04 2022 +0100

    Update rthandler.cpp

[33mcommit c1461947eb45afdd6b8d777404ff73152f6184fe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 01:27:44 2022 +0100

    If audio device fails to open, retry 10 times before giving up.

[33mcommit 48562aa23b01f7e6b516448854f2adc7360ec0ce[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 01:11:15 2022 +0100

    Don't try to connect to radio we are already connected to

[33mcommit 8fd8fa390ef0b56746571c178af8acc60656f328[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 14 01:11:00 2022 +0100

    Fix mod meters in PA/RT

[33mcommit c84cc4330d33e2fb7422a7a0310ba987d7cb138a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 13 18:45:13 2022 +0100

    Add waterfallFormat to wfserver

[33mcommit b78f613ef2fd9ca75cdf1496621ccb4554d4c3c7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 13 16:09:26 2022 +0100

    Bit of code-tidying and minor fixes

[33mcommit 2b9d163613c33df90efb421d8723daec140c9d1f[m
Merge: dccafb9 6a5f9ce
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 13 13:43:13 2022 +0200

    Merge branch 'wfserver'

[33mcommit 6a5f9ce988689b4f9c8b5c6ebe94309fff1d9733[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 13 12:35:28 2022 +0100

    Portaudio try to find compatible format

[33mcommit dccafb9ffba41e2918055da7f94a748fe06e100c[m
Merge: a7671b4 1aa45dc
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 13 11:23:45 2022 +0200

    Merge branch 'wfserver'

[33mcommit 1aa45dc849e3f4e6baf9f9e1948a269554b53dc8[m
Author: M0VSE <phil@m0vse.uk>
Date:   Fri May 13 10:20:17 2022 +0100

    Fix latency display on rtaudio

[33mcommit a7671b403d2262eab6178497905af6072ee58f4d[m
Merge: 9285809 0b74407
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 13 11:07:44 2022 +0200

    Merge branch 'wfserver'

[33mcommit 0b74407448c38be313cdbebc8884e3d20c4f4b1b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 13 09:55:16 2022 +0100

    Set default audio device if not found

[33mcommit 9285809819f09f799266bfc6efb33b41b41a6f78[m
Merge: eded80e b28dab4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 13 08:19:23 2022 +0200

    Merge branch 'wfserver'

[33mcommit b28dab4dc268d28757f4ee5279e4c8adef38d8fb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 20:30:51 2022 +0100

    Use a mutex to protect RT buffer.

[33mcommit a855c931b6d8a8e3ab6b125402a87b02833d0312[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 20:04:30 2022 +0100

    Add amplitude to output audio

[33mcommit c90611e444638f56af19eb52353f44fb10939df6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 20:04:15 2022 +0100

    Fix 8 bit audio encoding.

[33mcommit 717b71ad4c1fe6e49be9543454de0dc0ec42e6ce[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 15:57:45 2022 +0100

    Update servermain.cpp

[33mcommit 2e433ed71f63bfbd9b9507cfdc5092dadaf978bf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 15:53:48 2022 +0100

    Add pa/rt to server

[33mcommit e6ff2ae15228b4da1b051e3ffe7c1111872e3ce1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 14:29:08 2022 +0100

    Update servermain.cpp

[33mcommit 5eb294dc6da4a632dfcb9ea41535724bdd354647[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 13:42:19 2022 +0100

    Update servermain.cpp

[33mcommit 728f01abf484e61f5a168c616db4804de6bbb44c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 13:39:00 2022 +0100

    Update servermain.cpp

[33mcommit cd1f0f0ba9f9e103ddfdb5e6efc74777a95da2ae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 13:33:34 2022 +0100

    Update servermain.cpp

[33mcommit 33211908bcc859d72f42fc334ec385bde5df9489[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 13:27:35 2022 +0100

    Update servermain.cpp

[33mcommit 95ec59d46def14cea893ae26d5ae8d804e489e48[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 13:13:59 2022 +0100

    make PulseAudio the default

[33mcommit 6b7387cba28a0fe5b80620c749106e8dc7145231[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 12:35:53 2022 +0100

    Update servermain.cpp

[33mcommit 92b43e8fa27bdccadbc102ac7c1132fcc27cc429[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 12:29:29 2022 +0100

    Update servermain.h

[33mcommit 578b993f70a8311b972aae27007ac697ed79b7cb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 12:24:27 2022 +0100

    Various fixes

[33mcommit 44f6ec27402555c2c3cde7c6a59bf9d50b725286[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 12 01:46:20 2022 +0100

    Add back support for portaudo and rtaudio APIs

[33mcommit eded80efe8fdd8ee23d2ab22743e2445d912791b[m
Merge: 5694963 63c5e02
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 11 07:41:39 2022 +0200

    Merge branch 'wfserver'

[33mcommit 63c5e0257d3ed82a18802374577037e97331630f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 11 00:22:08 2022 +0100

    Some more tidying

[33mcommit 04ad04757d91dea304f69490fc60755bbe5095ac[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 11 00:18:28 2022 +0100

    More post merge tidying

[33mcommit 2fa83028d3e15f81454ee36fe5f587c0a655e101[m
Merge: 772375f 498f502
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 11 00:15:06 2022 +0100

    Fix merge error

[33mcommit 772375fb9dbf7074d43a9122a36a36fba4f0707f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 11 00:11:29 2022 +0100

    Split udpHandler into multiple class files

[33mcommit 498f5020a57dec9996ba427b6c6f71e81a3db33e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 11 00:01:04 2022 +0100

    make latency too high trigger at lower value

[33mcommit 00f15a059ef841cc7ef2df06161fdb4d8dc0af34[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 10 23:55:18 2022 +0100

    Add some more audio status

[33mcommit 30ca8aa44a842d0bbf676fc0ca1fc0b34a053109[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 9 18:51:26 2022 +0100

    Try to force 16bit int instead of 8bit uint

[33mcommit fba9c6f20750a8d8a126d74709e0ce7422b455e1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 9 18:47:12 2022 +0100

    Try to force minimum of 48K sample rate for native audio

[33mcommit 5694963450b1bd5a95d06a27dfa2307130a3a949[m
Merge: 9ef7fba 82faf2c
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon May 9 11:48:05 2022 +0200

    Merge branch 'wfserver'

[33mcommit 82faf2c463263cd504059686da3d182485f4e173[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 9 10:38:33 2022 +0100

    Remove extra Opus configuration

[33mcommit 8d924b6fdadda957de0edcab9b58d7680847b2d3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 9 10:33:30 2022 +0100

    Configure buffer for input audio

[33mcommit bddf283e7a356a9e80c6bc45c214aa3851655f9e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 9 00:29:07 2022 +0100

    Always use "preferred" format where possible

[33mcommit ec169ca3d4962eb52726d1695348d3435b9beb92[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 22:48:39 2022 +0100

    Fixes to audioconverter

[33mcommit fda7d2123941972a20d1f0473d5daa9074b502c1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 21:48:47 2022 +0100

    Update audioconverter.cpp

[33mcommit 3e0c0081447c254ded55e254b3000fd2dd1c7596[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 20:57:35 2022 +0100

    Fix if 0 samples detected

[33mcommit 70985641216d753f2df96a4008675d8e34bc9a47[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 19:44:31 2022 +0100

    use quit() instead of exit() to cleanly shutdown

[33mcommit 75f1f2d6ea35119838ee31f1c776f14b2bb66d1a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 19:37:29 2022 +0100

    Update servermain.cpp

[33mcommit 135640df448fee40ff7c8627228538d27aef9080[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 19:35:47 2022 +0100

    Remove parent from classes that are moved to a thread

[33mcommit ed3ba16a8d34c17b7510ee0c47ab27ce71729e2a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 19:33:22 2022 +0100

    Update udpserver.cpp

[33mcommit ac677db7ac20ade73ecbf8aa0ff03ff5b63f28c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 19:31:05 2022 +0100

    Change class constructors to include parent

[33mcommit 066c1b58ac166e42020e8cb9a3bae52069280d67[m
Merge: 7cb31cc b8838e2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 18:45:35 2022 +0100

    Merge branch 'wfserver' of https://gitlab.com/eliggett/wfview into wfserver

[33mcommit 7cb31cc8afc5006e650f30476fb986c408e9630c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 18:45:22 2022 +0100

    Update udphandler.cpp

[33mcommit b8838e24682269a35b148bd696f9c840f5424a7a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 18:44:20 2022 +0100

    some improvements to audio

[33mcommit 9ef7fbab0a7f6448ff65f8a4f01603a4819b3ff7[m
Merge: 3308bbe 181c2ee
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 8 16:08:22 2022 +0200

    Merge branch 'wfserver'

[33mcommit 181c2eeb0712aab1384be1ece086d8ef29d48c0a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 12:19:35 2022 +0100

    Log when port is being closed

[33mcommit 3deb7ab6ecb762d3a2f9ffcf3c6f070eef5db331[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 11:54:42 2022 +0100

    Update audioconverter.h

[33mcommit 376fe72b497c3e14590f67a0f19c2f6181c8eb82[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 11:52:24 2022 +0100

    Update audioconverter.h

[33mcommit 8e38a8efb254a01cb4468c678e38b7cbd7987b26[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 11:46:33 2022 +0100

    Create function to convert Icom format to QAudioFormat

[33mcommit 3308bbeb5415a85260a23b2bc0de72cea9d4348e[m
Merge: ad71e43 72bef7f
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 8 11:19:51 2022 +0200

    Merge branch 'wfserver'

[33mcommit 72bef7f783397e9a92602efbafe33ff491e26076[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 10:19:21 2022 +0100

    Fix build for new audioconverter

[33mcommit ad71e437a2c76c8558c498cb09387a101cad2cd8[m
Merge: 4125737 71f88cd
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 8 11:16:35 2022 +0200

    Merge branch 'wfserver'

[33mcommit 71f88cdfedc024ea16dab28bf3050592b46db3b4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 8 10:04:36 2022 +0100

    Skip unneeded float conversion for Opus

[33mcommit bbbb36ebaecadbebcd91daa7d06dcc306c315ead[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 7 11:14:01 2022 +0100

    Remove Opus FEC for now

[33mcommit 3068b0f5ccb21f927ed589025c74f81dc4156149[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 7 10:54:20 2022 +0100

    Update audiohandler.cpp

[33mcommit 05c3433bcd9394d8fad73ac84d2d98f0534ac0d7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 7 10:51:02 2022 +0100

    More testing

[33mcommit 1e51e36c9de3320848de6dd63ebe660a3a05c74c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 7 10:43:28 2022 +0100

    Backup out of last change

[33mcommit 0d529b4a1b5830dd21c8ebcb1da81151105d3cb8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 6 23:27:25 2022 +0100

    Update audiohandler.cpp

[33mcommit de6db645def2104a800b0646cfd2bc700f3e462b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 6 23:19:31 2022 +0100

    Update audioconverter.cpp

[33mcommit 550407e0a9850d0af8f5d78bb077ad88eb15a4a8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 6 23:15:32 2022 +0100

    Update wfserver.pro

[33mcommit 9118d8f4dc87b0f33ce4bd48aefa4cd976659232[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 6 23:12:29 2022 +0100

    Update audiohandler.cpp

[33mcommit 78f16b7cff51b49b9a681f5344d7e89c0ec308d5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 6 23:10:46 2022 +0100

    Add experimental audio converter

[33mcommit 41257378199119d2531c07028678d105d7edac11[m
Merge: 3bc0b48 5bd2909
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 6 08:14:43 2022 +0200

    Merge branch 'wfserver'

[33mcommit 5bd29096e5ea04468bbb6534db7b7964c7a5d927[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 18:48:45 2022 +0100

    Found issue with splitwaterfall function

[33mcommit 8b958d572caf34769e939cd623d82cb53cfd4978[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 18:09:16 2022 +0100

    Fix some compile warnings and try to support sharing lan connected rigs

[33mcommit 3bc0b4840a62d454a40a597cc21ba4179bf8da81[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 5 13:41:31 2022 +0200

    WHATSNEW

[33mcommit 159d757d5333b0fd601b4088fca32e663a1e23df[m
Merge: 7956684 bf5f819
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 5 13:41:04 2022 +0200

    Merge branch 'wfserver'

[33mcommit bf5f81992761149da556bebaabd321070cec3792[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 11:16:59 2022 +0100

    found another small retransmit bug

[33mcommit 7956684f7d98a5ad8a00c031d708bddebc3ff96a[m
Merge: b3068c2 7924d2b
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 5 10:44:22 2022 +0200

    Merge branch 'wfserver'

[33mcommit 7924d2b87f5908365222cf734c5738cc334bb90a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 09:01:04 2022 +0100

    Fix retransmit missing message

[33mcommit f662d1f7cd9d00b9d54ab978f10e72ddc225212b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 08:57:50 2022 +0100

    Found another retransmit bug in server

[33mcommit b3068c269d07d8c826810fcc1df27ea3eedfcc6b[m
Merge: df2fb20 069c536
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 5 08:41:04 2022 +0200

    Merge branch 'wfserver'

[33mcommit 069c5368d870c7695b4423dc0ce21c154b6575a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 02:01:20 2022 +0100

    More debugging

[33mcommit 7b5f29f4fe122d5bcd3106efdafe9d9c21f1e855[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 01:49:21 2022 +0100

    More retransmit debugging

[33mcommit 4b19ed522fabf600d04fe5cea3f842caf8f43b8d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 00:31:55 2022 +0100

    Hopefully fix occasional retransmit bug

[33mcommit 24841c5da9f1f9df80cc8a3231aa99198ba30366[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 00:23:42 2022 +0100

    Fix debug message

[33mcommit cac93198a63cebdb2ab8695aaa8f9702da94d371[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 5 00:06:50 2022 +0100

    Debugging retransmission (again!)

[33mcommit 85ba0fb56967fdae1ae1ce6d180009c12089ad03[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 4 22:29:05 2022 +0100

    When using Float, force 32bit as Mac doesn't like it otherwise

[33mcommit 3aefa6893964506a45cbe3e0c466accd80de66c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 4 20:37:58 2022 +0100

    More fixes to make client and server behave in same way

[33mcommit bdca404457c1c1dd9dcafc4645126e913657afe1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 4 20:21:29 2022 +0100

    Make udpserver and client behave the same way for retransmit

[33mcommit f42fbc2e54af93cb647d1f08cfbd9f7fe5d9f9c0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 4 15:43:34 2022 +0100

    Try using float for all audio. Not tested on Linux/Mac

[33mcommit df2fb20d780dc5b084e61dfcdc651318bf7759b0[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 3 14:57:34 2022 +0200

    WHATSNEW

[33mcommit 6826586ef61bb74f40c52d83fa3bd165418fa6f2[m
Merge: f3bb465 105d178
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 3 13:15:22 2022 +0200

    Merge branch 'wfserver'

[33mcommit 105d1782ed83558bb80b85d8df65795117318eda[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 3 11:21:08 2022 +0100

    Remove unnecessary code

[33mcommit 2b04b84eb158a68598a508303df5927ca8c6a173[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 3 10:37:41 2022 +0100

    Fix for uLaw encoding

[33mcommit f3bb4655ffa4078eccbf7a3d9e34af58f0d886e7[m
Merge: b2d2ee7 92288aa
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 3 08:12:37 2022 +0200

    Merge branch 'wfserver'

[33mcommit 8d75d01e2c2ded4688787c3bf40fd4ecf27f07bb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 17:15:41 2022 +0100

    Set Opus complexity to 7

[33mcommit 95cace832b56461408404c6b460fe3dc56737481[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 16:41:28 2022 +0100

    Try a reduced complexity on Opus encoder

[33mcommit 8217b12bc290f98537c60c24fa01c1859c38eb02[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 16:13:47 2022 +0100

    Update udpserver.cpp

[33mcommit a929b2b8a87dc2c40eedbe3b2d3db6e3a570940c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 16:11:35 2022 +0100

    Name threads to ease debugging

[33mcommit 76e52c42db9b9c2f36474933767adf90b980664a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 16:01:54 2022 +0100

    Add parens

[33mcommit fb99a572726ad4c3b45bc22082d9f2c45d446b53[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 15:58:41 2022 +0100

    Tidy some connections

[33mcommit 43f423f1ad6d24dbac1d9365254da59b127eeb59[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 13:53:49 2022 +0100

    Tidy getnextaudiochunk()

[33mcommit 92288aa768ea95cae495bf693b603089940e6a99[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 13:08:42 2022 +0100

    Increase late audio timeout to 100ms

[33mcommit 09ea7a2c26b21da6e32c1cb8cc2d4f3811a0c937[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 13:03:27 2022 +0100

    Try readyRead instead of notify

[33mcommit 7bf2f54255838dbfc115170c80d23a16379ec708[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 12:55:58 2022 +0100

    Use notify instead of timer for audio input

[33mcommit fb0d662b4037e79cbdae7551fc4a22eeccd4f836[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:56:40 2022 +0100

    Add debugging back after revert.

[33mcommit 94bb78af50854b54149ef9046a04a2ef42691c3d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:52:23 2022 +0100

    Revert "Add some more debugging when audio is delayed"
    
    This reverts commit bb6c615b4ccfc126e26f6de1eac345e3ccc51557.

[33mcommit ab36165d9db4eb9b878b8cdc087ac9cee3f98717[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:52:09 2022 +0100

    Revert "Update audiohandler.cpp"
    
    This reverts commit 7130f71335487e38bc98a6dd905ac6dff0cd0f55.

[33mcommit 8f3a8d38d4c8b7e1ea65f93ffed5fab0806a8a17[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:51:36 2022 +0100

    Revert "Update audiohandler.cpp"
    
    This reverts commit 60824c2055aecd89614cf9bdc8c9c8233c195ac4.

[33mcommit 1ba97134081b11b474ffe7c611f0dac500fc3dfb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:51:12 2022 +0100

    Revert "Reduce audio polling time"
    
    This reverts commit 26ea3bc3dcd496e561a32f2f3c8aa05e343794c6.

[33mcommit 26ea3bc3dcd496e561a32f2f3c8aa05e343794c6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:47:44 2022 +0100

    Reduce audio polling time

[33mcommit 60824c2055aecd89614cf9bdc8c9c8233c195ac4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:32:28 2022 +0100

    Update audiohandler.cpp

[33mcommit 7130f71335487e38bc98a6dd905ac6dff0cd0f55[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:29:34 2022 +0100

    Update audiohandler.cpp

[33mcommit bb6c615b4ccfc126e26f6de1eac345e3ccc51557[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 2 11:22:05 2022 +0100

    Add some more debugging when audio is delayed

[33mcommit 2525641e76d8ea027e4b221f3746496fa9a0bcc9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 12:32:40 2022 +0100

    Update audiohandler.cpp

[33mcommit 5b50127d0bbfd78f98df59614d65267b60944ebe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 10:52:46 2022 +0100

    Replace deprecated hex modifier

[33mcommit d0df7004ac0e0dde841db7cbdbe8f3b579eea119[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 10:46:47 2022 +0100

    Improve hex encoding

[33mcommit ff17a74df8954e3cfac42c1e8c37990573542a45[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 10:32:55 2022 +0100

    Make missing packets qInfo instead of qDebug

[33mcommit a9c21871b2c4ea4be2199abe9cf09494e9f9b95c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 10:05:51 2022 +0100

    Update audiohandler.cpp

[33mcommit 382f0951e58643612dfff6af6de906b815f58dd3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 28 10:02:51 2022 +0100

    Try to support all possible native audio formats

[33mcommit b510b70f041ca43f3259f793518b58686e440961[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 26 08:40:58 2022 +0100

    Add WFVIEW response type back in

[33mcommit b2d2ee7001198886ce38a4994aa8732354585d41[m
Merge: e0f50f5 6716a57
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 20 14:44:58 2022 +0200

    Merge branch 'wfserver'

[33mcommit 6716a57d5b6f1bea1c6709e6a20c6489745af4f8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 20 13:43:45 2022 +0100

    Fix wfserver

[33mcommit e0f50f579fb2f8e10efea3847781fafa0c0ab15b[m
Merge: 5685f6a 86502a5
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 20 14:38:17 2022 +0200

    Merge branch 'wfserver'

[33mcommit 86502a5c3a5d69563e16926cc79d19ea95ca6947[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 20 13:35:23 2022 +0100

    Various compatibility improvements

[33mcommit 5685f6aa67b83f4af00416b8327875e8e01b64f0[m
Merge: 4ba4358 d86f895
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 19 08:15:18 2022 +0200

    Merge branch 'wfserver'

[33mcommit d86f8958b2724e7c186e735852a85efe12de3498[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 18 20:29:50 2022 +0100

    Add support for splitting/combining waterfall/scope data

[33mcommit 4ba435845b16bef619ce835ea4fcfa238ff54124[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 18 14:13:53 2022 +0200

    added contributors and whatsnew changes

[33mcommit c1f9358d38bac2bd222cc4de1f174ec19ce80645[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Apr 14 18:02:11 2022 +0200

    updated WHATSNEW and CHANGELOG

[33mcommit 2ff36e26b729abe3fbf9c3feab3ee154cd50192b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Apr 14 08:20:48 2022 +0200

    bumped versions for wfserver and wfview to 1.2e

[33mcommit 4fe9b2cba7e28fa55205ac9bda98cb49536743d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 13 12:55:59 2022 +0100

    Move wfserver configuration and add simple keyboard handler

[33mcommit a3977395c1b6d45840ec40d2b2ef07a61a7d3500[m
Merge: e834396 b67b43d
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 23:25:30 2022 +0100

    Merge branch 'wfserver' of https://gitlab.com/eliggett/wfview into wfserver

[33mcommit e8343963873eeb98231794998c17afd9c619f08c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 23:24:56 2022 +0100

    Force 16 bit samples if 24 is requested.

[33mcommit b67b43d0cb72da49abfbc067c932479747c243fd[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 11 14:20:24 2022 +0200

    added eigen3 for suse build

[33mcommit 55fdf52436d91a3ccda2ea1d667602a0890f1a97[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 12:13:48 2022 +0100

    Update audiohandler.cpp

[33mcommit 7542cc3f56753dc31930b1f25ae076d9e02cddee[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 12:11:10 2022 +0100

    Update audiohandler.cpp

[33mcommit b7dee4f5f4bcd372d0ecb237059a39f3b83f0270[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 12:04:40 2022 +0100

    Update audiohandler.cpp

[33mcommit 2b588ffce00ff690026bb2c11cb603d3d851b3b1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:59:38 2022 +0100

    Switch back to using a timer.

[33mcommit af2be449193392dd3c9119a4b63dad3d4568c323[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:54:06 2022 +0100

    Update main.cpp

[33mcommit 1e94a97964f2a6f626cd899cd2d71db2a810ae88[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:52:57 2022 +0100

    Add time to wfserver debugging

[33mcommit 718d54ee3b6b3094ffec5379f479c0f8a133195c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:49:48 2022 +0100

    Update audiohandler.cpp

[33mcommit 9d63c36e51ba780f83a78e0100dc06f11e065e0d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:44:53 2022 +0100

    Update audiohandler.cpp

[33mcommit c0a1000c9a5d0e26935dc01dbff1a3e0af0821e3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:35:33 2022 +0100

    Update audiohandler.cpp

[33mcommit 74de3087c8ee1015791d765145d18bab6341cd3f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:28:32 2022 +0100

    Update audiohandler.cpp

[33mcommit 3296e16195e5e7612934528c558ab566d8c66e50[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 11:26:52 2022 +0100

    Found bug in server audio

[33mcommit 3815ac67e74c2296c7cd09bff8e057f06a21d6af[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 09:59:21 2022 +0100

    Update audiohandler.cpp

[33mcommit ecbac9b48c6fca757136b384c207ef3c9a69a9d5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 09:53:58 2022 +0100

    Update audiohandler.cpp

[33mcommit 5ba0a382a984fa584c25b089985169ca52c19376[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 09:49:25 2022 +0100

    Update audiohandler.cpp

[33mcommit 4ca21b84cb512a16423beceec2607d61be9128eb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 00:33:39 2022 +0100

    More opus changes

[33mcommit 109bdb28125322ab233b2fcbf35bfd9efa48723d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 00:23:08 2022 +0100

    Update audiohandler.cpp

[33mcommit 0a711e5c1fef87322eebd2aa697240860edbb4fb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 11 00:05:54 2022 +0100

    Try a different method

[33mcommit e068bc5745d934030235547d4374544428d31840[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 10 23:19:45 2022 +0100

    Fix for opus?

[33mcommit 50c8b4e5455d462e6a7b5e571c4e854d9f6f1f2f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 10 23:13:51 2022 +0100

    Change audio output to use single/slot

[33mcommit a108f1ca997a201925026b7daba50d6a8db17f83[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 18:55:41 2022 +0100

    TX Audio fixes.

[33mcommit 87c0850c5b89c665b0c92b28c627bdca23dbb505[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 18:42:37 2022 +0100

    Fixed retransmit bug in server code.

[33mcommit 9e269d774ae0e3f68fecc8e97bafbbc1b7d6a5b5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 15:51:59 2022 +0100

    Add some more startup logging and improve windows startup speed.

[33mcommit 03b324be1ec61aef2aa00697846e5709f65c4d9f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 15:28:46 2022 +0100

    Set HighDpiScaling attribute

[33mcommit e87719bfd12b8454c5c19d98ad44dcf87e914501[m
Merge: 1f25ca6 2ee20d9
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 14:40:43 2022 +0100

    Merge branch 'wfserver' of https://gitlab.com/eliggett/wfview into wfserver

[33mcommit 1f25ca67e8cff514b74c318f863cac6cf63dd54b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 14:40:05 2022 +0100

    Fixes to TCP server and attempt auto port detection on non-linux!

[33mcommit 2ee20d9415c8e290e6603d25c4bdaee9a7e3aa72[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 12:12:31 2022 +0100

    Fix max build

[33mcommit 0aaf5de9764bbd71d8ee51a321a9c1ace7951003[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 11:22:33 2022 +0100

    Hopefully fix silly retransmit bug

[33mcommit 87b1a9f84d2367a3b33895fc4baed96e0bf2fe4b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 09:58:58 2022 +0100

    Change some log messages

[33mcommit 23b37f27543197dc9d55bf5069518248dd3e8e87[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 8 09:53:02 2022 +0100

    Attempt to fix weird retransmission bug

[33mcommit 2ca376a3c19eeb524c714534bd4d7db329e458e8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 19:23:38 2022 +0100

    Opus now should work with any native sample rate/type

[33mcommit 4f5e4addb6316ceb7f0df28b2c5f2b6a36401417[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 19:04:49 2022 +0100

    Update audiohandler.cpp

[33mcommit 86237afd8b1922a0efeaadaa325ecb7c58b920d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 18:57:42 2022 +0100

    Update audiohandler.cpp

[33mcommit c027413ee2b44e9174107f8811614bdddc11d0f7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 18:52:43 2022 +0100

    Update audiohandler.cpp

[33mcommit 2a889488c4cbb09366dd85cf024531ad071aa5ab[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 18:48:47 2022 +0100

    Update audiohandler.cpp

[33mcommit e4d5efb3d586ce16376e2db6d9d1a46c4169be28[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 18:37:47 2022 +0100

    Update audiohandler.cpp

[33mcommit d543f1474d4150471166e5adb8004bd5855b15a1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 18:35:44 2022 +0100

    Update audiohandler.cpp

[33mcommit a21a44df865549ec916c107ee1ec7263f8425e87[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 17:35:58 2022 +0100

    Opus issue

[33mcommit 53cae9173dd601eccbf6610758f7d28eab2ca6e6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 7 16:03:50 2022 +0100

    Use floating point opus encoder/decoder

[33mcommit 0f7a5566fc10ca9393bc5d4760f0a0b0e07cb710[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 6 22:02:43 2022 +0100

    Fix underrun handler

[33mcommit f00051ecd47cc998b01f3536b3326f91ec0a4c49[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 6 21:52:22 2022 +0100

    Use signal/slot for audio signal reporting

[33mcommit cfc22dcb30b84dcaa999b3a64e78c9986ecec30d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 6 19:40:14 2022 +0100

    Allow for 8bit signed (in case computer uses it!)

[33mcommit 833371530ac4f06b92c70aca27514d551f1548e6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 6 19:39:43 2022 +0100

    Enable High DPI Scaling

[33mcommit 47772a4925a451a853a06519c23def7be39e5f84[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 5 19:02:29 2022 +0100

    Few more audio fixes

[33mcommit 38fdec3da6f69bf95f902048624cdab843488d5a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 5 16:47:43 2022 +0100

    More tidying and use float resampler

[33mcommit 45ac1fbe1c48b90d369820fdde72f52d6eb17ae1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 19:22:11 2022 +0100

    Remove ringbuffer as no longer needed

[33mcommit ac70ba5f8324e12c71e2ab54f999d0c4a32694aa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 11:52:59 2022 +0100

    Add debug to make sure buffer size is really changing

[33mcommit 2cc875ff7cc3b245e99d41c79d64907fcb42fa6e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 11:47:55 2022 +0100

    Try to fix linux audio stopping on latency change

[33mcommit 64b4ef2019d11c2b67bdbbd2c626ba6be47c1a88[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 11:12:06 2022 +0100

    Few more fixes and move windows builds into separate directories

[33mcommit cf7a947beb7751371e77958c5c6fde495cade0ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 00:37:07 2022 +0100

    Remove debugging message

[33mcommit 5c2d6e57b2df88217359b00396f7a18212775f44[m
Author: M0VSE <phil@m0vse.uk>
Date:   Mon Apr 4 00:23:23 2022 +0100

    Fix linux compile

[33mcommit 83c494ecc1dafb3bbd134615ae80cc23626d1e1d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 4 00:01:08 2022 +0100

    Remove rtaudio/portaudio for now

[33mcommit 389f661c79ab248596539eb5bd8c5dc84824a218[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 3 20:16:52 2022 +0100

    Working (in Windows) audio output

[33mcommit 8ec62fec8d19380e07fee11d16de8deaed1de97b[m
Merge: acf4c1b e5b9750
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 23 17:28:11 2022 +0000

    Merge branch 'master' into wfserver

[33mcommit acf4c1bf63df2f81333d369267e198f6723f9c64[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 23 17:27:47 2022 +0000

    Start of test to include Eigen

[33mcommit e5b975038c4018bb41974d55da617757fae299c9[m
Merge: 6bd1395 32f438a
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 23 15:45:51 2022 +0000

    Merge branch 'fix/typos' into 'master'
    
    Fix spelling errors
    
    See merge request eliggett/wfview!8

[33mcommit 32f438aa01bdbc5c56387dbbd4e566d6f30b5541[m
Author: Daniele Forsi <dforsi@gmail.com>
Date:   Wed Mar 23 15:45:51 2022 +0000

    Fix spelling errors

[33mcommit 6bd1395c1a7a6b00ec8d662935bfdd9e3c481b1d[m
Merge: 440429b 7266331
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 23 15:45:23 2022 +0000

    Merge branch 'master' into 'master'
    
    Fixed broken implementation of "set_level RFPOWER" in rigctld
    
    See merge request eliggett/wfview!11

[33mcommit c5cf0fdf57ccf2f4419ff0d1e45f68e77224e7b4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 23 13:19:05 2022 +0000

    Slightly better tcp server implementation (still needs UI adding)

[33mcommit bfd9ddea52a2e92976239f928fa954a2792b354a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 23 10:12:42 2022 +0000

    Initial tcpserver support (needs work)

[33mcommit 72663310c5ddfadcf8aec1477e9488678f069edd[m
Author: Russ Woodman - K5TUX <k5tux@k5tux.us>
Date:   Sat Mar 19 14:05:46 2022 -0500

    Fixed broken implementation of "set_level RFPOWER" in rigctld

[33mcommit ece5933d55c62a34c6d49168f9e2ebd7f0824ad7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 18 15:41:11 2022 +0000

    Some extra debugging

[33mcommit 38274cc3c8a70ac0b65f27d2a57a35b059929754[m
Author: Philip Kubat <pkubat@gmail.com>
Date:   Mon Feb 7 20:31:29 2022 -0500

    Added IC-746

[33mcommit 468cfa13034db29a7edd44d0cc742169b3742c0e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 2 16:26:40 2022 +0000

    Add metahandler

[33mcommit feb5d13d135f4fe590d34a50122f5a8db152eea6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 2 16:07:03 2022 +0000

    Add some debugging to find audio/serial ports

[33mcommit 824a91b1258707ba00f86b50957ea4676d519aa9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 2 16:02:40 2022 +0000

    Few more fixes

[33mcommit 21a8bbc1a62aec595cca0b4b8fe7b9ddf5a46776[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 30 10:55:12 2022 +0000

    Qt 5.9 compatibility

[33mcommit 156d03706aec8083cffe331c44936e9cc183ee39[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 30 10:52:40 2022 +0000

    Update udpserver.cpp

[33mcommit 46cd6c1e5336af032e0c3bdde5095317a4fa656a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 30 10:29:23 2022 +0000

    Hopefully fix Linux compile error

[33mcommit 9ae8bc660c2c22b4c887e33997f58921fabb8540[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 29 22:50:58 2022 +0000

    Lots more fixes and tweaks

[33mcommit dd9be7c99ceb0576c5de837bf44b526c2118db50[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 28 09:52:26 2022 +0000

    Tuning lost packets

[33mcommit 30d5858663edd0ab5433bbe9f0a38b7345356fa5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 28 09:43:53 2022 +0000

    Max missing packets slightly less than buffer size

[33mcommit c0fabb3999f20044339f4686e85d55f9f449a8b3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:51:25 2022 +0000

    Update udphandler.cpp

[33mcommit 6427be05b824e9d97e05324e17f369cbe027d781[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:47:17 2022 +0000

    Update udphandler.cpp

[33mcommit be7dbbb39586967593441e5f684229c6b3ea1444[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:40:29 2022 +0000

    Missing ;

[33mcommit 9d2801a5054e78665fe1d8b77475529aa8d4fce1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:39:45 2022 +0000

    Improve congestion calculation

[33mcommit 3568c80fcc695a0cfeba22a9c68190b4df03ed84[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:28:34 2022 +0000

    Fix mutex

[33mcommit fe7bdf134538e382549b6dbc592e4820a12bc041[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:24:54 2022 +0000

    Flush buffers on excessive missing

[33mcommit d29c1ddba771bb951ac13fe36c92c69d2058cfd2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 23:12:07 2022 +0000

    More retransmit fixes

[33mcommit 3d1e04c556f5246b6628e81c93ad0abbfae32ec7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 22:52:50 2022 +0000

    Fix compile issue

[33mcommit 77b7462f74c1072bb23f947b5d1cb34923335b53[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 22:51:28 2022 +0000

    Restore partial QT5.9 compatibility

[33mcommit bce66393fb063b19f5c0052bb4a50d3726ec6c57[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 22:44:30 2022 +0000

    Remove unnecessary code

[33mcommit 2f4fe061b3c38896cdf5b4f34ea8a9e24e37ea1d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 27 19:11:16 2022 +0000

    Improve missing packet handling

[33mcommit 4a1be30c40d0d611806bc63a13f2c7f540153ea5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 26 09:49:52 2022 +0000

    Non-working standalone server

[33mcommit 5ae3549ba50e19784bbd0e4499352de525d42260[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 23 17:54:40 2022 +0000

    Treat GUID as 16 bytes rather than trying to be clever!

[33mcommit 87a36426cf0c25d4cae95f6eb3ffd282605dac56[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 23 16:43:58 2022 +0000

    Fix some compile warnings

[33mcommit 264ad231c0a40e59bbab1bdd98b088f72212d893[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 23 15:06:31 2022 +0000

    Now supports multiple radios on OEM server

[33mcommit ff47fbd82f7eb6e57c30640764055e675f00c0db[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 22:53:06 2022 +0000

    Make radio selection visible when there is more than 1 radio.

[33mcommit 1c8bc62fcca067e0c394531a4d71ded971971979[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 18:49:07 2022 +0000

    More linux build fixes

[33mcommit 45e074783acc3668894f5b4231fcb2201212abcd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 18:32:08 2022 +0000

    Hopefully fix linux compile

[33mcommit 3ff6e7180a0914462466a07af27df41a811de89d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 15:32:53 2022 +0000

    Add radio_cap_packet MetaType for server

[33mcommit 88d2124f358fcb021fcbd1c4f69175891c0c34e7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 15:28:41 2022 +0000

    Add MetaType for radio_cap_packet

[33mcommit 39540612c7d86f7b4c2e3e00b0aca33c2d4643d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 22 15:12:36 2022 +0000

    More multi-radio support (nearly working!)

[33mcommit 96de9c55fa3e355b1f8019d7513b83166a96227e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 21 23:58:18 2022 +0000

    More work on multi-radio support

[33mcommit 41e90bb30975bec82e7558caf89c754a31731d71[m
Merge: fcc7c9a 440429b
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 21 19:23:45 2022 +0000

    Merge branch 'master' into wfserver

[33mcommit fcc7c9a5dc49a0c59e2a30bd67d5121d6c819c94[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 21 19:23:32 2022 +0000

    Lots of changes, mainly for multi-radio support

[33mcommit 440429be9f1abf457e9a033d72d7faaf238166c1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jan 20 23:26:46 2022 -0800

    Adjusted window size for radios without spectrum. Thanks K5TUX.

[33mcommit e4cc2962b3cbdd5305f5118345323051591c088f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 09:06:23 2022 +0000

    Fix wfserver linux build

[33mcommit 4e086ac2200620d1c7b3d14c7b89d3457444cb91[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 09:03:41 2022 +0000

    Fix building both wfview and wfserver together

[33mcommit 9f059c9e7376c8540f798d4560f9d93763434799[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 08:58:02 2022 +0000

    Fix portaudio build

[33mcommit bcadb2176bda078a7f03db8f03e44de030ce4cc4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 00:18:52 2022 +0000

    Remove unnecessary signal/slots

[33mcommit 55baec6100095dd45669a0774caf42ead6bc720b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 00:14:37 2022 +0000

    Only show server status message once

[33mcommit 4ebfe457c75721f99cd16067bf0bc5503edb9380[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 18 00:11:15 2022 +0000

    Tidying of server code

[33mcommit 387b26187de42a78a33edb1d79f915736e9a4ccb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jan 17 17:23:55 2022 +0000

    Initial server commit

[33mcommit 28812be8bef2544bf1d2fdc2155d722461ed34d7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 16 19:17:32 2022 +0000

    Buffer for correct latency

[33mcommit b87e0de05d73a26927c2182445d8528486dc5bfb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 16 19:04:44 2022 +0000

    Force refilling buffer once it has been flushed.

[33mcommit b691398f6a9dcf93568697adbb527ba2dfb8ecd9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 16 18:54:41 2022 +0000

    Increase audio buffer size

[33mcommit 914477d1dcc72b4b93c80dac739132352cb3b6b5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 16 18:47:13 2022 +0000

    Tidy code after merge

[33mcommit ada263efedf4e609081769adfb1d48cf31d1d71c[m
Merge: 1611058 a75c6e0
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jan 16 18:28:36 2022 +0000

    Merge branch 'master' into audio-enhance

[33mcommit a75c6e0fdf2025b1d0ef69b347b75941e1fd4493[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jan 15 16:31:50 2022 +0000

    Zero audio to stop blip at startup

[33mcommit 01ea44df0b5b58b40913e40623ed0570f6480e79[m
Merge: 3abb4bc f8351ad
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jan 15 16:01:47 2022 +0100

    Merge branch 'master' into settings

[33mcommit 3abb4bc58464bdd8c24c47e8e2f826f8c0f40adc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:58:06 2022 +0000

    Revert "Try to send outgoing audio every 10ms"
    
    This reverts commit b8f6feacbf6cbd272b727ddf6833c7b801b4797e.

[33mcommit b8f6feacbf6cbd272b727ddf6833c7b801b4797e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:55:41 2022 +0000

    Try to send outgoing audio every 10ms

[33mcommit 03799b2cda34735497bbd73a86d9f26ae6a9a5ef[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:52:25 2022 +0000

    More fixes

[33mcommit dcb47127405efebebf579a086aec1506b245e911[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:41:56 2022 +0000

    Move buffering code

[33mcommit 7d5ec8a0658fa4fcc643748dfb935efdaff7f829[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:29:27 2022 +0000

    Check when audio has been buffered

[33mcommit b4884e773a059ce6245aa45e9474259a2dec90d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:23:59 2022 +0000

    Ensure buffer is at least 1/2 filled before starting

[33mcommit 918238835d664883fc3dd7f929d339d7bcd99e3e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 20:12:50 2022 +0000

    Catch excessive missing packets

[33mcommit 2eace96be9754e7b403d0400dee058882378bc07[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 19:57:12 2022 +0000

    Set tx/rx gain in server

[33mcommit ed7d88eb1ec5aae0c0570bbadf5361fb1645e2ad[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 19:43:48 2022 +0000

    Fix that didn't work!

[33mcommit d9a611e118244216b1ecd7388ff239e5d13553ee[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 19:40:25 2022 +0000

    Set latency for tx and rx

[33mcommit 4a200006eb357b9aa8f6eef3f99214dc4f7293c7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 18:57:20 2022 +0000

    Extra audio debugging

[33mcommit 02f222355e3c31905c01ced2a50a81745406afbf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 16:23:01 2022 +0000

    Set latency for tx and rx

[33mcommit 5a9342d9a9cdfd0c46251509056c9fce8d0c4ef4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 16:21:32 2022 +0000

    Clear audio buffer if it's full

[33mcommit 94a89dea33c53f69f3dcb37f7c11f56941e062f9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 16:00:05 2022 +0000

    Clear audio buffer if too many packets are delayed

[33mcommit 317cbd640a7159b941ce13aa227480b415153825[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 15:49:35 2022 +0000

    Minor fix to retransmit handling

[33mcommit e00b598fd9d592414ae4fe81f893dc2bfd7d6821[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 14 14:10:21 2022 +0000

    Hopefully improve retransmit search

[33mcommit ce0544ac6a6dabc2f32b40b8b5232da64a1183d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 21:53:53 2022 +0000

    Ignore single missing audio packet

[33mcommit 4696fe68246400feb28a23003fcc8a6d90f3869f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 21:50:43 2022 +0000

    Set buffer size with a #define

[33mcommit 99ca97370b5d79f4c0c8c690cba97dad7bec3e59[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 21:12:27 2022 +0000

    Correctly remove items from the buffers!

[33mcommit 1291990d54fd97e68e22364a1bb8b6d807e5472e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 20:26:42 2022 +0000

    Reduce number of mutex locks during retransmit processing

[33mcommit 4fa83b79f0ace1d3278b6bcd3a6e9b29ebd7d402[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 20:16:14 2022 +0000

    More debugging to isolate audio stalling

[33mcommit 184af7a582496c1b21f115673077c63e7f196654[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 19:54:43 2022 +0000

    Add extra debugging

[33mcommit d4868ae14c531a60f154f6dde00afe42a71fd168[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 19:34:34 2022 +0000

    Latency on server is wrong way round!

[33mcommit e00fa2622915404ed7ab2d966f654e9289a821c6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 13 11:17:13 2022 +0000

    trying to find the cause of server audio issue

[33mcommit 5a92c071d1252abd575ed7fbfeee275f5fd2a965[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 12 20:01:17 2022 +0000

    Set audio send period on server

[33mcommit f8351ad6100452200db05c169c34253b8cce060c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:14 2022 +0100

    Revert "Beginning of the new Settings tab. Does not compile as-is yet."
    
    This reverts commit 0c807f54c2a752f760da65e431c09f06d04a7674.

[33mcommit 9cbcb3aa1cd407f0a3f7a2b8f500f5561052b5e1[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:12 2022 +0100

    Revert "Brought back the Save Settings button"
    
    This reverts commit 888824f9a4d0991786899dca86fddf1f5d3fc50a.

[33mcommit b85c268f2c5f79d60ee8ec3044221b0c3b03600b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:11 2022 +0100

    Revert "Added more pages."
    
    This reverts commit 700ac53b288fc987f009c9d421192f08ea63d1be.

[33mcommit ae5684238c55f8851b217c9d1f321fe91edab83d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:09 2022 +0100

    Revert "Working preferences with a list."
    
    This reverts commit 26f15cc9dbe35b58fca01108b52c355d4a8f0ab4.

[33mcommit 0a42db97c200ccbfd1be6b9d32896fade7ff45ed[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:06 2022 +0100

    Revert "Changed width of list, other minor tweaks."
    
    This reverts commit 6b30cb53bc0cac03cb599b9f5fbb9d30a5ca7de8.

[33mcommit 942d7beb9996267bd5cd88f502422f65850ec08d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:04 2022 +0100

    Revert "Added clock and UTC toggle."
    
    This reverts commit 314d78ad051081c0ced4c843a4574ccf743fab7b.

[33mcommit 1667df89baeff36f1a13d3b27cfb2f36fa628be3[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:19:03 2022 +0100

    Revert "Slightly wider radio connection box"
    
    This reverts commit 7564239a68c90fc7863758b29c1cc3718dea7d61.

[33mcommit 91d90f2ae688ca13b769cf3c9ab2a4bab7487286[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:58 2022 +0100

    Revert "Changed the order of the mod source combos"
    
    This reverts commit 9a781c671ec8cb9aa8c7b818c3c2367cdd0d38e6.

[33mcommit 837cc5fa8b79aa249c3d0b0135d9c78d0107a6bf[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:57 2022 +0100

    Revert "Larger vsp combo box... seems like it resizes though?"
    
    This reverts commit 37a2c1caf398d2919e97ea209f8d61a73059cc3c.

[33mcommit eaa7a8edad2cc8ef41fbe94ab226d898791ea7dc[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:55 2022 +0100

    Revert "Change to width of group box"
    
    This reverts commit c860fa77a161e52fcb04ab0693fd218cfa99a244.

[33mcommit 809f67ad19490a18e8bac146f309d9c877d8bd8c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:54 2022 +0100

    Revert "Changed the enable/disable for some network UI elements."
    
    This reverts commit 7977de42d9e4c7c4bb25a770e1bce949701e1f45.

[33mcommit 4f34e54b02ccd78373a16710b9c26b5f7aba7b45[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:53 2022 +0100

    Revert "A little more on the enable/disable UI elements. Should be good."
    
    This reverts commit 8e95919aa92b4d66b65ff848969bb096e50d47f5.

[33mcommit 9dd9293036d52c852ba5b2ce18fe7ed57a8760a1[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:52 2022 +0100

    Revert "Masked password input after focus change."
    
    This reverts commit 605a87dec9087b78c5c1420444b0c76ca0cb4d96.

[33mcommit d1d0194600f6c7145ecda4d75ec2e02a42ced07e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:50 2022 +0100

    Revert "Integrate server setup into new settings pages"
    
    This reverts commit e1cdcad65baaef93b5d80a1492b86d302f21a0ea.

[33mcommit 54d08c5b8a9b27c2db7bbc9768df6601c05c3ed2[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:35 2022 +0100

    Revert "Edited some size constraints for better resizing and full-screen mode."
    
    This reverts commit c8b4992bab0a676852e1ca485cd5413a46415a9e.

[33mcommit 41ec08ecba5481742a7e24c70149c84e40638d23[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:34 2022 +0100

    Revert "Minor typo with the new audio selection combo box, only on PORTAUDIO"
    
    This reverts commit d1165980a93ecd1839fc443e7f5bc9389cc0995d.

[33mcommit a231cb086f7a9ccbede8f7d541d6c70b98c9ca00[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:32 2022 +0100

    Revert "Added forced manual RTS setting"
    
    This reverts commit 8dd42ba39224623f2881e760176637ebc1a2fa66.

[33mcommit d742c7564a3c020d1880b1dd925a9a2fb15e237b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:30 2022 +0100

    Revert "Allow dynamic restarting of server"
    
    This reverts commit 3f06ab60612edc3d98988de7964b81046830fd54.

[33mcommit 9e87d35069e1abe6de3ceb70d54b38a53cd96c82[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:29 2022 +0100

    Revert "Clear server status when disabled"
    
    This reverts commit 2c82561ff57f444f21ffb02e5ec7afd58684e853.

[33mcommit 91f4269de8e51ad0f64c08e8c4707e9c225f91ad[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:27 2022 +0100

    Revert "Update server users table dynamically"
    
    This reverts commit cf1caf0a0f1caca88468ab3961f6c809844b6a3d.

[33mcommit f36f4db49795345bbbddee8c0b98302db4259f8e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jan 12 19:18:19 2022 +0100

    Revert "Fix typo for portaudio/rtaudio builds"
    
    This reverts commit daf94543acd337e25d57282b29736ee5c61fbfc0.

[33mcommit 89ac99b3f9dd522fb8a803997d3ff66d7e002bb8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 14:52:33 2022 +0000

    Bit of tidying

[33mcommit 82b655cc0986f18b91af5dce1219c243ae0729da[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 14:42:32 2022 +0000

    Try to fix lock condition in server

[33mcommit db702123f9083f69cf8ac520256abf1f6d10e4f7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 12:31:15 2022 +0000

    Keep audio mutex until all processing is done

[33mcommit 6e44480d6b9a55d50a0d7b7afe009be6f9f0fa8e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 12:28:43 2022 +0000

    Revert "Tidying of audio handling"
    
    This reverts commit 5a6ff8489249e260a76c496d6318281a6b65001f.

[33mcommit 3036108d6f094fc28b07f79ab81dfb4c1c5dce08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 12:28:30 2022 +0000

    Revert "Hopefully fix opus issue in last commit?"
    
    This reverts commit 7dd325c3ae510bf89a3617b340be71d78b6d3523.

[33mcommit 7dd325c3ae510bf89a3617b340be71d78b6d3523[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 12:16:33 2022 +0000

    Hopefully fix opus issue in last commit?

[33mcommit 5a6ff8489249e260a76c496d6318281a6b65001f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jan 7 12:09:36 2022 +0000

    Tidying of audio handling

[33mcommit 6c29eb12a25ae4ba830533b838341834bd4b5b26[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 6 13:44:29 2022 +0000

    Improve FEC operation with opus

[33mcommit 9284a494697fd0a784e66d8eabe4aed619bc1ab9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 6 10:08:36 2022 +0000

    Try again to fix server restart!

[33mcommit 43fb31e8d008973c750f75651e39bed8807011ea[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 6 10:03:58 2022 +0000

    reconnect server signal/slots after restart

[33mcommit 5e2c77b87252760bdf9bcf01a82a7ff73ee3e1a1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jan 6 09:53:47 2022 +0000

    Spotted another error in audio settings.

[33mcommit 3035e5c712cfbd68dc27699864dc48375884608b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 5 15:27:46 2022 +0000

    Attempt to enable FEC, not sure if it works though?

[33mcommit 8bf107fadc68b62134c690f0372f8cb58832eafb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 5 14:53:12 2022 +0000

    Fix a few compile warnings

[33mcommit 9b8221b32e1cc8d28da00eae8603c2de4603fc26[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 5 14:45:34 2022 +0000

    Ignore wrong ping seq

[33mcommit f77defd9d75f7a10d3c2bbac0a5b7a863e152be0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jan 5 14:40:40 2022 +0000

    Fix silly typo which was causing audio crash!

[33mcommit a91450c90b3f6d92dbb7c68cbeae6d83d4c9adf5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 21:30:48 2022 +0000

    Maybe fix server audio issue?

[33mcommit 1611058f7755f078fb76f96fac7dde0edffd85d2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 21:26:03 2022 +0000

    Maybe fix audio crash?

[33mcommit 18646ab0cf5030ecf71fb7d7c8116159248860e4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 19:55:48 2022 +0000

    Add SERVERCONFIG metatype

[33mcommit fce3a3d6be3fdab54d0549922880bde73fe34915[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 19:50:38 2022 +0000

    Try to fix server

[33mcommit 8ec82e74069c3381ff8eb8495aff8e6bc904e26c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 19:04:36 2022 +0000

    Various tidying in server and client code

[33mcommit 360ceccdc6f3700c7c2faf9b691f393a5737e722[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 18:42:47 2022 +0000

    Update wfmain.cpp

[33mcommit 93e2c5529e31ea7465d2b85a1e349807d4f769ad[m
Merge: 6f719d8 daf9454
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 18:40:44 2022 +0000

    Merge branch 'settings' into audio-enhance

[33mcommit 6f719d8961d0658599773abea760e298dd2bd058[m
Merge: ff6d0a6 6011816
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 18:37:07 2022 +0000

    Merge branch 'rigctl-enhancement' into audio-enhance

[33mcommit ff6d0a6a65d040967564205de44af26417407e5e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jan 4 18:34:34 2022 +0000

    Merge settings branch

[33mcommit 88ebf74e14447da199b3ecee4ce200e14cd25dc9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Dec 31 12:06:17 2021 +0000

    Fixes to allow both QT5 and QT6 to compile

[33mcommit 4203b62327be2f9ec4aa226468c253be2c819411[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Dec 31 00:34:51 2021 +0000

    Quick fixes for v6.2

[33mcommit 60a1d405497e02bacbf2756fe96c11c99c6490fa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Dec 31 00:30:22 2021 +0000

    Fixes to maintain QT6.2 support

[33mcommit 2a0b79449dff1dbbaad143afe48f8a17d2f0cc5e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Dec 31 00:03:05 2021 +0000

    Merge

[33mcommit 7d95f2d863643c8638b3a6da7c8568fd72f6af17[m
Merge: 0d9c7b5 daf9454
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Dec 31 00:02:47 2021 +0000

    Merge latest settings changes

[33mcommit daf94543acd337e25d57282b29736ee5c61fbfc0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 30 18:27:22 2021 +0000

    Fix typo for portaudio/rtaudio builds

[33mcommit cf1caf0a0f1caca88468ab3961f6c809844b6a3d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 30 18:22:34 2021 +0000

    Update server users table dynamically

[33mcommit 2c82561ff57f444f21ffb02e5ec7afd58684e853[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 30 11:24:44 2021 +0000

    Clear server status when disabled

[33mcommit 3f06ab60612edc3d98988de7964b81046830fd54[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 30 11:17:54 2021 +0000

    Allow dynamic restarting of server

[33mcommit 8dd42ba39224623f2881e760176637ebc1a2fa66[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 30 01:52:23 2021 -0800

    Added forced manual RTS setting

[33mcommit d1165980a93ecd1839fc443e7f5bc9389cc0995d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Dec 29 19:49:16 2021 -0800

    Minor typo with the new audio selection combo box, only on PORTAUDIO
    builds.

[33mcommit c8b4992bab0a676852e1ca485cd5413a46415a9e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Dec 29 17:52:35 2021 -0800

    Edited some size constraints for better resizing and full-screen mode.

[33mcommit e1cdcad65baaef93b5d80a1492b86d302f21a0ea[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Dec 30 00:48:39 2021 +0000

    Integrate server setup into new settings pages

[33mcommit 605a87dec9087b78c5c1420444b0c76ca0cb4d96[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Dec 24 08:21:50 2021 -0800

    Masked password input after focus change.

[33mcommit 8e95919aa92b4d66b65ff848969bb096e50d47f5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 21:04:53 2021 -0800

    A little more on the enable/disable UI elements. Should be good.

[33mcommit 7977de42d9e4c7c4bb25a770e1bce949701e1f45[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 21:02:35 2021 -0800

    Changed the enable/disable for some network UI elements.

[33mcommit c860fa77a161e52fcb04ab0693fd218cfa99a244[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 20:55:48 2021 -0800

    Change to width of group box

[33mcommit 37a2c1caf398d2919e97ea209f8d61a73059cc3c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 20:45:58 2021 -0800

    Larger vsp combo box... seems like it resizes though?

[33mcommit 9a781c671ec8cb9aa8c7b818c3c2367cdd0d38e6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 20:26:53 2021 -0800

    Changed the order of the mod source combos

[33mcommit 7564239a68c90fc7863758b29c1cc3718dea7d61[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 20:15:39 2021 -0800

    Slightly wider radio connection box

[33mcommit 314d78ad051081c0ced4c843a4574ccf743fab7b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 20:05:34 2021 -0800

    Added clock and UTC toggle.

[33mcommit 6b30cb53bc0cac03cb599b9f5fbb9d30a5ca7de8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 19:52:56 2021 -0800

    Changed width of list, other minor tweaks.

[33mcommit 26f15cc9dbe35b58fca01108b52c355d4a8f0ab4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 19:45:58 2021 -0800

    Working preferences with a list.

[33mcommit 700ac53b288fc987f009c9d421192f08ea63d1be[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 19:34:45 2021 -0800

    Added more pages.

[33mcommit 888824f9a4d0991786899dca86fddf1f5d3fc50a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 16:52:29 2021 -0800

    Brought back the Save Settings button

[33mcommit 0c807f54c2a752f760da65e431c09f06d04a7674[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 23 14:30:58 2021 -0800

    Beginning of the new Settings tab. Does not compile as-is yet.

[33mcommit 4be3398dfa9abc573f84a8ec793d66dd679fdc8c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Dec 20 14:40:41 2021 -0800

    About box updated to include Patreon site and to indicate path for
    stylesheet only as applicable.

[33mcommit 1dc5cc042816b281b733d327ed1f4b692f37d38a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Dec 11 10:49:06 2021 +0100

    added 500 Hz step for VFO

[33mcommit da9f60e0bc2a012f3e7d591a555101f9079a59f3[m
Merge: e729c34 6011816
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Dec 10 12:09:32 2021 +0100

    Merge branch 'rigctl-enhancement'

[33mcommit 60118169e1026456abd0633003dbd7d1bde37344[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Dec 7 14:52:47 2021 +0000

    Properly handle different rigctld client versions

[33mcommit 247817077c69bfb9c80eac8e505802c0307c0c7d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Dec 7 12:32:51 2021 +0000

    Add RIT function and other rigctl fixes

[33mcommit e729c34ec5010985af4466457533a9a489811b5d[m
Merge: 55cda83 81c9563
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Dec 6 17:41:55 2021 +0100

    Merge branch 'rigctl-enhancement'

[33mcommit 55cda83ec242e2ad614fc72188161d47b51e33d2[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Dec 6 17:41:11 2021 +0100

    WHATSNEW updated

[33mcommit 81c9563f1679dfad95022336288c34e5fef30bf4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Dec 4 19:21:23 2021 +0000

    Various changes and fixes to rigctld/rigstate

[33mcommit e104262bff87f0e4f80e665cdf7e11b963d32328[m
Merge: d71fb9e 43119e1
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Dec 4 17:22:36 2021 +0000

    Merge branch 'master' into rigctl-enhancement

[33mcommit 43119e101c3b61d3630cde44df0eb9d006eea6f2[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Dec 4 18:19:01 2021 +0100

    removed another swapfile (some day I'll learn..) and normalized WHATSNEW

[33mcommit 0d9dfed85eb5ef1d66bca92dceb4873247b21db5[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Dec 4 18:09:32 2021 +0100

    WHATSNEW and CHANGELOG updated

[33mcommit 0d9c7b5e308553e9f41ef5536f5cae183d082d11[m
Merge: 4743cd1 b1ef680
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 1 10:24:42 2021 +0000

    Merge branch 'master' into QT6.2

[33mcommit d71fb9ef4a3c76871243d334b2361e8b32a40001[m
Merge: 47b439b b1ef680
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 1 10:22:29 2021 +0000

    Merge branch 'master' into rigctl-enhancement

[33mcommit b1ef680dbf71edefbbef63125acc754df46dc499[m[33m ([m[1;33mtag: 1.2d[m[33m)[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 1 10:21:40 2021 +0000

    Another "minor" update for RX only rigs

[33mcommit f67bed583226c01feb67ca77041865cec87f11f5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 1 10:13:21 2021 +0000

    Disable certain TX commands for RX only rigs

[33mcommit 47b439b74166a1167482a80fd3a9d362e57b91c8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Dec 1 10:01:05 2021 +0000

    Rewrite rigstate (again!)

[33mcommit 104b3955452767f75172c7750a7ce3f6f27c1b2e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Nov 26 18:05:32 2021 +0100

    missing space

[33mcommit 66734b3be05ca6ecad1f92519dbc3c05eec3a470[m
Merge: ac72eed 9ba216f
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Nov 23 08:19:17 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit 9ca9379f88c4f3a7301c270cdf6e694b04620d82[m
Merge: 10fc10e ac72eed
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Nov 23 00:40:46 2021 +0000

    Merge branch 'ui-enhance' into rigctl-enhancement

[33mcommit 10fc10ef82aef654f6b258378eb2526b0a2218ed[m
Merge: 8ddf688 9ff394e
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Nov 23 00:40:05 2021 +0000

    Merge branch 'master' into rigctl-enhancement

[33mcommit 8ddf6880ee6915b9ae473be86c54b6ff400ca66c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Nov 23 00:39:36 2021 +0000

    Windows support files

[33mcommit c5d2ecb79333fa5565a4891cc92414e5a91ea88d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Nov 23 00:39:10 2021 +0000

    Change rigstate to a class

[33mcommit ac72eed7c8d5c8b190fd46d9c4e7767cd80f3d3f[m
Merge: 13af5c0 9ff394e
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Nov 22 13:36:54 2021 -0800

    Merge remote-tracking branch 'origin/master' into ui-enhance
    Also added in changes for the size of the combo audio source boxes.

[33mcommit 9ba216f1d94f9bbf4d686d3c4c43835504139e7b[m
Merge: ee6889d b0ffc94
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 22 10:37:38 2021 +0000

    Merge branch 'audio-enhance' of https://gitlab.com/eliggett/wfview into audio-enhance

[33mcommit ee6889d163ceba36264aff757704134c2c14e4cf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 22 10:37:21 2021 +0000

    Add mutex within rigState to protect access

[33mcommit 9ff394e0cdb6e7480314337abec888b97cc458e8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Nov 19 20:50:53 2021 +0100

    changed debug command

[33mcommit a4a9297747980d91146cf4153bcf6d7716cc549f[m
Merge: 1f4878d b0ffc94
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Nov 19 20:48:05 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit b0ffc94ff977d74b9c774dad00ed2a8d2183b825[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Nov 19 18:48:15 2021 +0000

    Add more version info

[33mcommit 1f4878d4537ae31b6347f9f0598d7e4923e9c9a5[m
Merge: 8908f5a d86eb07
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Nov 19 19:29:40 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit d86eb078326789b1e3c052f972139a5d116abbec[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Nov 19 18:26:45 2021 +0000

    Fix WFVIEW_VERSION in mac/linux

[33mcommit 8908f5a34c2c73a941eddd7aede3ac8f6a27a49c[m
Merge: cbaac5f 39f0741
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Nov 19 19:01:30 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit 39f07414eecd6a2a20ea92416a78df23b574e325[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Nov 19 17:52:18 2021 +0000

    Add --version command line argument and WFVIEW_VERSION #define

[33mcommit 13af5c0b1ba797a31f86e095da92e8309e837844[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 18 13:13:54 2021 -0800

    A little less green in our gray.

[33mcommit 1a03214f59e574be0006c7860a2c325cf94a59c9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 18 13:12:12 2021 -0800

    Added size rules for audio source combo boxes.

[33mcommit cbaac5fd2658ffff9540dcbd58fbcf12158d9413[m
Merge: 62087bf 62771f6
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Nov 18 18:51:19 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit 62087bfd5b9ef5e3e1e3297df1df50e0f8b703b0[m
Merge: 1503ecd 130d105
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Nov 18 18:50:28 2021 +0100

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 62771f6b590e5aa46e4a4891d51c662d01978ec4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 14:09:09 2021 +0000

    Fix silly bug in retransmit code

[33mcommit 130d10553539e358ecc0e5603e5d1ea1137a90f5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 14:09:09 2021 +0000

    Fix silly bug in retransmit code

[33mcommit 4743cd126b9437162e37ba8ef05697e73fe8a357[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 14:09:09 2021 +0000

    Fix silly bug in retransmit code

[33mcommit 9f0673ae717535f685f8fd30ae1383dd8efdab20[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 14:08:40 2021 +0000

    Fixes to tidy-up QT6.2 support

[33mcommit 395469daf32337fa65fb2c051d8a01f15a210029[m
Merge: b7407fc cc2f2b2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 12:23:27 2021 +0000

    Merge branch 'audio-enhance' into QT6.2

[33mcommit cc2f2b2576331000297cd58dccec0622b42edd6d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Nov 18 12:20:36 2021 +0000

    Remove some extra logging from audio

[33mcommit 42675ae770ad79920f7914265c33cdf279b12ff6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 15 19:28:44 2021 +0000

    Add mutex for incoming audio on udp and server

[33mcommit b5591e08674bd3883f002e2da219834f2d933b20[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 15 15:26:18 2021 +0000

    Force PA to use 48K Sample Rate if default is 44.1K

[33mcommit 04526db635c023a0c47f4155709a064fd5595b35[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 15 15:02:00 2021 +0000

    Try using slot for audio again

[33mcommit 0cc2945d28792959094fdcf9bf94bd2f14e155b6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Nov 10 20:45:59 2021 +0000

    Server only tries mutex lock for 10ms before giving up.

[33mcommit 1503ecd9f74303243444d349d7159f9c4b86eb12[m
Merge: a2cfc8c c99439e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Nov 10 20:32:26 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit c99439e759ecb20c6454f24ad7e57bd59ec88c45[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Nov 10 18:07:48 2021 +0000

    Check number of samples in opus packet before attempting decode

[33mcommit 81c83357f2e421adf474c778906975b12ca147ce[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Nov 10 17:44:33 2021 +0000

    Catch exception in opus_decode

[33mcommit a2cfc8c0645da45b77e984ae06711bd9e18e0969[m
Merge: d99b14c bb53558
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Nov 10 08:41:44 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit bb53558c92bc4235870f9dcfb3ee03cf8736fa9f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Nov 9 15:55:43 2021 +0000

    Small changes to audio handler

[33mcommit d99b14cc411d9b5c1094dbe2ff23f0036ce6c546[m
Merge: 847e607 827614e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Nov 7 15:43:19 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit 827614e080687169f220dc8d70c41158786b2aac[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 14:24:25 2021 +0000

    Add SSE2 enhancements to resampler

[33mcommit f04284db0ef4b83287737577122b9f478e35c371[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 14:18:20 2021 +0000

    Update audiohandler.h

[33mcommit ca552a0d8005442454b3e2a546b04785761b8a2e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 14:11:22 2021 +0000

    Enable SSE or NEON enhancements for resampler

[33mcommit 6247611d85001fa69e2d301ffb4b4812d8eb60e6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 13:49:00 2021 +0000

    Fix resample ratio for input and output (hopefully!)

[33mcommit 6fbd9cec707938e1c773d6fe5a950dc6045b92f9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 13:27:52 2021 +0000

    Create resampleRatio

[33mcommit 847e60791f78b3498e37993f24e48c09814bf615[m
Merge: bd5a233 1c0f33c
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Nov 7 13:31:28 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit 1c0f33c721b669c903164246dc8129a82a2dc13c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 12:16:25 2021 +0000

    Close PA stream after stopping it!

[33mcommit 93869b1bea4e73b15d9c6b78e481049034ab4e1a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 09:58:41 2021 +0000

    Fix Linux rtaudio build

[33mcommit 2b3c0165462fd9446d3b16b0292a62d10a0fd008[m
Merge: 589df71 1d03764
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Nov 7 09:43:02 2021 +0000

    Merge branch 'rigs' into audio-enhance

[33mcommit 1d037646845c70a3215c47db7b5c2789ac4103fe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Nov 7 01:28:35 2021 -0700

    Pulled out some debug code that isn't needed.

[33mcommit 0315033f8951a60e7217832f8b0c20949eb22e1a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Nov 7 00:24:08 2021 -0700

    Fixed manual rig ID issue with uninitialized variable.

[33mcommit 88430db9ac342a298ea3943272c3c614cbaccbea[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 6 23:59:03 2021 -0700

    Added override allowing the user-specified CI-V address to also be used
    as the Rig ID (model). This is useful for older radios that do not reply
    to Rig ID queries. For radios using the default CI-V address, it should
    "just work".

[33mcommit 863dd6ba959d4469687cfebe4417cf020007b243[m
Merge: fe3d645 d58a078
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 6 22:50:13 2021 -0700

    Merge remote-tracking branch 'origin/rigs' into rigs

[33mcommit fe3d645711611f333dd96be95f229d41e42f3322[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 6 22:49:10 2021 -0700

    Added PTT "snooping" to the commHandler, such that radios needing RTS
    set for PTT will work. Includes replying to PTT queries with the RTS
    status. There is currently no UI to manually select radios that need
    RTS.

[33mcommit bd5a233563b6bf1c1e5ac8d84274774f5a13662c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Nov 6 13:10:41 2021 +0100

    deleted a lingering swapfile from one of my vim sessions

[33mcommit 35cb1ed053fcbe15ba77e4ce455c38252cd613c6[m
Merge: 37205a1 2f480e4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Nov 6 13:10:21 2021 +0100

    Merge branch 'audio-enhance'

[33mcommit d58a078f705fb111622a816215e53155982099a5[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Nov 6 13:09:55 2021 +0100

    deleted a lingering swapfile from one of my vim sessions

[33mcommit facdd7dfbd37b31010a3e58f6531f9771ad2428c[m
Merge: 35bc1af 2f480e4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Nov 6 13:07:30 2021 +0100

    Merge branch 'audio-enhance' into rigs

[33mcommit 35bc1afb4bb7465e675b94aadf930a8f2c454428[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 6 10:42:13 2021 +0000

    Change MAX/MIN to use qMax/qMin instead

[33mcommit 589df7180575a61cc7bfb13008b8a8c4caeeacb8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 6 10:42:13 2021 +0000

    Change MAX/MIN to use qMax/qMin instead

[33mcommit 0f48959a75a43d1d15031bbb5c05c6450a0b115d[m
Merge: 2f480e4 77509ae
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Nov 6 10:20:11 2021 +0000

    Merge remote-tracking branch 'origin/rigs' into audio-enhance

[33mcommit 77509aead67356d8e09c11786ed8fbd75aa775cc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 5 23:21:36 2021 -0700

    Added RTS PTT support commHandler and rigCommander. RTS is only sent
    from rigCommander at this time, the pty is not parsed.

[33mcommit cd7ea9724281f32a24e6b080138f52b6b895e9ae[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 5 17:38:07 2021 -0700

    Added geometry constraints to the transceiver adjustments window, and
    disable controls which do not function except for debug builds.

[33mcommit 76c5488983f876a2d139455074951fd2f6f367d2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 5 17:12:19 2021 -0700

    Changed IF/TPBF commands to be "unique priority" inserts. Added "more"
    button for extended transceiver controls.

[33mcommit a9fb81d8f681006458af656492b584981aeb4ed1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 5 11:52:20 2021 -0700

    Added an IF Shift-like control for radios with Twin PBF.

[33mcommit cecaee397deead4942cf79f9e82dd561106b3e99[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 5 10:09:31 2021 -0700

    Added support for IF Shift and Twin Pass-Band Filters. Currently
    accessable only via the debug button.

[33mcommit 522557c6e8b7ac98524b0e18f0fe47f476ac33d2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 4 20:45:02 2021 -0700

    Added IC-736 FM mode

[33mcommit c3bdcf2287b1319d298a606a86e524596be78eaa[m
Merge: 12d202c 37205a1
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 4 20:39:14 2021 -0700

    Merge remote-tracking branch 'origin/master' into rigs

[33mcommit 12d202cbcd65497b1213b4604142d9c07f9eb846[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 4 20:17:50 2021 -0700

    Added code to force IC-736 to rigID

[33mcommit 2f480e4b822e63d96cf1d163a93981e7b22b7c0e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 20:39:12 2021 +0000

    Use QT Audio by default

[33mcommit 6f4cf6bbbea98af0ea7d2d8393bf4f2aacbb049e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 20:28:04 2021 +0000

    remove unneeded audio signal handler

[33mcommit 0c7bc1738268498d01453bff961f3f3e12fe3c8c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 20:27:33 2021 +0000

    Add portaudio support

[33mcommit 43efadbeaa2eeebc1b34155745ab83d2aff7227d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 12:44:23 2021 +0000

    Make switching between audio apis easier (and tidy .pro file)

[33mcommit 5b0a3620bd8c077ec3c6e90ce0bc13f4ee05442b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 11:17:25 2021 +0000

    Use buffered audio for Linux (was just Mac only)

[33mcommit ffdcada18c83ae0ddc76f6965cf755659c08f2a6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Nov 1 09:18:01 2021 +0000

    Adjust buffer size depending on latency setting

[33mcommit 4e5f315f08acb821c9b164e46e7dff555f8ce7ec[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Oct 31 23:53:20 2021 +0000

    Stuff audio buffer directly rather than signal/slot

[33mcommit 37205a11da2feacd80fc0ecd9bf40d1a6697b561[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Oct 24 18:09:11 2021 +0200

    changelog

[33mcommit 53d47c16c02ab64b6e86448094956ce06ad6c53d[m
Merge: 511f2b5 bab77eb
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Oct 24 18:08:04 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit bab77ebaba0e2fdb6cc7db64a2495e3dcb629855[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Oct 22 00:24:30 2021 +0100

    Don't block until audio buffer has space

[33mcommit 2c1272de66d4d1a3d566355b6bf7d1b33bab71fc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Oct 22 00:24:15 2021 +0100

    Bit of tidying

[33mcommit 511f2b5e98233acd8e56bb52cfaf7b25bd3e28ae[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Oct 20 14:54:44 2021 +0200

    changes in udp crashes in server side code

[33mcommit 52a4016379fa39e3c82797ac63abf3460dbcd3fd[m
Merge: c4a9465 620d789
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Oct 20 14:49:42 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 620d7890cdd28b3580b60a6ae737536a2057d9bc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 20 12:47:16 2021 +0100

    Tidy-up server shutdown

[33mcommit 24a88794e6647851e48e2fdb61bcc23df0512ae5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 20 12:13:18 2021 +0100

    Trying to find cause of lockup when client disappears

[33mcommit c4a9465fb3c959535f2d7078e5871ed2314794e5[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Oct 8 10:25:32 2021 +0200

    pushed 1.2d; fixing some reliability issues w/ wsjtx/rigctld

[33mcommit b7edb2cf96df76dcdfb82701c3e6f2b610f26e00[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Oct 6 23:41:11 2021 +0100

    Send TX/Freq changes multiple times with rigctld

[33mcommit 66912e13b280648ab80bf10f6dd27fc1601e04fb[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Sep 26 11:48:18 2021 +0200

    changelog

[33mcommit 6d0ad471c7a63bc6e66b0ae745d0d6fb87d6f475[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 22 11:07:07 2021 +0100

    Remove duplicate setPriority()

[33mcommit c712e8737353264356784c6eb27895eaeaefa244[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 22 11:02:09 2021 +0100

    Fix typo

[33mcommit 4cb16b105f5dbd7c2a0cbc703aab3e64359c99c8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 22 10:59:03 2021 +0100

    Add keepalive for linux/mac pty

[33mcommit 2c5f37d06c1a91568756340bdbbf31a2cab05074[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 22 10:50:07 2021 +0100

    Fix alignment of rigname in taskbar

[33mcommit 18eb9119befce6fd593abaaffac5f267dba07c43[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Sep 22 10:39:35 2021 +0100

    Only send RX antenna byte to rig when it has an RX antenna option in rigCaps

[33mcommit 21bc02cc25e9a04401600ef309f935007985391c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Sep 7 20:32:47 2021 +0200

    bumped to 1.2c

[33mcommit 8b06e9d048c22f40fe0a8274875c9212a9314d0e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Sep 7 18:04:43 2021 +0100

    Make rigctld state work for USB connected rigs

[33mcommit f7524c7fbaed947ad0ed9eb94a85072150b08cb8[m[33m ([m[1;33mtag: 1.2b[m[33m)[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Sep 7 09:01:55 2021 +0200

    fixed warning

[33mcommit b37713124bb67862f50340abfaf1eb5eabdffa5b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Sep 7 08:11:51 2021 +0200

    WHATSNEW, CHANGELOG

[33mcommit 9413d20906ce844915a2e95fe01e6dd1b40ae9a1[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Sep 7 07:55:17 2021 +0200

    bumped version to 1.2 beta

[33mcommit f1221c6f1b9b71ad4228fdc2136f3b8b382958ea[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 31 09:21:02 2021 +0200

    added 25 kHz step for tuning

[33mcommit c46eb659744cacbaf50fcef5b5ca53123950cb70[m
Merge: 54d6850 bf16dfe
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Aug 30 10:05:56 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit bf16dfe19a24664b0d975da9ac80e2a6ad4a5052[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 29 11:51:53 2021 +0100

    Experimental support for split mode in rigctld

[33mcommit 57e68571afed9866e3956f4286850c359f99d534[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 29 00:25:23 2021 +0100

    Ignore control levels that we don't currently support

[33mcommit 67c80c25105dd9e58ddfc6da8e336a5ba4672270[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 29 00:16:31 2021 +0100

    Add better detection of ci-v transceive disable

[33mcommit b7407fc10865e63a6ce8ffa6a88b5a7e4bdbb9d3[m
Merge: cffb2bf 54d6850
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 28 00:22:38 2021 +0100

    Merge branch 'master' into QT6.2

[33mcommit 54d6850aa1f0ffb75b94f6243b8484e8ac7d80dc[m[33m ([m[1;33mtag: 1.2a[m[33m)[m
Merge: 8d96c97 97e5ff9
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 27 19:09:16 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 97e5ff9ff8d9e9c411391c937d66b945062de144[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 27 18:07:45 2021 +0100

    Remove unneeded debugging

[33mcommit 3a9f9db4a3968dc3d45b04ea234933fc5e2861fa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 27 18:06:09 2021 +0100

    Add saving of meter2 state

[33mcommit 5216b43230f77e38f8fffc95ca91d89adcc42bc4[m
Merge: 5b1ed2d 7c96102
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 27 17:18:12 2021 +0100

    Merge branch 'master' into audio-enhance

[33mcommit 8d96c97f3ac898518f4f5d2a3440c0a725a8295a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 27 11:45:45 2021 +0200

    bumped version to 1.2 alpha for testing

[33mcommit 366676c286240abb56864bdfcb819200cf22a509[m
Merge: 7c96102 71c12b5
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Thu Aug 26 07:13:40 2021 +0000

    Merge branch 'fix' into 'master'
    
    pttyhandler: add errno.h
    
    See merge request eliggett/wfview!6

[33mcommit 71c12b508fceabd88131db754e017f8728ce0dc6[m
Author: Davide Gerhard <rainbow@irh.it>
Date:   Wed Aug 25 19:05:52 2021 +0200

    pttyhandler: add errno.h

[33mcommit 7c9610283c29513659557b55eae66ab31248d3dc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 24 09:34:40 2021 +0100

    Replace missing ;

[33mcommit 47d9c52f879f9a2af6364d2d95f55560dfa3b50e[m
Merge: acb976e 57b6f95
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 24 09:24:25 2021 +0100

    Merge branch 'opus'

[33mcommit 57b6f955c66358a2c0af6a4f7c66c9712bb8b476[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Aug 23 23:59:40 2021 +0100

    Set audio thread priority in the correct place!

[33mcommit cffb2bf93ac49f9e215f2f7148f6b4ef366712d4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 24 09:20:38 2021 +0100

    QT6.2 auto update audio comboboxes

[33mcommit acb976e68ec2e03265e0a89518a7a117f3902fcd[m
Merge: ed1684a db6a20d
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 24 07:51:30 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 5b1ed2dc353e7f6c3e843027c9f02a948dd46624[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Aug 23 23:59:40 2021 +0100

    Set audio thread priority in the correct place!

[33mcommit 1b3edbfec1f2930e5b15831c18a4b2dcdab95a07[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 24 00:00:49 2021 +0100

    Set 64bit for default build

[33mcommit af440fbf55edf91dfab1d6ed75aa06904da2273b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 24 00:00:29 2021 +0100

    Look at supporting dynamic update of audio devices

[33mcommit 886b597d565f6f6f126a5d520171392eb4bf88d7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Aug 23 23:59:40 2021 +0100

    Set audio thread priority in the correct place!

[33mcommit db6a20d3d03b5d4669f303209266a18160e2ea08[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 23 14:40:36 2021 -0700

    Now with dual meters for everyone!

[33mcommit edef09c8bf823b0bd15b9744c4eb164e87d1d2c0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 22 10:16:16 2021 +0100

    Beginning to support QT6.2

[33mcommit ed1684a5a3ae8a24f6cfca20b38d7705f74cdb1a[m
Merge: 58a6477 23ccee8
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Aug 21 13:49:19 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 23ccee8a21d4e96390d29ba4fadd8038be6696e1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Aug 20 22:23:16 2021 -0700

    Clear Center readout as well.

[33mcommit d9a9a3ba2b1f50dcd1cda780e1f679fb19e91131[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Aug 20 22:19:47 2021 -0700

    Fixed issue where the "none" selection didn't work quite right. Also
    fixed the T/R meter switching to clear out invalid readings.

[33mcommit 58a6477f53bad1a38cb664b9c5a0e5f33f48eac4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 19 10:21:30 2021 +0100

    Set audio threads to be realtime priority

[33mcommit c2840f43d9e7a7fe2434ba9d15e85138cf37c633[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Aug 18 12:11:23 2021 +0100

    Update wfview.vcxproj

[33mcommit 968835e58a0e3644f67dd57e6d4f5274948aa733[m
Merge: 4b564aa 0b5a6cc
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Aug 18 12:11:14 2021 +0100

    Merge branch 'master' into opus

[33mcommit f44dd4bda2a00d2cbba5499dcebafab16a4bc08b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 17 14:00:37 2021 -0700

    Added IC-736

[33mcommit 4b564aa8fc3ccbe966a087b2ec0fd8a43576af4b[m
Merge: 064d9cc b8b2892
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 17 21:27:59 2021 +0100

    Merge branch 'opus' of https://gitlab.com/eliggett/wfview into opus

[33mcommit b8b2892a1bf0f2af8b725e9671094c753313ef37[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Aug 17 17:54:41 2021 +0100

    Remove unneeded submodules

[33mcommit 390f9500dc9c16dde7cf043c4bea4e77a9aa36dd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Aug 17 09:35:40 2021 -0700

    Added more support for the IC-9100

[33mcommit 0b5a6cce1f55eae10c158275cae9368c3f2c894d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 17 16:11:50 2021 +0200

    dual meter support/WHATSNEW

[33mcommit 064d9cc2ce35038473f30f72e7595db150d75ff3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 16 23:37:50 2021 -0700

    Current scale tic marks on both sides now!

[33mcommit 7f44af239acb635ada47f8a148f8323afa99d595[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 16 23:25:11 2021 -0700

    More meter work, fixed some scales and added labels.

[33mcommit a09e8c51b81ad4de6803163f3a7ce18b949bef39[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Aug 16 21:51:45 2021 -0700

    Better meter fonts

[33mcommit 493f2f60d97300c3d13f4779e0ef35dca81b07f8[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Aug 16 14:40:54 2021 +0200

    WHATSNEW and INSTALL.md: added opus

[33mcommit fc2d1e24e276685429ff280468b779df2f78fab1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 15 14:41:54 2021 +0100

    Fix for left over CIV client on server

[33mcommit 04562a6ca5bcba2852a962c9aa40fc052030e411[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 15 11:36:08 2021 +0100

    Add a bit of logging for creating/destruction of opus

[33mcommit c5c0046e3a3708b9e7ba54d3f29d1e0cd137fd33[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 15 11:19:34 2021 +0100

    Improve detection of unsupported codec.

[33mcommit 758724f8af8a39948eb9fa2d97081d1a777619e9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 20:08:11 2021 +0100

    Fix for Opus TX audio

[33mcommit 21c8b54819bc40667fd3d5370556e013eae1827f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 19:02:04 2021 +0100

    txrate logging wrong

[33mcommit e5effe657192dafcc487ea66148196c774f07b34[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 17:22:20 2021 +0100

    And another one

[33mcommit 484da6e55d0911c14353c54f129b1e8c8e6c0ccb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 17:20:40 2021 +0100

    More warning removal!

[33mcommit c7a2561a3f24331fb6761821b4f792db99ec8269[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 17:18:35 2021 +0100

    More commenting in rigctld.h

[33mcommit dc299fc4f7649b45a7ee1e4bdc0a563d434cca83[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 17:15:59 2021 +0100

    Is this going to fix 8 bit audio?

[33mcommit f17e69b4be458c12ff7e5ab48c99db1a21fb316e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 17:15:44 2021 +0100

    Comment unused structs from rigctld

[33mcommit 9d07bbc2813f91a8ad069e35a1ee19c7840cc372[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 16:27:31 2021 +0100

    All audio codecs now work in both directions!

[33mcommit eb5dc0d095858f0a2a137dc2674e660c1af47c3a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 16:16:11 2021 +0100

    Update audiohandler.cpp

[33mcommit 9ae9e563999263182a7d7af39c7dc0037725cd32[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 16:11:48 2021 +0100

    more 8bit fixes!

[33mcommit 2b8449d4d313135ffb82a69d719d62b831133346[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 16:04:50 2021 +0100

    ulaw encoding test

[33mcommit 02ba27b6b2869bb4878d28e98e2e2b5887ee139f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 13:16:39 2021 +0100

    Update audiohandler.cpp

[33mcommit ac74f8ba5c3f41afe71a49ec90775c5647ab6ec3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 13:05:18 2021 +0100

    Update audiohandler.cpp

[33mcommit 208ed9596ced107d3ceff43bbee6d80311fd5aaa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 13:02:07 2021 +0100

    8 bit encoding fix

[33mcommit 218dc493e227fba86a545fe1aac51c8c7142d890[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 12:25:50 2021 +0100

    Another issue with  8 bit audio

[33mcommit f84172d5aee10e865dfaa0f95a824bd3b5d2ef60[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 12:17:43 2021 +0100

    Try again to remove debugging!

[33mcommit e6750be84ce0ad81011c4b38606391040642de29[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 12:08:50 2021 +0100

    Revert "Remove extra opus debugging"
    
    This reverts commit da53f5371bbeb35b10cbb831b83b74e1bdd771f9.

[33mcommit da53f5371bbeb35b10cbb831b83b74e1bdd771f9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 12:03:04 2021 +0100

    Remove extra opus debugging

[33mcommit 2214ec7783dbbacfb89d59e2659abbf9b2753ec5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 10:29:22 2021 +0100

    Move opus init

[33mcommit 9ca727ac65763309cd33981c0bfadf2c6f8b1f5e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 10:11:58 2021 +0100

    Add some debugging for decoder

[33mcommit d2098996ac8342696389e780c3cbf3a007dacdb5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 10:06:17 2021 +0100

    Use radio samplerate for opus

[33mcommit f5913dc0991a6a3d98c63b5bf96c64a5e4663781[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 01:01:50 2021 +0100

    Remove big/little endian conversion

[33mcommit d4023e8b36bc358762e0a914e0d45dd8f59ab491[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 00:56:16 2021 +0100

    More Opus fixes

[33mcommit e80d12e477837644ac7b9a935b794167beaa1e71[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 00:03:26 2021 +0100

    Update audiohandler.cpp

[33mcommit 88ec30bb490d2ee57e2f7d62a009d400bb41fd08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 14 00:01:45 2021 +0100

    Update audiohandler.cpp

[33mcommit 95b10ce4656548ec3a741968ac0013453dea97d5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 13 23:35:42 2021 +0100

    Update audiohandler.cpp

[33mcommit aff0e6847c5cb63e31475c3577a616eb9853ec1b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 13 20:52:18 2021 +0100

    Update audiohandler.cpp

[33mcommit 28b2a716d135724ef2ed323ae0fac939d19719e6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 13 20:41:42 2021 +0100

    Update audiohandler.cpp

[33mcommit b336d1836e621ec511f4943605379c99fbf1d8ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 13 20:30:53 2021 +0100

    Try to fix Opus 2-channel

[33mcommit eb2f897a81b44e24628f5f225a4a0c6c883e553b[m
Merge: bc0d69f eb637d2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 13 20:15:00 2021 +0100

    Merge branch 'master' into opus

[33mcommit eb637d2029a9a3b3d940e97dec170bdeb5e96d7a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Aug 11 08:47:51 2021 +0200

    WHATSNEW?

[33mcommit 4a7dc5d99e77291810f4f504c74bfda3a9a64019[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Aug 11 08:41:45 2021 +0200

    changelog

[33mcommit e1dae9edd970798f69f1b30f16d6a81db05e4219[m
Merge: cc00228 d755d30
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 10 07:59:41 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit d755d30f83e0b3e484488ea5f6130895c508fcd7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Aug 9 17:37:06 2021 +0100

    Add constants to make parsing (hopefully) easier

[33mcommit cc002281abc53923fdde9c0eab5b18fb9e5cba23[m
Merge: dfc101e 953f572
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Aug 9 10:50:49 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 953f57267365756c0403fcb477c188dc078a61d2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 8 21:09:10 2021 +0100

    Fake known functions

[33mcommit 4d0153741cec679888f0f89bbe44bc2130a7b4ea[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 8 21:03:22 2021 +0100

    Fix float warning

[33mcommit 979442de8745f2a4e574e952f513a2d92f5d38e1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 8 17:17:12 2021 +0100

    Remove calibration debugging

[33mcommit 85271c398ee6a30478275576a65b3de0ce0eefba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 8 17:14:48 2021 +0100

    Add proper s-meter calibration

[33mcommit dfc101eabe88d267dd05b9cbc3be2fe9b4938b09[m
Merge: 9320892 3a2db78
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Aug 7 21:35:55 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 3a2db789a2abf0e7645a5db11b10224fbc156409[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 18:34:34 2021 +0100

    Add ritctl model to rigCaps

[33mcommit bbb45a01c78b478b7fe6c112bd27061d4c6fb7a9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 17:31:55 2021 +0100

    Fix to make wsjt-x work again!

[33mcommit 84261a49cc76675e5de178a1e030c1803d7ba143[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 13:34:47 2021 +0100

    Add split/duplex support

[33mcommit 932089285d1924a8cd933980e2513ffde305618d[m
Merge: ce63ab1 b2b438f
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Aug 7 12:21:22 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit b2b438f0038abab427ac5c870a5864934e83cd33[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 11:20:36 2021 +0100

    Update rigctld.cpp

[33mcommit ce63ab1617c58db7ee2a3add34f69c0e177aeb92[m
Merge: 69d2a37 f86aa6e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Aug 7 12:18:08 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit f86aa6e4f62a70cc832010baa32f28bbd1ae0e59[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 11:16:55 2021 +0100

    Correct lack of parentheses in conditionals

[33mcommit 69d2a374b6edba69026615ccd673174f1e86ea99[m
Merge: 2cadd16 9ca39a0
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Aug 7 11:33:00 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 9ca39a06769cd515828323a80952764b6812dd3b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 10:04:16 2021 +0100

    Fix typo

[33mcommit 73e29cd0fd02ae136e1a8179a16e566e5c8c0491[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 01:16:02 2021 +0100

    Remove some debug logging

[33mcommit c4ed4d2de4ebf842a88c9ed67d55ff6b006988e9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Aug 7 01:14:41 2021 +0100

    More rigctl features/fixes

[33mcommit a945988671925399b3863ad8a79c8f326d12896a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 6 12:50:56 2021 +0100

    Fix for get_powerstat

[33mcommit 6b34d500f32b920c4b670dea5bedd43914fd6ba7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 6 12:40:58 2021 +0100

    Update rigctld.cpp

[33mcommit 797ed3bc5ec7f56a7e30b7f6d242758bad0083ab[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 6 12:37:03 2021 +0100

    Add some levels and other functions

[33mcommit 2cadd16de2da2263963864e79c51bf779608b6be[m
Merge: ec1ce2a 04bd237
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 6 10:48:12 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 04bd237eb4cfd82fb28f95f461b00a822e91b8b8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 6 09:47:32 2021 +0100

    Fix compile warnings

[33mcommit ec1ce2a3d3e9748c3c557e0a137a21aa2c65238f[m
Merge: e9df69f a112ee4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 6 10:39:25 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit a112ee43cbd83ca7416e20442771ecfa3604c33e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Aug 6 09:25:08 2021 +0100

    Add frequency ranges from rigcaps

[33mcommit e9df69f156ea5fd9f611fe6d04d14700eb763821[m
Merge: 2a524fc b8345bf
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Aug 6 09:32:09 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit b8345bf77fc3ad13586fe1f75088318f0c75100e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 14:45:19 2021 +0100

    Move rigctld settings in Ui

[33mcommit 543b289c99756fe6433a503299aa60639a9a8596[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 14:15:10 2021 +0100

    Fixes for setting freq/mode

[33mcommit e091ed52541a5e7fa2eefb1d2339dfc6b0dae629[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 13:52:06 2021 +0100

    Support for more rigctld commands

[33mcommit 273132de8938a651d2c887a05bd4c456c68e0950[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 11:29:20 2021 +0100

    More fixes to rigctld

[33mcommit f36cefac3a1e70828a55f0d555a82b93a82e6da2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 11:06:35 2021 +0100

    Change the way rigctl response is built

[33mcommit 2a524fca1dc7aac69da7868a87408d82cab65e84[m
Merge: 3fc7532 1e2a0db
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Aug 5 09:08:02 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 1e2a0db60423eed3927c5e8ef0b54333a7d0a32b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Aug 5 01:23:15 2021 +0100

    More rigctld fixes

[33mcommit ee2cf70dc046f161fa4d3be61174d9a90397c79b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Aug 4 20:49:32 2021 +0100

    Add rigctld config to ui and fix some bugs

[33mcommit 3fc75320e476608c21b5895c805d3bef55d92d50[m
Merge: 3be0813 bee4f03
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Aug 3 20:13:50 2021 +0200

    Merge branch 'linstall'

[33mcommit bee4f03edb6edf534c2cfe71c5219a5b2a7cb382[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Aug 2 08:06:19 2021 +0200

    added derSuessman prefix code

[33mcommit 3be08137dd25b0addc65a2f3c91d144910b7f079[m
Merge: 6df36a0 eddc5d4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Aug 2 07:54:51 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit eddc5d42ba7e237f2ca87f02d7c5cb363b067b20[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Aug 1 18:34:32 2021 +0100

    Fix broken 8bit audio

[33mcommit 680262b4d2ef00b49fa3e98e0f41c0ab4d6a816f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Aug 1 13:58:47 2021 +0200

    added derSuessmann additions to have a linux install prefix

[33mcommit 472709357805cdfdf6de24888397e0d768c25cce[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Aug 1 13:55:44 2021 +0200

    added derSuessmann additions to have a linux install prefix

[33mcommit 6df36a05be176a05d163463b19623a9a42d6007e[m
Merge: 3f1fc95 f8beed5
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Sun Aug 1 11:07:07 2021 +0000

    Merge branch 'fedora_install' into 'master'
    
    Fedora build/install notes
    
    See merge request eliggett/wfview!4

[33mcommit 4e63a72106d6da3eb87e28f263415d54fc722947[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 30 17:30:34 2021 -0700

    Added a little extra logic, also some cross-platform help, to the custom
    stylesheet loader.

[33mcommit bc0d69ffb5ad64de367f57fdc210258f4ece3055[m
Merge: f4cdccc ea09647
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jul 29 16:18:59 2021 +0100

    Merge branch 'master' into opus

[33mcommit 2ac1b8c0acb6a6c6a3f4374f050aa828f07fa7a5[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Jul 29 11:28:45 2021 +0200

    fix: set the style once

[33mcommit 7992d6f870b2edc92a20c0c40ac629dd32da96d6[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Jul 29 10:53:57 2021 +0200

    added /usr/local to search path for the stylesheet

[33mcommit 3f1fc957acdd17d89a9e7f20fa869d62c9df279c[m
Merge: 2403f53 a777864
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jul 27 12:11:17 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 2403f5352adce212474a78e245f0b56b78ed0b44[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jul 26 19:50:40 2021 +0200

    changelog

[33mcommit a777864a0bd7ada7c47b373db4082b5e6a522c8b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 26 09:03:09 2021 -0700

    Fixed error in IC-7410 attenuator spec.

[33mcommit c312277189359bb14b6d90fb1efcc50602d41db4[m
Merge: 8983b4e dd84be7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jul 26 10:56:26 2021 +0100

    Merge branch 'master' into audio-enhance

[33mcommit 8983b4e090ab2314a9dfe592924be26edc88562b[m
Merge: 93ec46e 459f157
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jul 26 10:56:04 2021 +0100

    Merge branch 'ui-enhance' into audio-enhance

[33mcommit ea096475977c12c4264bbe66b197ca2b1c843715[m[33m ([m[1;33mtag: v1.1[m[33m)[m
Merge: dd84be7 93ec46e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jul 26 11:31:27 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 93ec46e077238a9e9cf6245c2ad5fad3834f6cd1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jul 26 10:27:39 2021 +0100

    Fix for blank username/password in server

[33mcommit dd84be71c5e7d4b6f935cd69cff88d6400370492[m
Merge: b5cbc68 459f157
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Jul 25 11:11:31 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 459f15733ff1184887fe0cca306952619291bf6c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 24 22:55:28 2021 -0700

    Version string to "1.1"

[33mcommit 6c27f44ccc937771e1d2ab29e358b51a89e93c3d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 24 22:53:36 2021 -0700

    Meter now scales the meter bar to fit the avaliable depth. Text should
    scale to have the correct aspect ratio. Fixed minor bug in reporting
    connection type (always reported serial and then was replaced with
    connection status).

[33mcommit b5cbc68a3832849ed229c3c803352db65ec23761[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jul 24 14:38:29 2021 +0200

    changelog

[33mcommit 944e1ae1ccbfd9ed0238274c7767504f64e50714[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jul 24 10:59:01 2021 +0200

    small changes to INSTAll.md and addition of mint 20.2/openSUSE 15.3

[33mcommit 3b209286dab8ec5fccac80b76c465ffc5ec99fad[m
Merge: 59234f2 b512c3e
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jul 20 20:56:11 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit b512c3e306934dbecf7a79958b0f3cb4560daf1e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jul 20 11:11:51 2021 -0700

    Clear out meter values when type is switched.

[33mcommit 59234f25f7f08c66d20da8f86fa9393104daf796[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 20 14:45:44 2021 +0100

    Allow user to turn off power-down confirmation msgbox

[33mcommit 44b11ba623aab38842f58f515e1e0e620632e47c[m
Merge: 1c9edb7 5d78f52
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 20 14:30:32 2021 +0100

    Merge branch 'master' into audio-enhance

[33mcommit 5d78f52c97204b0ada464c9bdd71d2e6c17a2613[m
Merge: 886ca86 4fb9177
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jul 20 08:51:00 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 4fb9177f2c601e473d4faf47e5422f2fa47bf871[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 19 23:46:11 2021 -0700

    Font sizes are... better... but not perfect.

[33mcommit 886ca86a902615f6dc422b8c44edda05456d2138[m
Merge: b8ab1b9 274cc65
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jul 20 08:07:28 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 274cc65dbf1ead66b8bb6619d2ce58d28b540bdc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 19 16:56:04 2021 -0700

    wfview now uses the meter's own balistics (average and peak code). This
    makes it very easy to meter any parameter 0-255. Meter Type "meterNone"
    or other will display data in "raw" format.

[33mcommit decdfe370b01bcae181309af7ecdd1bbe7df9ef2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jul 18 14:27:21 2021 -0700

    Added center tuning for IC-R8600, partially moved meter balistics
    (average and peak) to the meter class.

[33mcommit 1c9edb7cf9a6f6a5a4c373656705082f91463cb9[m
Merge: f3492ce dc6b488
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jul 18 11:57:37 2021 +0100

    Merge branch 'ui-enhance' into audio-enhance

[33mcommit f3492ce3d2ea6bc00ff42ba0d762f3a71006f29a[m
Merge: 62e3e2b 98f29d2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jul 18 11:56:37 2021 +0100

    Merge branch 'ui-enhance' into audio-enhance

[33mcommit b8ab1b966d6e358701286f1e46cb6e95a8ed4ec9[m
Merge: a29b821 dc6b488
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Jul 18 12:45:50 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit fd82de26479d140abf3d5076381d26f8d8210c47[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jul 18 00:03:04 2021 -0700

    Quick debug for the metering queue, just in case.

[33mcommit dc6b4884d6c832b04869f2e6e239c5d2d84ea20d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 17 23:42:18 2021 -0700

    unknown change confusion

[33mcommit a501ddf51eb5897451442c6a0c9c7a6805c208cc[m
Merge: 027815f 98f29d2
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 17 23:40:50 2021 -0700

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 027815f4c0b6db77ad92fe5b4a1a278e13826144[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 17 23:39:40 2021 -0700

    Preliminary secondary meter support. See Settings tab for selection.
    Some scales incomplete.

[33mcommit a29b821bdb933662d2f1feebaf5b3fa74e9afe49[m
Merge: 62e3e2b 98f29d2
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jul 17 18:43:43 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 98f29d2bde87d9323eee8334fab946233fc65679[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 12:07:52 2021 +0100

    Replace function wrongly deleted by merge

[33mcommit 62e3e2b8d85e2d96232931e9f4731b06a9366d69[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 12:04:48 2021 +0100

    Fix wrongly deleted definition

[33mcommit 166e714c26717db56e54ce9cdeae85b5a17d8c79[m
Merge: 3577162 0764ad7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 12:02:04 2021 +0100

    Merge branch 'ui-enhance' into audio-enhance

[33mcommit 0764ad73e0c18c839e9078652065eb3ba837ae0a[m
Merge: b2c4bbf 8257a35
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 12:01:01 2021 +0100

    Merge branch 'ui-enhance' of https://gitlab.com/eliggett/wfview into ui-enhance

[33mcommit 357716206a6a09017519ef24dcb3bba1b99011ca[m
Merge: df690e0 b2c4bbf
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 11:58:31 2021 +0100

    Merge branch 'ui-enhance' into audio-enhance

[33mcommit df690e0fd1c90b0af57399cf3f0a6559da3bb517[m
Merge: 0440097 6a6a45d
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jul 17 11:49:44 2021 +0100

    Merge branch 'sequence' into audio-enhance

[33mcommit 8257a3540f3cd42e737a9ac4167d27cdb44d1077[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jul 17 00:37:27 2021 -0700

    Added SWR and ALC scales to the meter.

[33mcommit c9692f63a6cb6dd0b07fabe04aa97378158ea5bc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 16 23:23:29 2021 -0700

    Fix error in scale of power meter

[33mcommit b6a4e06fe20ef1a77a5c17c3458fadd173c8bb2f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 16 17:45:07 2021 -0700

    Power meter for transmit. Much work remains on this meter alone.

[33mcommit 0440097f5357b3425c0fc0cbed301cccb351142e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jul 16 20:14:59 2021 +0100

    Add missing break

[33mcommit e4dea630296e3bb3d168f37786aae1cf05684482[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jul 16 19:33:15 2021 +0100

    Get antenna status on start-up and slow poll for it.

[33mcommit e40545cf77854e0c2df79f316b4b77757b1f1de3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jul 16 17:08:55 2021 +0100

    Add RX antenna selection for rigs that support it

[33mcommit 6a6a45d0795b124c43a4360f3b8907c9f02ae5d3[m
Merge: 050e449 21b9be4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Jul 15 08:52:21 2021 +0200

    Merge branch 'sequence'

[33mcommit 21b9be4f945faa7507fe0ace785b31f8988e5cd7[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Jul 14 22:44:05 2021 -0700

    Preferences added for Anti-Alias and Interpolate.

[33mcommit f191775acb96b7fe26b5910c9a4d82df02a7097d[m
Merge: d12906d 54f2dcd
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jul 14 12:00:02 2021 +0100

    Merge branch 'sequence' into audio-enhance

[33mcommit 050e449df71f7f8af104888db019c79fe5722388[m
Merge: 8a49e02 54f2dcd
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jul 14 10:29:40 2021 +0200

    Merge branch 'sequence'

[33mcommit 54f2dcd5b86a18941d442d64ae7f0ed75b474607[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jul 13 22:42:55 2021 -0700

    Added waterfall display options: anti-alias and interpolate. Not in
    preferences yet. Debug button enables wf pan and zoom.

[33mcommit 8a49e0267d43fc6fd80eb18c70c509b95c07a447[m
Merge: 2f32990 d545624
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jul 12 18:47:53 2021 +0200

    Merge branch 'sequence'

[33mcommit d12906d7d3a82a887944217190ecdb728aa891e4[m
Merge: 2f32990 d545624
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jul 12 09:15:17 2021 +0100

    Merge branch 'sequence' into audio-enhance

[33mcommit 2f329904f668949e98752d0b929375b9b4441f56[m
Merge: c7f0cee 99b1e7f
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Jul 11 18:55:57 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 99b1e7f4077d7047bca8d9435e810f1c27c2c646[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jul 11 17:30:02 2021 +0100

    Allow user to select whether to confirm exit or not

[33mcommit d54562469d53fe66719a710ce2466e80e7bea8cd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 9 23:59:32 2021 -0700

    Reset PTT timer for control-R keystroke.

[33mcommit 2e9c734b64b3fa70628da839746b463a9ad5a8b8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 9 18:13:11 2021 -0700

    Added a fix to keep the local frequency in-sync with any recent commands
    sent to the radio.

[33mcommit b6cac33ee96cb227102a1a2e7718b57e3c3ac234[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 9 15:15:04 2021 -0700

    Added more support for the IC-7600 in rigCaps.

[33mcommit e50b0327165066728cf80b9c2e142fb63a318323[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jul 9 12:02:50 2021 -0700

    Added time, date, and UTC offset commands. Currently initiated by the
    debug button. There seems to be a bug in the 7300 where the UTC offset
    has one hour subtracted, ie, -7 HRS becomes -8 HRS. The hex command
    appears to be sent correctly.

[33mcommit 23869fdcd2e83018dab11fda299bfee9dd80443f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 15:31:35 2021 -0700

    Changed wording about the need for Save Settings -- please verify, I may
    have it wrong.

[33mcommit 0a9f7639e12631fcc02eddbf690ae236b81289ae[m
Merge: ff5cebf c7f0cee
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 10:15:50 2021 -0700

    Merge remote-tracking branch 'origin/master' into sequence

[33mcommit c7f0cee09fdba096352da26ed4ec3a6fa6cd8647[m[33m ([m[1;33mtag: 1.1a[m[33m)[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 16:34:02 2021 +0000

    Update aboutbox.cpp, added a space

[33mcommit d0dbfe20ad0d15baff105f5915dea72bcb58555f[m
Merge: 29ef8d4 9e41278
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Jul 8 17:52:54 2021 +0200

    Merge branch 'sequence'

[33mcommit ff5cebf4758afcbea481077fa706ec2a0f39f99b[m
Merge: 9e41278 29ef8d4
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 08:42:14 2021 -0700

    Merge remote-tracking branch 'origin/audio-enhance' into sequence

[33mcommit 9e41278272b0a3850070b0fc44a73d7fc869f54d[m
Merge: ab93788 b5c08ae
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 08:39:47 2021 -0700

    Merge branch 'sequence' of gitlab.com:eliggett/wfview into sequence

[33mcommit ab937880104e29e2f308b8ac8730b9c0b4ef3833[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 08:39:13 2021 -0700

    about box external links are now working

[33mcommit 29ef8d482d12428dcc8b49bfac0c6f4e5b472843[m
Merge: f7060ed b5c08ae
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jul 8 09:32:22 2021 +0100

    Merge branch 'master' into audio-enhance

[33mcommit b5c08ae01337d62e3ca749059a95bae58dd96297[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Jul 8 10:29:44 2021 +0200

    fixed layout to prevent horizontal scrolling in the about box

[33mcommit f7060edbe41ccb402310c8180c830cbc96c93509[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jul 8 09:28:20 2021 +0100

    Remove typo in latency display

[33mcommit aeaee8de5252d920d38b569fd9022206552521fb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jul 8 09:23:53 2021 +0100

    Delete oldest entry from UDP buffer before adding new one.

[33mcommit 469d44d6db96cbbb73b1d30e25566d0744e15e2f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jul 8 09:23:16 2021 +0100

    If pty connected s/w sends a disable transceive command, dont' send any transceive commands to it

[33mcommit ce2f862b6a8f0cd7e72237eb34e38118d79f5161[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 01:01:24 2021 -0700

    More about box modifications

[33mcommit 8bd786f235bfe981334bdead468b33449b8b8d1b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jul 8 00:31:59 2021 -0700

    New about box!

[33mcommit cfa1331b8fe04ad771c6c8cd1ec524308e9a41b4[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jul 7 16:33:51 2021 +0200

    WHATSNEW and CHANGELOG updated

[33mcommit 9588aa1eb96886c35667c149c15e25a63269e333[m
Merge: 3d62fb6 e6e8a11
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Jul 7 07:56:26 2021 +0200

    Merge branch 'sequence'

[33mcommit e6e8a115c3fe22c003158b341d4b195bcad54432[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jul 6 12:02:09 2021 -0700

    Local af gain now has anti-log audio pot taper.

[33mcommit 3d62fb6f08c289e15a2d31c6ad7dec94523b6573[m
Merge: 97c1513 e4fdf60
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 6 10:09:34 2021 +0100

    Merge branch 'sequence' into audio-enhance

[33mcommit 97c1513c45f93f41f8c06406bb81d9b71c249a16[m
Merge: a3e98b7 df233d5
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 6 10:09:08 2021 +0100

    Merge branch 'master' into audio-enhance

[33mcommit f4cdcccfec2a3b10fafeaf7c85a11fc58c211c6f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 6 10:04:35 2021 +0100

    Only start audio when stream is ready

[33mcommit a3e98b771d2b3bb397aaad901a94a72423a25ce5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 6 10:04:35 2021 +0100

    Only start audio when stream is ready

[33mcommit 280f7133d9168cbbb41a4d250b3afe41899ac594[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jul 6 10:04:35 2021 +0100

    Only start audio when stream is ready

[33mcommit e4fdf6058cabac7e73dc12d13ef058f5bf0dd301[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 5 23:56:24 2021 -0700

    Added transceiver adjustment window show code, for the debug button only
    currently.

[33mcommit ea26eaf53b8ca6d9b7e4137ca40ab8dddff3821d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 5 23:50:37 2021 -0700

    Added window title text change to show radio model. Needs to be checked
    cross-platform. On Linux Mint, displays: "IC-7300 -- wfview"

[33mcommit f51f91afafa16ade2b4306e7975d160e1e12d3c0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 5 23:24:35 2021 -0700

    Applying setup.volume within audio handler if the audio handled is an
    output. Added this to the init function.

[33mcommit a8b1e905a0ddd2d728f137d9ec4cce2e398bcb11[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 5 17:04:55 2021 -0700

    waterfall theme is now saved.

[33mcommit 9799e4f745f11422891550ffb871c229424fb987[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jul 5 16:45:19 2021 -0700

    Added local af gain and wf length to the preferences.

[33mcommit df233d525798b4136aa9f3208c28ab0f4dd9c5db[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jul 2 19:30:29 2021 +0200

    changelog

[33mcommit 1563fccf54b3a1995390879c5fd2e69dc2e5f716[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jul 2 13:19:53 2021 +0200

    fixed small error where the tx latency was not copied in the UI

[33mcommit 82d4862e217b669cb1a824ba6d0ac61bc6b462f0[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jun 29 09:43:51 2021 +0200

    WHATSNEW - added 718/7100

[33mcommit ff4b945699f20a4ccbda185a4a7ee00df1e474b7[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jun 29 09:36:30 2021 +0200

    updated changelog

[33mcommit 05d9c16eb92435a0b3841cae3eb8797c56dedf46[m
Merge: b071e8d 38f45b1
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 19:09:58 2021 +0100

    Merge branch 'sequence' into opus

[33mcommit b071e8d78828db3d5125f6533f91a8ab1c1afad5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 19:09:41 2021 +0100

    Revert "Check whether data is nullptr in doCmd"
    
    This reverts commit c25040f793c776591c19f22c7331ba289fb9c1ab.

[33mcommit 6d58034a41a1caf900d4bcdcb149cd3ff5b67acf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 19:09:24 2021 +0100

    Revert "Move nullptr check to modefilter"
    
    This reverts commit 19f33d1ed794c5684cc567596ed1b69deff3f815.

[33mcommit 38f45b1d316a6906dd06812e671bf882f2165949[m
Merge: 3803229 298a659
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 26 09:25:35 2021 -0700

    Merge branch 'sequence' of gitlab.com:eliggett/wfview into sequence

[33mcommit 3803229b9098a5bb3c0c1a83ee76286d61f0d89e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 26 09:24:58 2021 -0700

    Duplicate of existing command.

[33mcommit 19f33d1ed794c5684cc567596ed1b69deff3f815[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:58:54 2021 +0100

    Move nullptr check to modefilter

[33mcommit c25040f793c776591c19f22c7331ba289fb9c1ab[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:52:38 2021 +0100

    Check whether data is nullptr in doCmd

[33mcommit 298a6594433801e5312ba24deb7372242d97270b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:13:35 2021 +0100

    Remove unnecessary escape sequence

[33mcommit 83f782cf1096c5a7949f608869db968d621a686f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:12:39 2021 +0100

    Fix for fix of missing __PRETTY_FUNCTION__

[33mcommit 51a2d1093765667e97dc21360d99549ba538afb5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:13:35 2021 +0100

    Remove unnecessary escape sequence

[33mcommit 1b626b441728889bb08e678472682b48c48f480d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:12:39 2021 +0100

    Fix for fix of missing __PRETTY_FUNCTION__

[33mcommit 74d38e5a8f54d6ad6d7b9d85aa9b8610a268abe8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:05:00 2021 +0100

    Add __PRETTY_FUNCTION__ for compilers that don't have it.

[33mcommit 47df391c9814a802187c771363421076da3d0d5a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:05:00 2021 +0100

    Add __PRETTY_FUNCTION__ for compilers that don't have it.

[33mcommit fbb4f778c4f414b9247d2a8051afa0c859172fa1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 10:05:00 2021 +0100

    Add __PRETTY_FUNCTION__ for compilers that don't have it.

[33mcommit 605b7686aad1071f335fbe275000586c222fab2f[m
Merge: 4002d7f df6a0fb
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 09:55:54 2021 +0100

    Merge branch 'sequence' into opus

[33mcommit 4002d7f341b53aae0fe893f429d9f73dcd250d8e[m
Merge: 0258ae8 d819c6c
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 26 09:55:06 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit df6a0fbc33180dc755a528a39a5c524183288d1f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 23:56:19 2021 -0700

    Mode changes from the combo box now use the que. There are still other
    methods to change mode which will transition shortly.

[33mcommit 95b31104f5c37227168f31dc67581d9611f4c1aa[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 19:47:40 2021 -0700

    Faster PTT

[33mcommit 5ec9a595c9ac7689c30467a2cd1f158ac4dcc902[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 16:26:13 2021 -0700

    Added PTT to the queue.

[33mcommit 7df82fc7b709993b93afaae1f9e44320daab536c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 15:43:53 2021 -0700

    Added unique priority insertion methods.

[33mcommit 3553f3cc64ee3e8fbe18726d897be4ee583f7493[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 02:31:18 2021 -0700

    Changed how commands with parameter data are added.

[33mcommit 301b48cb0264f7a63640f71180ec42d7af451175[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 25 01:24:21 2021 -0700

    Initial queued "set" command commit. Only the frequency set command is
    used so far, and only for the "Frequency" tab and the tuning knob.

[33mcommit 241f0db2c3969d9863d466f20a7f967110f8d644[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jun 24 12:40:23 2021 -0700

    Quick hack for WFM

[33mcommit 0258ae8bf9c4b0585ee68396812a800fd9b8768c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 23 20:25:45 2021 +0100

    Remove extra debugging for Opus and add warning where opus isn't available

[33mcommit d4335e100285193fe1a0ddc0dc7a7a4563098abb[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Jun 22 16:16:15 2021 +0200

    WHATSNEW

[33mcommit 2ee66afa9bed4a2db175c1891960b17498588770[m
Merge: 70c920e d819c6c
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jun 21 21:04:18 2021 +0200

    Merge branch 'sequence'

[33mcommit d819c6c93119e654a4c82ec1d756cfef7ae04447[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 21 10:49:22 2021 -0700

    Added polling button

[33mcommit 70c920ee87db57d30c0a5280f2309f14c403e246[m
Merge: 86be98f 5ef6afe
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jun 21 10:49:22 2021 +0200

    Merge branch 'sequence'

[33mcommit 8917ffabc2af5fbfc90317144aac8d01f59e4837[m
Merge: 4b88620 5ef6afe
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 21 09:20:42 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit 4b88620a5ee9db287192509ce4855313a83c8b17[m
Merge: d5a785c 5395746
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 21 09:08:09 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit 5ef6afe58c0de8933b3b9ada2eac279c31fad3cc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 21 00:41:10 2021 -0700

    Removed unused variable and related comments.

[33mcommit ac76696b440525f8e40205f63acb0655ace08088[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 21 00:36:57 2021 -0700

    Moving to std::deque (double-ended que).

[33mcommit e7d07ed7f81e12811b9d37093fb58ee907482af9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jun 20 23:46:26 2021 -0700

    IC-R8600 span is now received into the UI correctly.

[33mcommit 849ddfe8d47f8db8463426adcf8aed590fe1e033[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jun 20 23:44:24 2021 -0700

    debug (control-shift-d) lets the user input timing parameters. Just
    something for development for now.

[33mcommit 761dbd18a03fe33c927d72e5fbc951f19f3b7697[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jun 20 13:34:32 2021 -0700

    New unified outgoing command queue. Tested on IC-9700 and IC-718 (to
    remote wfview server). CPU usage seems higher but please check your
    system.
    
    Timing seems to be acceptable but could probably use some tweaks. S-
    meter polling is 25ms for fast radios, and slower rates for slower
    radios. Half-duplex serial radios receive 3x slower polling to make room
    for replies.
    
    For Freq, Mode, etc "regular" constant polling (new feature):
    
    IC-9700 polling is 5 per second, IC-718 is 1-2 per second.
    
    Just helps keep the UI in sync with changes taking place at the rig. The
    polling is slow enough that it doesn't impact anything. But quick enough
    that it catches discrepencies pretty quickly.

[33mcommit d5a785c675b90bc9190cb48febf994842ca511c8[m
Merge: 0329ea6 2508b4d
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jun 20 10:52:59 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit 86be98f958524fe5a40e851f3583719acecddcdc[m
Merge: 4b26783 5395746
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Jun 20 10:43:16 2021 +0200

    Merge branch 'sequence'

[33mcommit 53957466bb94c76cedf0ce12a61b02d345a0b18b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 19 16:18:00 2021 -0700

    Added a few more slider things

[33mcommit b5c7eaf95abdd4bdfaa04dcd22d2800024782c1c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 19 16:16:07 2021 -0700

    Preliminary slider to status work.

[33mcommit 8eb00fa38976d3db79e0bf9087fb0d07446e0185[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 19 12:39:11 2021 -0700

    No more negative frequencies!

[33mcommit 4b267830713afb35ce4e39e362f68ff503904986[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jun 19 21:37:30 2021 +0200

    whatsnew: improbed IC r8600

[33mcommit cfc29bb86f8e0769074b53211c0895c931b01c70[m
Merge: 72ca86f 2508b4d
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jun 19 20:09:57 2021 +0200

    Merge branch 'sequence'

[33mcommit 0329ea65b37462e9d45c085acfb7e7b08b9c8e6f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 19 14:09:27 2021 +0100

    Update audiohandler.cpp

[33mcommit 72ca86f1bd534ce8eb357f83f3530947fa5daf51[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Jun 19 15:07:29 2021 +0200

    WHATSNEW etc

[33mcommit 2508b4d71cd45041538637657f7913ceee012902[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 18 22:45:12 2021 -0700

    Additional support for the IC-R8600, including wider scope spans.

[33mcommit ee54e76c5f7454bea9f5a332f9e01eefebb069ee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 18 21:56:11 2021 -0700

    Minor change to remove some old debug code that snuck in.

[33mcommit 4e827b45072cb1fb33747c6b9b704038e0e86063[m
Merge: b9551ed 8024893
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 19 00:11:58 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit b9551edf36d825d0c45b33c1d6336df05bbc0a41[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 19 00:11:25 2021 +0100

    More opus work

[33mcommit 8024893a62483e06f5d0945e2c60aa67710a01ac[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 18 15:57:21 2021 -0700

    If no rig caps, then don't mess with the window!

[33mcommit 9323c2bab42a8beff28f82955ebf35f46328c966[m
Merge: 17f1863 cc92aa1
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 18 23:48:03 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into opus

[33mcommit 17f186354e4db5b8dd5f193212d23b17c34831c9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 18 23:47:22 2021 +0100

    Mono opus working ok but stereo still crashing?

[33mcommit 24480c790b1342d81882eb83cf05bc44b5e06df7[m
Merge: 6749b5d cc92aa1
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jun 18 23:08:57 2021 +0200

    Merge branch 'sequence'

[33mcommit cc92aa1b5cf932cb437cc37c4cb825cb2666abdb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 18 12:41:56 2021 -0700

    Added full duplex comms parameter to rigCaps. We assume half-duplex
    until we receive a reply to rigID.

[33mcommit 6749b5d6953641755332a2e9b9b621bc606e54f9[m
Merge: f99c963 bd1b6fc
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jun 18 15:18:45 2021 +0200

    Merge branch 'sequence'

[33mcommit bd1b6fc1de8dc117e51c3007c7ab663e977b9ddf[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 18 00:58:02 2021 -0700

    Fixed accidental s-meter timing parameter change.

[33mcommit 7a2e8560cfccf13d52f20f3af455da644d59c16e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jun 17 23:14:28 2021 -0700

    Radios without spectrum do not show spectrum, and, the window properly
    resizes for those controls. Also, a new key command, control-shift-d has
    been added to run debug functions from any tab in the program.

[33mcommit bbf981cc24d4f909e0608491a976521289df3f9f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 17 09:55:09 2021 +0100

    Trying to fix stereo opus stream

[33mcommit 74382b29eec772626999fb1380deb33d4971bb4a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 23:44:59 2021 +0100

    Enable FEC for Opus codec

[33mcommit 7c0b2a51b191e94c16e78e4480956acdab3d67b4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 19:14:21 2021 +0100

    Another attempt for Opus audio

[33mcommit db1dbef168c8ff27e10aba80c9fb664c745bf0ea[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 19:00:56 2021 +0100

    Try converting to Big endian first

[33mcommit d3db0484af6f7c95da77259cbd6c3e5d886e0662[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 10:38:54 2021 +0100

    Another opus fix

[33mcommit 579d670a75fa50057eb9b74264034ff31e15c7e8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 10:35:45 2021 +0100

    Another opus try

[33mcommit 6a5e7692e63ce63881e56eada5f17927829287a8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 10:33:16 2021 +0100

    Fixes for opus

[33mcommit 55a38c55ea8f352d30de5d83fe323d5210194110[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 09:52:47 2021 +0100

    Fix opus include directory

[33mcommit 869659ad54c0cbddfa888bf18e09a3d137c88040[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 16 09:49:38 2021 +0100

    Add opus encoding/decoding

[33mcommit 542376124b542719f5a2e0a6a718d78596d9166a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 15 13:38:16 2021 -0700

    Additional code to hide/show spectrum and correcting an issue with the
    rig name not populating for non-spectrum radios.

[33mcommit d3d59b2a94880c582c5532ac1cf4a143232bb37d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 15 11:27:45 2021 -0700

    Dynamic show/hide spectrum for rigs without this feature.

[33mcommit 2392bdd932a1faf8681bdc0fdad69fb217eb373b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 15 10:28:20 2021 -0700

    Additional data corruption checking.

[33mcommit 815591f07d4d9055c61065416e8f187160be0318[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 14 20:55:28 2021 -0700

    Changed collision detection code so that we can more easily see what
    message was missed.

[33mcommit 43d281cda71b9b608281f708c1340a106d7aa53d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 14 20:36:13 2021 -0700

    Added collision detection for serial commands. Collisions are aparently
    frequent for true 1-wire CI-V radios.

[33mcommit 9fcf959cb689aabd2ef5bb4ba074a8bc6ac7ae20[m
Merge: 885cb54 297478f
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 14 20:24:02 2021 -0700

    Merge branch 'sequence' of https://gitlab.com/eliggett/wfview into sequence

[33mcommit 297478ffca9f2847968985d9636a332c404664bb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 14 20:22:15 2021 -0700

    We now calculate polling rates immediately upon receiveCommReady for
    serial connections. For network connections, we assume sane values and
    modify once we receive the baud rate from the server.

[33mcommit 85a07881d6de15c10b9627caa404158af2bb3be9[m
Merge: 9664238 8cd64c2
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 14 20:01:26 2021 -0700

    Merge remote-tracking branch 'origin/audio-enhance' into sequence

[33mcommit f99c9638ed32ed01b485ba0dfdc37ae61ad8733a[m
Merge: e70246c 8cd64c2
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Jun 14 21:41:47 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 8cd64c2dde27ba5ab03c822d76921fdbb7f8f19c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 14 19:09:51 2021 +0100

    Add Neon (ARM) support to resampler

[33mcommit ea09e1fd3e9d4faf0289b31f3d085b6d0e055d93[m
Merge: acfb061 566cfbe
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 14 18:33:54 2021 +0100

    Merge branch 'audio-enhance' of https://gitlab.com/eliggett/wfview into audio-enhance

[33mcommit acfb0618254495dc00bbfcd31fa699df13a4c041[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 14 18:33:38 2021 +0100

    Revert to using resampler directory rather than opus-tools submodule

[33mcommit 885cb54e34ff2395dbeed5f7b6550d68c890ebfb[m
Merge: d12e4c5 9664238
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jun 13 22:21:17 2021 -0700

    Merge branch 'sequence' of https://gitlab.com/eliggett/wfview into sequence

[33mcommit e70246ce336a78381d069223868c56fefb146cd1[m
Merge: 561a1d9 566cfbe
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Jun 13 17:26:06 2021 +0200

    Merge branch 'audio-enhance'

[33mcommit 566cfbe6d6bea2076b1ae779959ddd963ac22329[m
Merge: a7e0800 e56f817
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 12 08:58:04 2021 +0100

    Merge branch 'audio-enhance' of https://gitlab.com/eliggett/wfview into audio-enhance

[33mcommit a7e0800508dbdd98441afe2f4e09aba6d30ab665[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 12 08:57:52 2021 +0100

    Add tooltip showing percentage of TX power when slider is moved

[33mcommit 561a1d951be4dabfa3de01e77f0ee229475500a4[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jun 11 19:37:52 2021 +0200

    adding a second path/way for the qcustomplot link if the first fails

[33mcommit 65744cc9347366a16e61b1bffb836545733c324a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Jun 11 19:28:53 2021 +0200

    changelog and added WHATSNEW

[33mcommit e56f8170c6674bf71c00365cbdc0bd6910ce7a6e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 12:59:42 2021 +0100

    Update udpserver.cpp

[33mcommit e6197f68cbcdc00fadca391355d6b48a127f57b4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 12:53:00 2021 +0100

    Use global watchdog rather than per-connection

[33mcommit 9fe71b4495f26f37142cabed994bcc490fe29095[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:45:04 2021 +0100

    Update udpserver.cpp

[33mcommit 013df134f9253790f65fe4bf888a720bf358f846[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:42:59 2021 +0100

    Report when users are disconnected by the watchdog

[33mcommit 721d931be4cd623ea6e2f61a7fab28efd622e6cb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:39:28 2021 +0100

    Update udpserver.cpp

[33mcommit 7ff3b860826352fd1d80192116612b25a9f97d3b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:38:08 2021 +0100

    Use watchdog to cleanup lost server connections

[33mcommit c86f96315e5812152d75a802ecd7f94948556493[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:20:09 2021 +0100

    Fix crash on disconnect

[33mcommit 15d5ed2e19da806204207dbf29268947a0ba65ce[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:16:13 2021 +0100

    Make status update after disconnection

[33mcommit 141955b3b40327c7933174eec912a10736b7c1ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 10:08:04 2021 +0100

    More server disconnection cleanup

[33mcommit 7a8dbf172b7b44f55441806a42dc8d77dc9a9d29[m
Merge: 869cde7 0becc45
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 09:56:36 2021 +0100

    Merge branch 'audio-enhance' of https://gitlab.com/eliggett/wfview into audio-enhance

[33mcommit 869cde74b847adc9653497e06ac801b36d9dcc68[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 11 09:56:19 2021 +0100

    Improve server disconnection/cleanup

[33mcommit 0becc4508f1bf6cdda22f994a4033e7d7bef0f61[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 22:32:53 2021 +0100

    remove spaces when adding extra server users

[33mcommit 35316dcd0412bc1b77cf2188de6a5d1e33f0d40e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 20:45:44 2021 +0100

    Update udpserver.cpp

[33mcommit fe5fe17588d87805fa004daa175a030b36436026[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 20:32:55 2021 +0100

    Allow both encoded and plain text passwords

[33mcommit dc46913c3f31e1bb1623651d1c225c407b38b810[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 20:30:42 2021 +0100

    Lots of fixes to server setup table

[33mcommit 73dec8ce40eaed8edc5eb0f407e97e4eef91d450[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 17:22:31 2021 +0100

    Hopefully fix the occasional 0xe1 packet from hitting the pty

[33mcommit 0c8609842f10f2bd959bb9d9c902cd39832b8471[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 11:02:55 2021 +0100

    Add more info for server connections

[33mcommit bd21a0c9c3e8a5826719a362df47292582d03a4c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 10:57:39 2021 +0100

    Make sure that user is authenticated before allowing CIV/Audio

[33mcommit c09e2a615ab989e9e567a6ce91a30093a7b96b2a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:43:56 2021 +0100

    Update udpserver.cpp

[33mcommit 8ef994358468a8541d3274182e65387457dd081b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:41:56 2021 +0100

    fourth and (hopefully) final attempt

[33mcommit 8bd41e1df8f6cc6910b7ef7344e1d87ba6590d15[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:35:29 2021 +0100

    Third attempt to get messages from server!

[33mcommit 170b494badfcca348b557f95a2b631ab8c7fc5f5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:30:24 2021 +0100

    Use correct location for statusupdate!

[33mcommit 4747dd4f61120ea959e288f1c0d0e4a26cbd9ad9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:20:05 2021 +0100

    Indicate when TX is not available

[33mcommit 999dedac1a7f99dad2703ba2709e5627cb92a998[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 10 09:09:11 2021 +0100

    Show server connection status in taskbar (only for USB connected rigs)

[33mcommit 8bc43c9f835aa6fb6fa10ae732c78e1cb64ffb11[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 9 09:24:20 2021 +0100

    Allow sender or receiver to be 0xe1 in server

[33mcommit f61925391a274891863872e94b68e7ac2e72f3f4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 9 08:31:34 2021 +0100

    Always forward wfview traffic to wfview clients

[33mcommit 9664238917c3bb64a2f4de43fa1bc141b42dce4b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 8 14:42:19 2021 -0700

    Preliminary IC-718 support. Very basic.

[33mcommit 4ce6500271cd9614e33c4296c20bb1f435a2281e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 8 19:12:01 2021 +0100

    Truncate wfview.log on open

[33mcommit ad73a3e9d28df3eeeb6d5176ffa13c044e9118ba[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 8 09:58:30 2021 -0700

    Adjustable waterfall length is now non-destructive.

[33mcommit b6cf0fcfc4298e83c028e2e1ca5da5348e1df4f5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 8 17:51:10 2021 +0100

    Detect radio baudrate in server mode

[33mcommit 87c2912210f62380fc25bef0340909596912f5fc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 8 17:50:57 2021 +0100

    Comment out rtaudio if not being used

[33mcommit 857286dae2dd89ac1eb400e51d86e0be4027f18a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 8 09:18:40 2021 -0700

    Baud rate calculations are now only happening when baud rate is received
    and reasonable.

[33mcommit b2c4bbf7f86057028542d9325b3a0a02cc7af3ee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 8 08:58:11 2021 -0700

    Better baud rate management for LAN-connected radios.

[33mcommit c6f55f34e7068f86996d2b4eb5a627bc4992f155[m
Merge: 6e03bb9 e5bad69
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 8 08:50:02 2021 -0700

    Merge remote-tracking branch 'origin/audio-enhance' into sequence

[33mcommit e5bad69d05fed01846d3684999d84e4aaeeb07e7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 8 08:08:58 2021 +0100

    remove shuttle function that managed to sneak in!

[33mcommit b77394abc7c2fa7e615d8b3f914a9559dea0bf2f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 8 08:03:00 2021 +0100

    Start removal of unused variables

[33mcommit 87b26d4ad7c96d6c332a6f69e90e005368bcf139[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 7 14:04:52 2021 +0100

    Check that we have at least 1 audio channel available.

[33mcommit 5d29dd8ac944d7baffcc675295c2ed65e67268be[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 7 12:31:58 2021 +0100

    Improve audio cleanup

[33mcommit 087ce98e4b8e0d4699f3560b662e3c3fbe472c42[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 7 12:26:29 2021 +0100

    Add extra debugging for UDP server CIV

[33mcommit caa61ee14d97cafaa2decc166019e9de317c41ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 7 10:58:58 2021 +0100

    Make only MacOS use buffered audio

[33mcommit 49ac3cb0205b60acfcb8892f91f14bd515c0cf84[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Jun 7 10:40:04 2021 +0100

    Improve mac audio

[33mcommit 6e03bb966bb66199d48072d61536f590afc0d3a7[m
Merge: f78a604 bbc81e0
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jun 6 18:59:28 2021 -0700

    Merge remote-tracking branch 'origin/rtaudio' into sequence

[33mcommit bbc81e0a88aa352a8f84ccfa37372b30d3862556[m
Author: M0VSE <phil@m0vse.uk>
Date:   Sun Jun 6 22:11:48 2021 +0100

    Fix TX Audio on Linux

[33mcommit 178bb9c6a70891e9f8a142344596df72da24332b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jun 6 21:34:54 2021 +0100

    Various fixes to udpserver

[33mcommit b3facd1140dfedb6e988b7d283815fa1f0818337[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jun 6 17:57:56 2021 +0100

    Make QTMultimedia default

[33mcommit 67927b0d56d704215faac71b33229e87768f17fc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Jun 6 17:56:48 2021 +0100

    Fix to allow rtaudio to compile again

[33mcommit f78a60482b82790deeef2510030d38ebf80b7283[m
Merge: b818e46 9a0ea32
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 5 22:37:24 2021 -0700

    Merge remote-tracking branch 'origin/rtaudio' into sequence
    Accepted "theirs" for merge issues.
     Conflicts:
            audiohandler.h
            wfview.pro

[33mcommit 9a0ea32b8880f8786150a3c1d07c40a665a9e287[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 5 08:42:00 2021 +0100

    Add latency check to TX audio

[33mcommit 0abdbd3218f3a8cebeb5027c60c522bc7f577550[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Jun 5 08:26:58 2021 +0100

    Fix incorrect use of latency setting

[33mcommit 9491005ab60aba822c5c6fb5441bb7c66ecc54f6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 4 14:38:59 2021 +0100

    Stop silly compile warning

[33mcommit eb70b92aed4bb85d06afbd1e32e2e4f34aa07005[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 4 14:25:07 2021 +0100

    Change udpserver to use new audiosetup struct properly.

[33mcommit d8a0431e36d6c4af8206f08d9a01c09594e02a22[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 4 13:47:32 2021 +0100

    Fix audio device selection

[33mcommit 6218cf5f0494b70fbe5b63a0f764167b917676cd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 4 13:33:57 2021 +0100

    Fix for txaudio

[33mcommit 24ce16b0b9bbc0e63e4fbc82636a1179296138ca[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Jun 4 08:24:26 2021 +0100

    Add QtMultimedia as default audio

[33mcommit 9de6d64dab09f37b92d2c668b8c5f5ba554a6f40[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Jun 3 12:05:28 2021 +0100

    Hopefully fix hang on exit when trying to close audio thread.

[33mcommit 2aefbe5dcddbc70ccb9b051df8713a6bdd19c8cb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 20:15:31 2021 +0100

    Replace removed code!

[33mcommit 84fd5e06319ff8940d662b0ceca7c932834b164e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 20:13:17 2021 +0100

    Use heap based rtaudio for enumeration

[33mcommit 0bc3d6adfed9a275046ccb072445ea39468e868c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 19:18:44 2021 +0100

    Catch possible exception when closing non-existent stream

[33mcommit e4ed41f3d3167c0d01cec20c892c2db2e84deeaa[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 19:16:46 2021 +0100

    Fix mac audio

[33mcommit 2d92dbbb8f1c765fea3dffa0cd6702110e2e4f46[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 18:53:30 2021 +0100

    Fix mac crash on closing audio

[33mcommit 006b2904cbb6a52343ec31b875bb6045e52a75bc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 18:36:33 2021 +0100

    Make sure audio is deleted in destructor

[33mcommit a04044e75bb8031cdc8113d80db3fc09631828b8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 18:35:04 2021 +0100

    Force specific rtaudio API depending on platform.

[33mcommit b850eb078b1a85e3a167a775654935b4dc2b887f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 18:16:38 2021 +0100

    Linux now uses librtuadio-dev where available

[33mcommit b818e4647f03a2a4a9241f831270336cb241795b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Jun 2 09:48:01 2021 -0700

    Removing local rtaudio code and using library instead.

[33mcommit 71b5cc47555638676c67ef299d3597d4713ad4b5[m
Merge: 82e57b1 2068af0
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Jun 2 09:27:28 2021 -0700

    Merge remote-tracking branch 'origin/rtaudio' into sequence

[33mcommit 2068af04e227b12ffc7daae75c348d0b64e1709b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:58:11 2021 +0100

    revert to ALSA for now

[33mcommit 462551838d6b4e9a18ed41c462078d6a8853fea5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:48:35 2021 +0100

    Tell rtaudio not to hog device

[33mcommit 69a34f1c008c423ef03b786f23615a85127f5bc7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:35:10 2021 +0100

    Update audiohandler.cpp

[33mcommit 53ab05bfeff2e984b29e49323ef57d4866069230[m
Merge: f548980 e8ad1f4
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:21:18 2021 +0100

    Merge branch 'rtaudio' of https://gitlab.com/eliggett/wfview into rtaudio

[33mcommit f54898045a4663074e6fe05bc26d3fe6bc5f2167[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:21:06 2021 +0100

    Select chunksize based on native sample rate

[33mcommit e8ad1f4e32037a17624ecad942876e0ee5e2a2ac[m
Merge: 22c5b59 887c3a0
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:10:35 2021 +0100

    Merge branch 'rtaudio' of https://gitlab.com/eliggett/wfview into rtaudio

[33mcommit 22c5b5990f42af647f0200bf0367e420365840ca[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:10:21 2021 +0100

    Remove QtMultimedia

[33mcommit 887c3a06cc280efb49934dff232c4cb0c8ad8b4d[m
Merge: f2a36d4 9df49d2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:09:00 2021 +0100

    Merge branch 'rtaudio' of https://gitlab.com/eliggett/wfview into rtaudio

[33mcommit f2a36d4cbbaf399370e71fa487dadaf46ebfd3cb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 12:08:48 2021 +0100

    Force 48K sample rate when 44100 is detected.

[33mcommit 9df49d2a64dd9fa650921fcdcccca2cb4bfcf034[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 10:46:13 2021 +0100

    change linux compiler directive to allow new-aligned

[33mcommit 66c0d18b759385cb494256b58aee8f4edb21a59c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 10:43:52 2021 +0100

    Tidy up server mutexes

[33mcommit 0f9e0085074860a4223b499e1ae90f0bcd23a149[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Jun 2 08:37:26 2021 +0100

    Fix server high CPU on disconnect

[33mcommit 4edf3f3f1dd3206c851ea65f3c029e508a5aef66[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 23:34:43 2021 +0100

    Stop deleting audio after last client disconects

[33mcommit 2112c97b1ed3ee99bb155ce9d92d128f30ebbd03[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 23:32:39 2021 +0100

    Change udpserver packet handling to be similar to udphandler

[33mcommit 27ae15af331fccaabcfd4d9b5e61c42be0bfb3a7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 20:42:26 2021 +0100

    Update udpserver.cpp

[33mcommit 1966d29ebcaedf91b7d6ca9e694b2d93c4c0c648[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 20:37:12 2021 +0100

    Update udpserver.cpp

[33mcommit 6bfb294502b58f34871aa1a756edb7969f1f4f08[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 20:19:06 2021 +0100

    Let buffer keep filling.

[33mcommit 9ab47c0866b3ab3ce94e0152e700f9d07f97f92b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 18:03:46 2021 +0100

    Change to 10ms poll time for server rx audio

[33mcommit 36c778961e5db4812e1276c3f4fd6e79b84f0adc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 17:49:48 2021 +0100

    Mutex work in udpserver

[33mcommit d12e4c5268eb85ff601190a33d0b6da372bb6bba[m
Merge: 82e57b1 c7c4a32
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jun 1 08:26:28 2021 -0700

    Merge remote-tracking branch 'origin/rtaudio' into sequence

[33mcommit c7c4a326dac325476443ece4b19af33fe20f735e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Jun 1 00:25:20 2021 +0100

    Fix for crash when remote requests tone.

[33mcommit 82e57b1fc4d7c484e9c6d5e74e94d7abdc487973[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon May 31 14:01:13 2021 -0700

    IC-7700 support

[33mcommit 00e7c0aed803121656cbb4bd5bef9fa69c2872cc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 31 18:11:24 2021 +0100

    Open pty non-blocking

[33mcommit cddb8fd0550b885e734c7d9d84c93b292b1c55d2[m
Merge: 35dd0c9 762e50d
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 31 17:55:14 2021 +0100

    Merge branch 'rtaudio' of https://gitlab.com/eliggett/wfview into rtaudio

[33mcommit 35dd0c9c6299f24cecb00d795ae79e15791a2f2f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 31 17:55:01 2021 +0100

    Fix for crashing pty on mac

[33mcommit 762e50de8ed62c17d9b3aeaad1fd6587bcf3b06e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 31 10:18:37 2021 +0100

    Fix compile issue after merge

[33mcommit 78f4b7b5081bec6e66d8ad0a7e2448849ad70397[m
Merge: 6a103d0 2368606
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 31 09:56:05 2021 +0100

    Merge remote-tracking branch 'origin/sequence' into rtaudio

[33mcommit 23686066d1349d95e0245739bd20af2df17cf89c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 30 23:42:45 2021 -0700

    Keep the theme during resize. TODO: preference for wf theme

[33mcommit 1785811e7181036ebf9c1e0f70eeba9461734c50[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 30 23:35:30 2021 -0700

    Removing my own uninformed sidenote.

[33mcommit b5167a6a97b9fb201db25f8c28a879ce3b7fff35[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 30 23:26:36 2021 -0700

    Waterfal length may now be adjusted. Let's see what range of length
    seems good and limit the control accordingly. Also there may be a memory
    leak in the prepareWf() function where the colormap is created when the
    image is resized.

[33mcommit d601983bebed59ac9cfdfa3b16ef9b0c01dc0e14[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 30 15:08:22 2021 -0700

    CIV may now be changed as-needed while running.

[33mcommit 6a103d051ffa4d1c35eabed9c1e1bcf2fdf3d753[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 30 12:07:51 2021 +0100

    Remove various compiler warnings and tidy up.

[33mcommit 05fc1677d4fd7d6ef85f760869315feadb497023[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 30 11:36:13 2021 +0100

    add silence if audio has stopped

[33mcommit d2375a90656c6f31bb84cbd0e4b924e64071b801[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 29 20:50:27 2021 +0100

    fix for mac/linux compiler

[33mcommit fba496e1d477097c4707e5d7f5f4984edda829dc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 29 20:13:52 2021 +0100

    Detect number of device channels and convert accordingly

[33mcommit 923adbaa30b836e6c16a181f67fa5f1907d99dc1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 29 18:59:45 2021 +0100

    Lots more changes for rtaudio compatibility

[33mcommit 7eeb5f08db41e4de7e486db71ef2134cfefcba27[m
Merge: 6b5a597 224c618
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 29 09:17:07 2021 +0100

    Merge branch 'master' into rtaudio

[33mcommit 6b5a59708335ef4e3353d2de75915656ad245644[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 29 09:16:28 2021 +0100

    Small change to show default audio devices

[33mcommit f2c82e2ca0e16c0ab0bc5ce83f574cc66dc0bfbc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 28 21:27:06 2021 -0700

    More chair movements.

[33mcommit 01a7be9942512316ad0ca43700d9e57c38d76ca5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 28 21:01:29 2021 -0700

    More arranging of the chairs. Also fixed a minor bug that prevented the
    "Manual" serial device entry on my system.

[33mcommit fe04dde66e16531dff02a4faf1b46dfebe47e314[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 28 20:51:19 2021 -0700

    Cleaning up the main constructor for wfmain.

[33mcommit f390badba22228d6f72660f23d8d09867817a52d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 28 18:33:40 2021 +0100

    Add some startup logging

[33mcommit 3db187e0a5ab8c0470acd4e25d7b5d3ff1b30a7f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 28 18:13:08 2021 +0100

    Update audiohandler.cpp

[33mcommit ba714adb38463d614fcf8fa3a3a33f2a84c78d9c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 28 10:48:04 2021 +0100

    Update udphandler.cpp

[33mcommit 366c2d74f7db0de12142553108d90579e830cf80[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 28 10:45:09 2021 +0100

    Change toolbar display formatting

[33mcommit d072fdc4f3abab235b09499192221da6bfcaba9c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 23:23:23 2021 +0100

    Use preferred sample rate rather than force 48000

[33mcommit 5e530b2eba23a91da2513b4c8f9af33075e755b3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 18:48:19 2021 +0100

    Latency reporting causing issue?

[33mcommit 3863c5fff9d4c7efa6a269dc3c9cc7a47607aa55[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 18:46:01 2021 +0100

    Allow higher latency

[33mcommit 2bad85bb35a241b1915b9b985353b75d9f0b36a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 18:34:44 2021 +0100

    udpserver fixes

[33mcommit b9c5194867dee7fc6de0081faa415b42d700d177[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 17:59:01 2021 +0100

    udpServer fixes

[33mcommit 5e9f812bb8f31f8ffe039f1e6ab87b4c69699c69[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 15:25:18 2021 +0100

    Update udpserver.cpp

[33mcommit e7051fcaef7a812ea7a19626c11ad1b244271a1f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 14:57:06 2021 +0100

    Update udpserver.cpp

[33mcommit 462d2cc8a28de813852676e69b161e4d9501ae7a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 14:42:28 2021 +0100

    Fix udpserver

[33mcommit 142c8a24ff766e83af0a8c058a20d0cda9cc5346[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 14:20:53 2021 +0100

    Merge from master

[33mcommit 85762365ba2e6e2ea64f4b62cba49645abe16bb5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 14:09:12 2021 +0100

    Fix for tx audio channels

[33mcommit 0c7892bd82349e72208e68613ce8999e50f123ad[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 13:54:52 2021 +0100

    Add tx audio

[33mcommit f726073e22205553988817eea284e9538bddd317[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 12:27:17 2021 +0100

    Fix for compiling on MacOS

[33mcommit a6a2168bc352939a62ce158da157a5f5119810b0[m
Author: M0VSE <phil@m0vse.uk>
Date:   Thu May 27 11:51:57 2021 +0100

    add asound lib to linux build

[33mcommit 20dd466f3b099e3e14a8b314527f99dc68e5a876[m
Author: M0VSE <phil@m0vse.uk>
Date:   Thu May 27 11:48:51 2021 +0100

    Fix qmake (again)

[33mcommit da0bf7ab73230dd1149f4fd589f69bb50a81ace6[m
Author: M0VSE <phil@m0vse.uk>
Date:   Thu May 27 11:45:58 2021 +0100

    fix qmake file

[33mcommit 596f2739b947ecd842e2ec66c38ca6c6fbf023cd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 27 11:41:08 2021 +0100

    Use ring buffer with rtaudio to eliminate mutexes

[33mcommit f8beed5531ec3352d52ad2fecdb3add650b96a3c[m
Author: Steve Netting <snetting@desktop.local>
Date:   Wed May 26 16:21:32 2021 +0300

    added Fedora build/install notes

[33mcommit 224c6183f028b26203a575daac85de78e91c6b72[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Tue May 25 14:36:03 2021 +0000

    Update INSTALL_PREBUILT_BINARY.md

[33mcommit c83f1d2afc8137d80611f3e9d0296d516e2a7cea[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Tue May 25 14:35:06 2021 +0000

    Update INSTALL_PREBUILT_BINARY.md

[33mcommit 2cd316bee6b6389f7a73247f1a57065f4c5c78d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 24 18:00:38 2021 +0100

    move ulaw to dedicated header

[33mcommit 2db02fbdd02f5bdfde80a110eb5d75ab86f203c4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 24 16:28:35 2021 +0100

    add rtaudio as submodule

[33mcommit bc7c3a1c3576fbaba5785e28e93775873ecfa6a0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 24 16:28:01 2021 +0100

    add opus-tools as submodule

[33mcommit 1393571de62a27ab176cd5b6ca3ca71000ec795d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 24 09:27:18 2021 +0100

    Add mutex for audio buffer

[33mcommit aaf6f67927f2336ef2b1f9ce5b39cc5ce5d6a676[m[33m ([m[1;33mtag: v1.0[m[33m)[m
Merge: 7ea69b8 eb3f5bb
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon May 24 09:16:17 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 6c47e2fa5ae180a5cf896751a80b7e0dd0f38f53[m
Author: M0VSE <phil@m0vse.uk>
Date:   Sun May 23 23:04:35 2021 +0100

    Fixes for linux build

[33mcommit 9c29732376cb38e187e69cdf3a28de526cf855b2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 23 22:45:10 2021 +0100

    First working rtaudio (output only)

[33mcommit eb3f5bb2cc8daca775ee84b1d0e4c40a8538c354[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 23 13:59:11 2021 -0700

    Link can now be clicked.

[33mcommit fa74fd319315899416b9a6d24a17c4c38d81b8e0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 23 13:56:44 2021 -0700

    Added helpful text to settings tab.

[33mcommit e1a4ca1614141eb20b29dfcec15fa051427080a4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 23 13:43:15 2021 -0700

    Allow entry to Server Setup for either radio connection type.

[33mcommit 8a50237a3536a5c33c7dc4f540e8e9466a375527[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 23 13:41:18 2021 -0700

    Minor change to clarify roll of Server Setup

[33mcommit ae69ef05e5ca5410d312d8752365554bae72fb9f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 23 16:09:41 2021 +0100

    Non working test

[33mcommit acfdb081ce247a5cc17da35f722a246fa4550932[m
Merge: d4f46e6 5ee6ff4
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 22 15:34:14 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit f22f4224dd1d044466265a0b7bd7efa48d2fb6f3[m
Merge: 12bf312 ed566de
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 21:09:04 2021 +0100

    Merge branch 'lan-alpha' into rtaudio

[33mcommit 7ea69b88e4ce604f1dc1d4e7706469ab113f8bb2[m
Merge: a44d217 5ee6ff4
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 22 20:18:17 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit a44d21717b05967ca566d1bc9e82055bb3c59785[m
Merge: d3fce18 d4f46e6
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 22 20:18:07 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 5ee6ff48badbce7495de76a8df630e67e6f79c40[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 17:02:51 2021 +0100

    Add debugging and fix silly error in audiooutput combobox

[33mcommit ed566deb0ecf556ede76673cb72c70d0349bc847[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 15:17:26 2021 +0100

    reenable audio buffers

[33mcommit cbad7e55fce6f7260cb671c4de284e88a27bff4f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 14:58:52 2021 +0100

    Attempt to fix crash

[33mcommit 9479aaeb3d69af13c78e9790b73b260a207532fc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 11:32:57 2021 +0100

    Make only first client have TX audio

[33mcommit 0bbb9017c910ff4d4127d449b83979c3ccef4180[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 10:43:57 2021 +0100

    Stop audiohandler re-enumerating devices on connect.

[33mcommit aab453a782378513385be59d65407fe970080e72[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 09:50:45 2021 +0100

    Stop preamps/attenuators lists growing every time we reconnect.

[33mcommit 99e71dd0e2b5b87c423bb5703aa0d3c1f2099844[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 09:24:19 2021 +0100

    Try increasing audio device buffer size

[33mcommit d4f46e60f142e24c7d8a512b783b7d16af364531[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 23:46:15 2021 -0700

    Changed method for adding modes to rigs and populating the rig menu.
    This should be easier to maintain and better in the long run.

[33mcommit 38c081baa93ddd064a5f5699cda6bcc1023de619[m
Merge: e7bbc13 a18d005
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 17:39:05 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit a18d005cee03a389d51178da423e46eb70fcbbd2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 00:52:01 2021 +0100

    "Hopefully" fix annoying UDP server crash on client disconnect!

[33mcommit 906f21855e1c5bf938f967438872bd26b8edc8d8[m
Merge: daf9d77 adebb07
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 00:32:19 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit daf9d7738da7cfbb1757cff1c576ea6af66ff685[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 00:32:09 2021 +0100

    Fix for TX audio in udp server

[33mcommit adebb076c0dbbab789f6277e2c172cd1d5282bdf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 22 00:10:22 2021 +0100

    Fix for stuttering audio on mac

[33mcommit e7bbc13d31459015ba55de82496eafddf965e999[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 15:26:40 2021 -0700

    Fixed missing break in switchs.

[33mcommit e705d071cf1afe386d504a4dc2e59e83d79bd1a4[m
Merge: 73d5503 b276851
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 15:03:38 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 73d550304cdad42dc49dcaae477fed3094eeae25[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 15:03:24 2021 -0700

    Typo in message about CI-V

[33mcommit b2768518d5886d91bfbef4111a56a383c04d9f17[m
Merge: b669605 99926d2
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 22:30:04 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 99926d2274953c9b981c680ca0e5c2d5f4507568[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 13:01:21 2021 -0700

    Dynamic timing update for all baud rates and connection types.

[33mcommit d6478d06a668dec318bb14a37330e1dfd4a8ccfc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 21 11:35:13 2021 -0700

    Fixed support for 9600 baud and lower speeds.

[33mcommit d3fce185e837c5cba0bec0257a0a426282b0bdd9[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 21 19:59:06 2021 +0200

    changelog

[33mcommit de570282b4af6a89f0b20adf0f67b0409f41dacf[m
Merge: 41fcf63 b669605
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 21 19:57:17 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit b669605e118798633f51576ad905647357a6ca5f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 16:30:52 2021 +0100

    Add baud rate detection for remote rigs

[33mcommit 07060d1f783caf2e57e2a29661a6225436f2464a[m
Merge: 4430262 43c204f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 15:56:31 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 4430262b8a786b44c5cfb5e34914a91f58d483d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 15:56:19 2021 +0100

    Correct propCIVAddr to work if less than 0xe0

[33mcommit 43c204f37f75bc2814bd18e6ed2d78ecdd07c52b[m
Merge: 61ccda9 f249106
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 10:51:37 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 61ccda9fac565ca29a26ed96ffdc01f4a5398490[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 10:51:25 2021 +0100

    Change audiohandler to use latency for tx audio

[33mcommit f2491064fdd3400987a9f56d4b6cab0a316ab03a[m
Merge: d31f9fa d776fb4
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 21 08:48:59 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 41fcf63e390116ff0b409e0f1e6857d945f05c11[m
Merge: 0e30c06 d31f9fa
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 21 09:21:34 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 0e30c06996af91f31a8330966edeb6b0005c7468[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 21 09:20:38 2021 +0200

    changelog

[33mcommit ec349cd1fca98687e49dd4c8403baf5b94d17807[m
Merge: 53afc62 d776fb4
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 20 21:48:15 2021 -0700

    Merge branch 'ui-enhance' of https://gitlab.com/eliggett/wfview into ui-enhance

[33mcommit d776fb4331d6543e01c8855b5361c618175c2b84[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 20 16:15:25 2021 -0700

    Added IC-756 Pro. Tested UI and back-end response with 7300 and fake
    RigID reply.

[33mcommit 8e85d0365f0ab66924a5c067b01480dd5a136686[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 20 15:57:30 2021 -0700

    Added additional support for the IC-756 Pro III. Essentially untested.

[33mcommit d0f95e22894716b5bfcb1910a53dd2b158a76a59[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 20 15:17:10 2021 -0700

    Cleaned up warning and UI help text.

[33mcommit d31f9fa4c10d0c5aa8adf449ef5f6600c0ed0794[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 20 20:01:30 2021 +0100

    Add usage help for windows

[33mcommit b59da5ebb8aa66a021435e0ca1e86952ad2110d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 20 19:34:36 2021 +0100

    Fix for absolute path in settings

[33mcommit 3af7b61d01e863bb2a170a22230b7267e0ad760c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 20 19:24:40 2021 +0100

    Add --settings option for settings file

[33mcommit bc4f5da9b6316c14093a5f9a939c92079891e20c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 20 17:44:40 2021 +0200

    changelog

[33mcommit b5d60632693e1d4dbb944f89df4a5c4ccb47b5e3[m
Merge: e30cf24 a717ce4
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 20 08:32:15 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit e30cf248af2e72b06b35bb3d23f0c64113b41d40[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 20 08:31:51 2021 +0100

    Add more features to rigstate

[33mcommit a717ce45657ca9b87f8a8bd4f1fb284d5599d61c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 21:51:24 2021 -0700

    Model ID text format fixed. Shows IC-0x followed by the raw rig ID
    received.

[33mcommit 75aea11c3990e10f16520d4675d0dc48ed30cdde[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 21:47:14 2021 -0700

    Fixed issue where unknown rigs were identified as 0xff. All rigs are now
    identified using rigCaps.modelID, populated with the raw response from
    the Rig ID query. Enumerations of known rig types continue to fall into
    rigCaps.model, and unknwn rigs will match to rigCaps.model=modelUnknown,
    as before.

[33mcommit 30b7f8ee0fa08063ffd6d0c409b1505d0a383e93[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 21:36:34 2021 -0700

    Serial baud rate is in the UI now. Added some enable/disable code to
    prevent confusion about which options can be used with which types of
    connections.

[33mcommit f60e3cf6213e57014c3a7d941f781fd63c9b9bd1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 21:09:56 2021 -0700

    minor typo

[33mcommit d23139e89eac9a08382d1182683042a1f249eec5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 21:09:08 2021 -0700

    Better about box.

[33mcommit a981f973fcf305e90b397a1ad6804b37f0395d4b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed May 19 20:35:45 2021 -0700

    Removed unused variable.

[33mcommit fb6510980074e1a306cbd9accbf1f0b5063ddc74[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 19 08:07:54 2021 +0200

    changelog and fixed filename in instructions

[33mcommit 7eba49497063b89ff975bb2ecc4b358e6c389249[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 20:17:47 2021 +0100

    Add MacOS serial port entitlement

[33mcommit ac3ed7480f25c428e7cb24518a2f634f855aa2a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 19:45:34 2021 +0100

    Remove unused variables

[33mcommit d90de4d199a1e1879b66ad44d6150a0423e3f860[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 19:45:01 2021 +0100

    Make windows build include git version
    
    Command line version of git must be available in the path.

[33mcommit 70fe1732fe83c910bb1b02c5c7ad7b923dc40a19[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 17:51:20 2021 +0100

    Various file tidying for Windows/Mac builds

[33mcommit ff3eecdfedb31a65c5a594989931d72cbf3a6e4b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 15:41:27 2021 +0100

    Flush buffers if too many lost packets.

[33mcommit 2b8cc4c849c1b42220dfb318a01b157db3a2ea4a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 09:37:55 2021 +0100

    remove duplicate audioPacket metatype

[33mcommit 6074372e37dfda5e713bc2e2c69d37221cdf5e9c[m
Merge: a91de00 b2bce01
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 09:35:58 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit a91de00363ab8a8cfcae62a8e9f5d0e9b6ce6e9e[m
Merge: 46213ec d907c21
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 09:33:39 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 46213ec01f90a836fe81f76448413ed47e866b6a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 09:33:25 2021 +0100

    Fix silly error in udpserver audio

[33mcommit 369ba9089766befa963bae6ace3c98a50a7e2dc6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 18 09:32:56 2021 +0100

    Allow receive only in udpHandler

[33mcommit b2bce0102842c182253fec306a1da2a6e8b7ee49[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue May 18 00:35:47 2021 -0700

    Manual CIV is now read from the preferences and populates the UI
    accordingly.

[33mcommit 7857680845fced15f926f4621645d992d2e308b4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue May 18 00:29:06 2021 -0700

    Changed UI a little, and added manual CI-V options. Seems to work well.

[33mcommit d907c213239715957473a9b580294735f89412c8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 17 17:12:51 2021 +0100

    Fixes for MacOS build - sandbox

[33mcommit 4d89d6e7c02634fa36a81eaa43ac40dbfef1a2e1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 17 17:05:19 2021 +0100

    Fixes for mac logging/pty since adding sandbox

[33mcommit bada37acdfbcfe2361a4e693fa83dc8e9c026835[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 17 16:19:36 2021 +0100

    Make audio input buffer a qMap

[33mcommit 02b93119316dc4fa22548db7d7bf3d925a553d33[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 17 16:03:53 2021 +0100

    Use qMap instead of qVector for buffers as they are auto-sorted.

[33mcommit 12bf3126c601402237624d13a303504e63bbc5eb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 17 07:46:10 2021 +0100

    Quick fix for VS project

[33mcommit 851c8a6e62486a28937838772aa5595a72405787[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 16 19:45:26 2021 -0700

    Fixed attenuator on IC-7000

[33mcommit e4edda9576100d3eb67e6d2f2f2ce608e0d67bae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 16 21:35:22 2021 +0100

    register audio metatype in wfmain with all of the others

[33mcommit ff4514a196e126fbc68800f0ff39095cf65d1b91[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 16 21:34:46 2021 +0100

    Move manual serial port so linux/mac only

[33mcommit 57bffd73a282fc7f8498ac511bc2daae56389e3d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 16 21:16:59 2021 +0100

    More rtaudio stuff

[33mcommit d304b368b697876f6bafaa6223d9d959e4e09cc5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 16 13:34:04 2021 +0100

    Test commit of rtaudio
    
    This commit doesn't actually use RTAudio for anything other than enumerating devices and is a proof of concept for each supported platform.

[33mcommit c56a5c0f05962109b013fedc39076327809336c6[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 16 13:17:57 2021 +0200

    changelog

[33mcommit 50112b751e5e378d2a7192e3c3f5a44668067292[m
Merge: 544c762 fddda52
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 16 13:14:52 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit fddda526ae08c66a30bf25b1afb11d2e48815819[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 16 00:07:24 2021 -0700

    Added IC-7200 support. This has not been tested.

[33mcommit 45e1c9d8177ac7e3ee92a2e1aaf2f5819998c893[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 16:57:08 2021 -0700

    Change to correct bug when CI-V is user-specified and rigCaps were never
    populated.

[33mcommit cb0f5f6e8434afd3f5125480e622730ab9006c2e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 15:36:57 2021 -0700

    Making the s-meter error command easier to read.

[33mcommit 861f596b687d886d79e815341ab25ffc0b9d22d6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 15:35:17 2021 -0700

    Added IC-706 to rigCaps. Support for the IC-706 is currently broken but
    development is in progress.

[33mcommit 235a0a85bea611559724e031d6992929732e4ef7[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 15:34:22 2021 -0700

    Added check for if the rig has spectrum during initial state queries.

[33mcommit 544c762742eb5808b09513d6231d5cf33f453938[m
Merge: a19e26c 8f135b8
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 15 20:14:10 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 8f135b8ddd38f38a424e31d2f684ecd86b66bf4f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 15 18:53:16 2021 +0100

    Add --debug option to increase debugging for release builds

[33mcommit a6f336c3849709de36f2d82ee00d3b0537a79cba[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 10:10:08 2021 -0700

    BSR debug code.

[33mcommit a19e26c9f3ffa69b51e1422e4406474818a2f53f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 15 19:02:12 2021 +0200

    changelog

[33mcommit d5886aa7487a564c8471cd56ce9e6955721bb13a[m
Merge: e148bc9 6d85b35
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 15 09:55:52 2021 -0700

    Merge remote-tracking branch 'origin/master' into ui-enhance

[33mcommit 6d85b358ecfe2993e3bc090f25694145c628a998[m
Merge: 9c09a36 a73a105
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 15 12:11:31 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 9c09a3600ad83a41012b767432f8305105699972[m
Merge: a115c73 e3a6207
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 15 12:11:17 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit a73a1055094371f1b7f8acff69f77357147ae1c6[m
Merge: 0e7b635 e3a6207
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 15 10:42:57 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 0e7b63500c782f0144f249102ef23830b781727f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 15 10:42:44 2021 +0100

    Small fixes for rigctld

[33mcommit e148bc94f41f68dc33b2d1dbd446138e745fd8a4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 23:58:22 2021 -0700

    Added basic IC-7410 rigcaps. Have not gone further than this with the
    rig.

[33mcommit e3a6207cbd5b849c7031c1b19367c2a9933b4082[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 20:12:16 2021 -0700

    Selecting an antenna now sets that antenna for TX and RX.

[33mcommit a0a2c025ef7725905201c22a8882051474e0c4a9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 20:00:38 2021 -0700

    wfview now closes when the main window is closed.

[33mcommit 6e087a9e4b02532f254fd6714ddc90ec82fdc173[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 19:49:37 2021 -0700

    Filter selection now checks for data mode.

[33mcommit 497b4c8743d77cfa853f4a11272d59a15b742232[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 19:04:47 2021 -0700

    Preliminary IC-7000 support. TODO: Input selection, modes, filters,
    reported bug with frequency input and s-meter.

[33mcommit 73abeb8d24faaa30d4f6a3f860e91c394201ae03[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 18:27:05 2021 -0700

    Moved the power buttons.

[33mcommit 53afc626858b46195522937c86ed71cb348f1692[m
Merge: cc4aede a115c73
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 17:30:59 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit cc4aede601dfba9bb3bfd5a11737e85a8195e593[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 14 17:09:27 2021 -0700

    Cyan for the tuning line.

[33mcommit a115c73848eabb484379f0a9ae86fda332919a6e[m
Merge: b0fc49a cd8b898
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 14 11:17:42 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit b0fc49a5b92ba7a110d45b6f9ca2d4ea9119e320[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 14 11:17:19 2021 +0200

    changelog

[33mcommit cd8b89867005fc86a4f4217ade3edfc94148859b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 10:13:00 2021 +0100

    Resize UDP port text boxes (again!)

[33mcommit 613202c18ebe56cfdd08d34d4d1ed7b31dfcfdcd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 10:08:45 2021 +0100

    Make UDP port textboxes expanding.

[33mcommit 7d50e433f5e1a1d7b4a616be51d430e7c01934f3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 09:55:02 2021 +0100

    Fixes to UDP server

[33mcommit 08f9b6e2da445e51aa06052b1a6e543836078fca[m
Merge: 94202d0 9b64b97
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 09:54:09 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 94202d0c8d570a9d7b0d10955cdd3d9e8c959505[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 09:53:59 2021 +0100

    Remove volume change debugging message

[33mcommit 9b64b979959298afbe101024dc9770e212bad883[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 14 10:06:42 2021 +0200

    changelog

[33mcommit 9373f4e54f5bc87bea099fe33c783657fd40d727[m
Merge: fc64173 bf9b4ed
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 14 10:00:34 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit bf9b4ed86d242bdbb59e7f590866637ebbb237ad[m
Merge: 8b59872 c410475
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 08:54:01 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 8b59872d080e5261313a8185e176e49189a92f80[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 08:50:53 2021 +0100

    Revert "Slight change to civ detection code"
    
    This reverts commit 6fba8c83288c31c4f70cf72a5a825bd9159a5763.

[33mcommit 6fba8c83288c31c4f70cf72a5a825bd9159a5763[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 08:45:40 2021 +0100

    Slight change to civ detection code

[33mcommit fc64173f15f733613666a98f9bbd834819e00515[m
Merge: 3e54f66 e8ea5a9
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 14 09:41:51 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit e8ea5a942792f892915aa98d90c4c9fa25dea3f3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 14 08:39:36 2021 +0100

    Hopefully improve stability of pty by filtering traffic for any other CIV id.

[33mcommit c41047521f607a9f9487ea66215d928de0556e2f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 13 23:19:37 2021 -0700

    Additional search path for macports compatibility (macOS only).

[33mcommit cf9ad0d8bc05272fdd9639592ba94005dd89b36e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu May 13 22:38:13 2021 -0700

    Slower polling for older rigs using lower serial baud.

[33mcommit d593fb9ac34d54a8e6d1f6bd332918986e42e3cb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 13 23:15:03 2021 +0100

    Fix CI-V packet length bug in udphandler

[33mcommit 70363e50e803c5fedf77011a9bc0552e79f97d76[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 13 23:13:48 2021 +0100

    Set pty serial port to NULL by default

[33mcommit 3e54f66b7fe8144218912339befb2fb17ccf73c0[m
Merge: 770f26c 65602b5
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu May 13 13:56:41 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit 65602b5e4895408c1ea4042137858cbb25ce58ce[m
Merge: ff4ec65 274e905
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 12 10:20:01 2021 +0100

    Merge branch 'master' into lan-alpha

[33mcommit 770f26cbdc392a4c38b354df0add1d3f0134c2b5[m
Merge: 7e597df ff4ec65
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 12 10:24:08 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit ff4ec65a0fe4d0849e017c8e132ecc9b6389d573[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 12 09:14:48 2021 +0100

    Fix for crash on MacOS

[33mcommit 274e905d214a7360e8cf2dd0421dbe3712a0ddcc[m
Merge: d831542 7e597df
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 13:43:23 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 7e597df042341dc88e7de9cba029665226c8fbc9[m
Merge: 973f30c 87cf5ec
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 12:20:43 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit 87cf5ec8e435bb37f76edccd4ebb7bb2ca31497e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 11 11:18:45 2021 +0100

    Make pty/vsp ComboxBox editable

[33mcommit d831542006a18b85e03132bcb3a268c42289ce6e[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 10:10:12 2021 +0000

    Update README.md

[33mcommit bc1069118e07e79e5d7df886c1dd8a9a4446468b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 11 11:01:51 2021 +0100

    Comment out unnecessary debugging for pty.

[33mcommit f648c67d8b4a8e46acf53489578db41c4f53bb4c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 11:18:05 2021 +0200

    changelog

[33mcommit d999b683c2da89bf756e8d11df61a151b6021219[m
Merge: 2878223 973f30c
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 11:16:30 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 973f30cdbd3fb6eb50c3a7b31020fbeda122fb4e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 11 11:15:43 2021 +0200

    changelog

[33mcommit 516416099c37870126bf8b31227251195656e85a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 11 09:58:38 2021 +0100

    Fixes for virtual serial port in Windows.

[33mcommit 06aa27bfb940b65f595dcf370bfcdae71643f467[m
Merge: 944b0f5 65a3f9f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 11 09:38:18 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 944b0f59877b1c4b8e86d2719c54ef2c6fcc4639[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 11 09:38:05 2021 +0100

    Initial commit of pty rewrite

[33mcommit 65a3f9f77ee1fbf1498ce34eb5902a850b6cf88f[m
Merge: 5ce4e49 4d3036b
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 10 13:29:40 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 2878223f7f1593aa951f52bdcd67f0b804bb33ef[m
Merge: 223f6dc 4d3036b
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon May 10 11:24:53 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 4d3036b4011f98b709b9ab0ea59d745ed4e33061[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun May 9 22:14:45 2021 -0700

    Enhanced accessibility for the UI.

[33mcommit 5ce4e49c5f2a00648b039da994099b9aa04e2d41[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 9 13:13:34 2021 +0100

    Changes for mac build

[33mcommit 5c1be2b62ca0fb1c873fc5f03e51c3214a0a14d3[m
Merge: 1ee4965 223f6dc
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun May 9 12:38:53 2021 +0100

    Merge branch 'master' into lan-alpha

[33mcommit 223f6dcc9c4f918cbebe8ada4ba74ce6789b7150[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 9 12:23:16 2021 +0200

    changelog

[33mcommit b041302c1a444c2816a47c88470342e0fbe4ec51[m
Merge: d4682be 908ec96
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun May 9 12:21:05 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 908ec96567da7e2eebd0014a3171b6a601842914[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 8 23:46:44 2021 -0700

    Data Mode now sends the currently selected filter.

[33mcommit 3968cdcbb08d23302689013e2c6c3561a9afcdea[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 8 23:30:49 2021 -0700

    Removed -march=native compiler and linker flags. This should make the
    binary builds more compatible, and also eliminate an issue on Pop! OS,
    where the default optimizations clashed.

[33mcommit 51fcf4f583341cdc35b180ddb945e125dfe7962f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 8 13:45:52 2021 -0700

    Preliminary (and untested) support for the IC-910H.

[33mcommit 5a034a7ebd21e6486285e5cbff233fc7c250882d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 8 13:33:05 2021 -0700

    Found a missing default in a switch case.

[33mcommit d4682beec30eb0185c73a1e9c2e086cbdf4b995c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 8 10:27:04 2021 +0200

    changelog

[33mcommit 1ee4965ea21e778ee0250bae7d224bc9d81f9a27[m
Merge: 544e64a b1a5d27
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 8 08:34:30 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 544e64af5db0c149c90b1e618bd6093d9aca8047[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 8 08:33:35 2021 +0100

    Add win32 debug mode to VS solution

[33mcommit b1a5d279ba3765294b2024b6f1166e99ba18461e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 23:43:04 2021 -0700

    Added serial port iterators for the IC-7610 and IC-R8600. Untested.

[33mcommit d16f92bd7e9bb8084f61f233576439a2a0b04d90[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 23:26:45 2021 -0700

    removing debug info we didn't need.

[33mcommit 4b11a424e2f13ab522686de53b1c1183436eede9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 23:15:24 2021 -0700

    Adding /dev/ to discovered serial devices, also ignoring case on "auto"
    for serial port device.

[33mcommit 6d3334e6e81e51132c096e407a6b16619a51f07f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 22:37:28 2021 -0700

    wfview's own memory system now tracks mode correctly. *however*, it
    needs work:
    It should track the selected filter, since this information is generally
    available and useful, and it should also be storing the frequencies in
    Hz. I am also not sure how well the stored memory mode specification
    will work across multiple rigs.

[33mcommit 228f18ca24be178e017e7e374925601beae75bc2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 15:57:40 2021 -0700

    Added more mode shortcuts: A = airband, $ = 4 meter band (shift-4), W =
    WFM, V = 2M, U = 70cm, Shift-S = 23cm (S-band)

[33mcommit 3a9967cb8c89d0f8b5ebd608f3659e4f1332b9c3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 15:46:47 2021 -0700

    Fixed BSR mode and filter selection.

[33mcommit 14b0ba2151c0fd83a4b1db0db7e12e054e87f339[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri May 7 14:52:19 2021 -0700

    The band stacking register now uses the newer integer frequency parsing.
    We also communicate a little slower to the serial rigs, which seems more
    reliable.

[33mcommit 8936c733b4e92dc7864e5c9adae5973c5b9c6d8a[m
Merge: 5435032 7206410
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 7 09:30:51 2021 +0100

    Merge branch 'master' into lan-alpha

[33mcommit 5435032dbf2685ed90ee2b519c9dbcfefec1d1c8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 7 09:28:01 2021 +0100

    Updater serialDeviceListCombo with current COM/tty port setting

[33mcommit 720641089cf0ab25163497c8d443d70c067ea5ae[m
Merge: 5720ca4 25d2670
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri May 7 10:04:35 2021 +0200

    Merge branch 'lan-alpha'
    
    this fixes RTS/DTR assertions when a rig is set to TX with RTS or DTR in CW or RTTY mode.

[33mcommit cb5144e9e11afa4a6bc805fd47f81ec54adca07b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri May 7 08:46:49 2021 +0100

    pttyHandler doesn't use pty parameter in Linux so set to Q_UNUSED to eliminate warning

[33mcommit 25d2670f980378286b045168013bb2ad704241bd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 6 20:41:32 2021 +0100

    Force DTR/RTS to false to stop rigs going into TX if USB Send is enabled

[33mcommit cf49551db0271e16a6ddb0ae8c7926b5368c8fc7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu May 6 11:25:23 2021 +0100

    Convert project to 32bit default on Windows and remove beginnings of VSPE code.

[33mcommit e041e6d1c5307d5a55571cec0a611029f351ed78[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 5 14:11:00 2021 +0100

    Add code to select virtual serial port

[33mcommit 5bd21ce6facf109c26e152983e16d86169791d67[m
Merge: 96f85a3 092a715
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed May 5 12:43:10 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 5720ca4c8e98834b3b23e81f0ba3b2a876b6175e[m
Merge: 96f85a3 092a715
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 5 11:04:29 2021 +0200

    Merge branch 'ui-enhance'

[33mcommit 96f85a3c8c154cfeef51f578208bea3cd74d59c1[m
Merge: ce7a708 458b488
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 5 10:32:19 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 458b4887b1ad92bc696831850e64e90b9dc65660[m
Merge: 7056316 ecb76d1
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 5 10:31:46 2021 +0200

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit 705631658f347c4dbc824775b189d8e6dcd370ee[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed May 5 10:31:31 2021 +0200

    fixed a typo in mint instructions and ubuntu instructions

[33mcommit 092a7152c06dca626d8f56952aaaf337706bfbe3[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 4 19:00:28 2021 +0200

    fixed the display instead of rigcaps so that ant sel starts with 1 instead of 0

[33mcommit ce7a70872ef2302be4493bb8ec9d5ce5596d8f76[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 4 18:42:14 2021 +0200

    reverted last change -- need to modify the display code here, not rigcaps

[33mcommit 64e0b0f2c4871f7511b0362301b901e7246e8a6d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 4 18:31:45 2021 +0200

    ant sel starts with '1' on the rigs

[33mcommit ecb76d10afc2892de1c577d4e894ff188526d435[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 4 15:55:50 2021 +0100

    Fix to add a blank user line in server if no users are configured.

[33mcommit 4f544a1cb449efcb9acbde123408cc1ee3ed4c55[m
Merge: c5df47f e4a187f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue May 4 09:06:55 2021 +0100

    Merge branch 'master' into lan-alpha

[33mcommit e4a187f7adcf075bba6f3d118146636b2937bbd3[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue May 4 09:31:12 2021 +0200

    small changes where to find the release; removed the src/build target directory requirement as the release unpacks in ./dist

[33mcommit c5df47f4a895d6ad6994b74084d68f438ebaabc0[m
Merge: 3715f27 46421f7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 3 20:12:13 2021 +0100

    Merge branch 'master' into lan-alpha

[33mcommit 3715f276cded5312c3410b877942bc752fa30d46[m
Merge: 98503a5 d19eaad
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 3 20:11:43 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 46421f72d2df93962e20ee17ce76811299dc9c6d[m
Merge: d19eaad 98503a5
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon May 3 11:09:52 2021 +0200

    Merge branch 'lan-alpha'

[33mcommit 98503a5a66de47bbf83a76157802488c2d96474d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon May 3 09:03:18 2021 +0100

    Fix for Built-in audio on MacOS

[33mcommit d19eaadfcdc3fdd8b6f8b57eb33d5b4be731aa0b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 23:02:27 2021 -0700

    Fixed bug 007, which allowed negative frequencies to be dialed.

[33mcommit fd87b3487f1f82e1898587c2f5da95645e8c9ff2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 22:30:45 2021 -0700

    Double-clicking the waterfall now obeys the tuning step and rounding
    option in the settings.

[33mcommit 95707c6b26e170e0f2f4d38c8771561ac2bf32e9[m
Merge: 5daeb05 fbd3f5b
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 21:53:35 2021 -0700

    Merge remote-tracking branch 'origin/master' into ui-enhance

[33mcommit 5daeb0506d6d963756e47001fc3cb694fced6fee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 21:53:05 2021 -0700

    Unified frequency-type MHz to use same code as Hz.

[33mcommit fbd3f5ba2c6ad31558641c3e92b3c732754b235b[m
Merge: 16f55b7 9c04d66
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 1 20:55:44 2021 +0200

    Merge branch 'lan-alpha'
    
    added compinling fix for mac

[33mcommit f67f31bffd8a2c306264166d3a4c08e0b618d206[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 1 19:54:43 2021 +0100

    Add WFM mode for IC705 and remove duplicate WFM mode for IC-R8600

[33mcommit 9c04d66b271a1bb14e2442ed65fd6a6388d3c252[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 1 19:42:32 2021 +0100

    make bandType start at 0 so it doesn't overflow rigCaps.bsr

[33mcommit 16f55b7a443f7d13852e25835dd805959cedbe9a[m
Merge: e3cf904 cb82472
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat May 1 20:42:11 2021 +0200

    Merge branch 'lan-alpha'
    
    moved over the code to master

[33mcommit cb82472ebe61967da0534facb8813693d5984dd1[m
Merge: d7ed1b2 4c2ee42
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 1 13:15:05 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit d7ed1b2efd0540be24cec90fb61d2d885ca0c227[m
Merge: d7927f4 a5d0c0e
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 1 13:14:44 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit d7927f4ae51c344bc99b3f78c6c49e25a9eab015[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat May 1 13:14:33 2021 +0100

    Fix windows release build

[33mcommit 4c2ee42d59eae050e4fd39ac2d84bb8dcf479514[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 00:58:30 2021 -0700

    Changed the organization and domain to wfview and wfview.org.

[33mcommit 6251cc434209c5d76c18148def8bb05c66d976b8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat May 1 00:54:42 2021 -0700

    Added more modes for the IC-R8600.

[33mcommit fa869dd2706e48ff68b4cebf2cadd1e2cb4fc4ed[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 30 22:15:30 2021 -0700

    Different timing on command polling for serial rigs.

[33mcommit 61291f2b8c129cbb65e510e11aa11c68defe74ca[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 29 17:13:14 2021 -0700

    Fixed minor extra character.

[33mcommit e6c598028701d099fbaa811d70749de6aa8851f8[m
Merge: d93743a a5d0c0e
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 29 17:02:14 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit e3cf904fbdc501626f7b2b620692bf7be972c0e4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 29 05:07:47 2021 +0000

    Update INSTALL.md

[33mcommit a5d0c0ed9b6a2b24cd7a0de56c1d64714f629afa[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 28 09:40:56 2021 +0200

    started splitting the tabs into different separate docs as at some point will end on the wiki/website

[33mcommit d93743a4f96ab432e4f56e89ed4c8881dce541a9[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 28 08:21:45 2021 +0200

    changelog

[33mcommit 8e157018c09e0dbb5c89d1f4b3779dcce925e1b8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 27 20:48:05 2021 -0700

    Minor changes to hide sat button until we have time to work on that
    feature.

[33mcommit e67f3efa4272311724c6047d5c9f754a59073355[m
Merge: 96a32f9 42acb81
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 27 20:36:41 2021 -0700

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 96a32f90698e659509f988087ccc91002852fe69[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 27 20:18:17 2021 -0700

    Additional bands added, Airband, WFM

[33mcommit 42acb81450e5fd3b6ea576023a3e6450981e564f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 27 21:49:31 2021 +0200

    added 630/2200m for 705; probably like the 7300 hardly useable for TX though. Also added auomatic switching to the view pane after non-BSR bands are selected

[33mcommit b5a943c333c213cf2439ba4845acece449d1c383[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 27 21:39:31 2021 +0200

    added 630/2200m to 7300/7610/785x

[33mcommit 53821c87928d05ef718fa0f1edae19ea291fbec8[m
Merge: e5746dc 5d5497b
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 27 20:05:00 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 5d5497b8a890faffb0856f111775c97562eb340c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 27 20:16:50 2021 +0200

    derp: fixed main dial freq display for non-BSR 4m band; see also previous commit

[33mcommit 3731ada7ac1487f7c015ea9b802620db775e920e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 27 20:15:24 2021 +0200

    fixed main dial freq display for non-BSR bands 60, 630, 2200m if such a band was selected in the band select menu

[33mcommit 839fa7f3dc44cb7971d690f095dd7667087467b6[m
Merge: 1b38c2c 06ffae9
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 27 08:06:32 2021 -0700

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 1b38c2c9d6c3805ff5be0d7a732716b808e03959[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 27 08:04:33 2021 -0700

    Minor change to set frequency, which was lacking "emit" at the start.

[33mcommit 06ffae913a8798c2b102abe2b66291b11049c18a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 27 10:08:28 2021 +0200

    changed the modeSelectCombo-addItem sequence a bit to have modes grouped; CW and CW-R are now grouped, as well as RTTY/RTTY-R; AM and FM are grouped now

[33mcommit e5746dcc82fee4cfd1a585eb9b939e579b3acf5c[m
Merge: 8cf7559 1923d5a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 27 09:01:12 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 1923d5a01c170601726789e38433b1f164444686[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 26 23:40:08 2021 -0700

    Well, that was fun. Color preferences now work, and colors can be
    modified from the preference file for custom plot colors.
    
    The preference file now stores colors as unsigned int. To convert the
    colors in the file to standard AARRGGBB format (AA = Alpha channel), use
    python's hex() function. Maybe one day we will fix the qt bug and make
    this save in a better format.

[33mcommit 7109c2b396f70efc3f33966f14d219933a9faff9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 26 18:24:28 2021 -0700

    Added dynamic band buttons. Fixed multiple bugs related to various
    differences in band stacking register addresses (for example, the GEN
    band on the 705 has a different address from the 7100 and the 7300).
    This code was only tested with the 9700.

[33mcommit 8cf7559d27ea205ad5c5f56faefc5c2d70ccd404[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 26 10:23:30 2021 +0200

    started rough docs for the usermanual

[33mcommit 7bfb30e9c68d3f5827c86ce3d57585cfec556caf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 25 11:21:37 2021 +0100

    Another linux compile fix.

[33mcommit 4f72e0ee50e73ac3a075835d2a60a21d0a5e5dc9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 25 11:14:58 2021 +0100

    Fix for linux compile of rigctld

[33mcommit 00e3a043a0fa9929df54e76fab51a787acba1a88[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 25 11:02:46 2021 +0100

    Fix typo in receiveRITValue

[33mcommit 119d328b170d05d983c58f97e4fcd4cf0d139715[m
Merge: 40ba038 80cc3e6
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 25 10:58:40 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 40ba0385ca4faf0aeaa4f0c8ccd05a101a73a4c6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Apr 25 10:58:25 2021 +0100

    More work on rigctld

[33mcommit a35609c629d48e568744e295c009135861ac1aec[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 25 00:32:37 2021 -0700

    Faster polling. Clarified in comments around line 309 in wfmain.cpp.

[33mcommit 80cc3e6dc018a4d8f627c8994e864c560ee18911[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 25 00:23:52 2021 -0700

    Added ability to read RIT status. Added RIT to initial rig query. Added
    variables to handle delayed command timing values. Fixed bug in
    frequency parsing code that had existed for some time. Changed tic marks
    on RIT knob because I wanted more of them. Bumped startup initial rig
    state queries to 100ms. May consider faster queries on startup since we
    are going to need more things queried on startup.

[33mcommit f619e9eca1589406809105a16d4d0112b9565f12[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 24 00:31:51 2021 -0700

    Receiver Incremental Tuning is in. The UI does not check the rig's
    initial state yet, but the functions are partially in rigCommander for
    that purpose.

[33mcommit 5bfb28729470616c889f89717f38348bd66837f3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 23:24:34 2021 -0700

    Found two missing defaults in switch cases inside rigcommander.cpp.

[33mcommit e5064f078927b9598f67f9b17f8eb0db0c781361[m
Merge: 9085380 93fc8a9
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 23:17:01 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 90853807eb2cfea795d3dfc1567ac3735a55de13[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 23:13:42 2021 -0700

    whitespace.

[33mcommit d4765f32871a765f88e5e8184d1f229dfacbda31[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 23:11:54 2021 -0700

    Modified rig power management to stop command ques (s-meter and others).
    Upon rig power up, the command queue is repurposed as a 3 second delay
    for bootup, and then, commands are issued to restore the spectrum
    display (if the wf checkbox was checked). I've made new functions,
    powerRigOff and powerRigOn, for these purposes.

[33mcommit fdbf1af30c34a819eaef115a56a313009e5e5f7b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 11:23:27 2021 -0700

    work in progress on spectrum enable after power off/on.

[33mcommit 89a185a174ec12d94c836c529a63e2a5a9a11b00[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 23 10:12:14 2021 -0700

    powerOff should work now.

[33mcommit 93fc8a9f678bcf42d5cf7be0ed07c27cabf25cb5[m
Merge: 6623c2c 2e5a4f0
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 21 09:17:31 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 3fa0dfaecda3674b9737afe1198551e4427e01ea[m
Merge: 2e5a4f0 6623c2c
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 20 08:54:23 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 6623c2ced035cf6ed4f574f27e51a98de8ad06c9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Apr 20 12:29:10 2021 +0100

    rigctl working (sort of) with WSJT-X

[33mcommit 2e5a4f05a440961dfb7e4106d4d93be3f22f283e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 19 22:55:39 2021 -0700

    Preliminary commit for power-on and power-off functionality. Is not
    correct for most rigs.

[33mcommit 303f3ad34146baeb2233fb7011e15fe99489914f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 19 22:18:45 2021 -0700

    Missing parenthesis

[33mcommit a8f70ba51c83a1736e0946c1d3bc84390aecd8c8[m
Merge: cb434d6 db3dd17
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 19 17:50:10 2021 +0100

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit cb434d694e809cd9bee81db93412994a369b262b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 19 17:49:51 2021 +0100

    Fix linux compile error

[33mcommit db3dd17622e57dd3e10fd27171cee97ceb0b96ae[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 19 18:48:22 2021 +0200

    changelog

[33mcommit b3758d210f89e2ee2211578565756df4c3a25017[m
Merge: c78ad19 ddc7fab
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 19 17:28:33 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit c78ad19210b2b0be373d9f0f8497bab02d14e21c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 19 17:26:26 2021 +0100

    Initial commit of rigctld (doesn't currently do anything useful!)

[33mcommit ddc7fab879e97999fa181b1e641109df50cb9808[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 17 19:44:22 2021 -0700

    Adding a default position for the frequency indicator line.

[33mcommit 63ef522e893692c323831d07d2197136b9a4a678[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 17 19:41:15 2021 -0700

    Goodbye tracer

[33mcommit dc144f0a173a0b687fbea7bae6d3a4c42bb240cb[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Apr 17 10:27:24 2021 +0200

    changelog

[33mcommit 9cd78885b11ec5ced6ba4d29c7d34c839fe41cf4[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Apr 16 11:19:51 2021 +0200

    added support info for prebuild-systems

[33mcommit e4f18dfce9cb1e103fba9a36c3a40814dbad9959[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 12 18:54:09 2021 +0200

    added airband, dPMR, lw/mw European and fast HF/70/23cm 1 MHz tuning steps

[33mcommit 3f2a66f8f87e63817e972ac35f2da7125b599fed[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 11 16:38:58 2021 +0000

    Added ATU featureon 7610.

[33mcommit ce3c4dfe54f6542504b8a130cd75b5a2644ca999[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 11 00:42:25 2021 -0700

    Now grabs scope state on startup. Found bug, did not fix, in the
    frequency parsing code. Worked aroud it by using doubles for now.

[33mcommit aac777eb07ad54654507aee5892c6a0397f70a6a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 22:48:32 2021 -0700

    Preamp and attenuator are now queried on startup. Additionally, the
    preamp is checked when the attenuator is changed, and the attenuator is
    checked with the preamp is changed. This helps the user understand if
    the rig allows only one or the other to be active (such as the IC-9700).

[33mcommit 4f3de02482bd29a2548b4d497e70d6a8a8e4aed4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 15:34:35 2021 -0700

    Added frequency line. Let's see how we like it.

[33mcommit 5cc2aa0efe4ae74035a6aa5180c582cee15c9813[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 00:21:58 2021 -0700

    Add some preliminary parts of getting the attenuator, preamp, and
    antenna selection on startup. UI not updated yet but getting there.

[33mcommit ec1e43ddc1c183d5a8ec7a8849d9b5ccc6e80d47[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 00:03:21 2021 -0700

    More stale comments removed.

[33mcommit 4f696bee59b15ca5163535d515c5ca4a6d0b9666[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 00:01:34 2021 -0700

    Removed more stale comments

[33mcommit 651edb2c304ef4fec800c731d1d1bc0493871d0f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 10 00:00:51 2021 -0700

    Removed old comments

[33mcommit cdb680422b31225f1def84961c9fc163ec9881d9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 23:56:04 2021 -0700

    Moved ATU controls to main tab.

[33mcommit e7e1431fe3d7fc7d285cc16905e741feaa03e915[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 23:38:36 2021 -0700

    Added waterfall theme combo box

[33mcommit f25524a43425c0f6b0b080c24707af635dae38f2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 16:29:04 2021 -0700

    Removed buttons from attenuator preamp UI controls.

[33mcommit 6b967a52b9dee3c5d99b034f6cc3dca74ed6ed75[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 15:57:16 2021 -0700

    Antenna selection might work, untested.

[33mcommit aff2e281e805e96f824dff0c43fe310b0cbb7d14[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 10:04:20 2021 -0700

    Preamp code is in. Can't read the current preamp state yet but we can
    set it. Nicer names for preamp and attenuator menu items.

[33mcommit 39d6fe0baba9f8d15dd58d0383b2cf12f1be2b9e[m
Merge: 84909b4 6595ce7
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 09:42:15 2021 -0700

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 84909b4a13f337606adf9551a6435bbf128e9aad[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 9 09:37:54 2021 -0700

    Added some helpers to the destructor in repeaterSetup. Not sure if it
    helps, but it hasn't crashed on exit yet.

[33mcommit 6595ce78d544b84682e8e731e42ffcdaf5e828f8[m
Merge: c8d2538 c68fc5e
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 9 12:04:25 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit c68fc5e99c02bdddf587e962de53603592c0439d[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Apr 8 18:57:20 2021 +0200

    re-added lost tooptip for SQ slider

[33mcommit 3d256b3863866d53b8dba003317ea49b031ab46f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Apr 8 15:06:23 2021 +0200

    changelog

[33mcommit c8d25385060899b66f0ecba5cbb788de032e5d75[m
Merge: a13d0b7 2cbbb26
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Apr 8 09:19:56 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 2cbbb26f0001cb501b7a9ac2d738d214843c4186[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 8 00:05:40 2021 -0700

    Preamp data is now tracked (but not used yet)

[33mcommit 5c84d31dc8a401b9f0a705f2f2589a14bdbb0d0e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Apr 7 23:43:18 2021 -0700

    Minor disconnect in the getDTCS call

[33mcommit 09ec3c271c7e0a6f4f4d7472a3f179bdf7e76067[m
Merge: 92f019b 60146a1
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Apr 7 23:38:06 2021 -0700

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 92f019b78798aecfdcb9c0e18ada5301430cae77[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Apr 7 23:35:24 2021 -0700

    Attenuators are in! Please try them out!

[33mcommit a13d0b711b4027ceaa7f29606d9a18abaf425fd1[m
Merge: e1cc2de 60146a1
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Apr 7 09:40:37 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 60146a1dc81b0046b290bd81614e94aa1dc1fd12[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 7 09:58:40 2021 +0200

    added prebuilt and run instructions for mint 20.1

[33mcommit 34751527d6f63518f4f4ca09e63bb2a4ffbcd481[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Apr 7 08:22:38 2021 +0200

    changelog

[33mcommit d18bb2b6497262bf94596660100a20866bcf8696[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 6 22:26:08 2021 -0700

    The repeater setup now disables elements for things your rig doesn't do.

[33mcommit e45557e04d71dca13daf69bc12cb7999754edf3f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 6 22:06:08 2021 -0700

    We now query the repeater-related things on startup, such that the
    repeater UI is populated correctly.

[33mcommit 9a622dfb21ba45cbc2bfa19fc3a226bf8081df74[m
Merge: 83ff116 2b03a3c
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 6 21:01:54 2021 -0700

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 83ff1161f796fd17568ac6b207291d56c5633c28[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 6 21:01:49 2021 -0700

    removed some debug

[33mcommit b22ab6bdc250af5c544130363d4aa0dbd49f7a34[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 6 21:00:47 2021 -0700

    We now have KHz as the assumed input format if there isn't a dot in the
    entry box. Enjoy that!

[33mcommit 2b03a3cfe44f78a01268d5e955f2ea254f23545a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 6 19:46:21 2021 +0200

    derp: typo.... some of these days..

[33mcommit cec27f6bb033d20a4a32fac27b5aef1cc50d9bb5[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 6 19:45:17 2021 +0200

    derp: changed formatting

[33mcommit cdb3f595163650619f1678593608bd81aa93d12e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 6 19:44:35 2021 +0200

    changed formatting

[33mcommit 8d39b0cb68bf9750472952727b1e3c9b3ab2d16e[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Apr 6 19:42:49 2021 +0200

    temporary instructions how to install from a tar.gz prebuilt version

[33mcommit 6b09aace4ec57b024c062ffaae1ca812b89f6259[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 5 22:56:03 2021 -0700

    Minor change so that we track the selected tone to mode changes, keeping
    the radio in sync with the UI.

[33mcommit e22cb0edf203d0668865df331605c68a317f24ff[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 5 22:34:40 2021 -0700

    Tone, Tone Squelch, and D(T)CS seem to work as expected. Mode can be
    selected.

[33mcommit e1cc2de05481b959d672645c236edda24bdf0278[m
Merge: fb19407 d30797a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Apr 5 14:58:56 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit d30797a8fcdc7897f927ffe49cf8a9ecf6a31419[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 5 10:00:04 2021 +0200

    removed 150 Hz CTCSS / NATO as it can't make that by itself

[33mcommit 4b956aa477ebe74645dbc2991e9a17b1b3f23972[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Apr 5 09:53:38 2021 +0200

    added 77.0 Hz tone to CTCSS

[33mcommit e4bbf62474d5faac84f852c06aa272948f44e40f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 4 23:46:44 2021 -0700

    We can now read the repeater access mode and update the UI. What remains
    is to be able to set the mode.

[33mcommit e7f2d1eba8a5d0f588070b2429fed13637801207[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 4 23:33:56 2021 -0700

    Working read/write of Tone, TSQL, and DTCS tones/code. Some code is also
    present now to change the mode being used (tone, tsql, DTCS, or some
    combo of the two).

[33mcommit 8cc78e37bbf82d114e7a8e2a1010741feed28b2c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 4 00:36:21 2021 -0700

    Tone, TSQL, and DTCS code added, but not complete.

[33mcommit fb19407c10d01d462ca7fb8217114b6b61a59f33[m
Merge: b6e4543 ebc6943
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Apr 3 16:24:00 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 3f0ea7258cc052e9ffe1370756b51f229ac8843a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 3 01:50:07 2021 -0700

    better tone mode names

[33mcommit ebc69434ab2caba03f3b55194bafa54eb8c78c2e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 3 01:26:28 2021 -0700

    Started work on the tone mode interface.

[33mcommit c6ecde8aa757e33406585074dd55c25dbc199885[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 3 00:46:05 2021 -0700

    Added CTCSS and DCS codes to the repeater setup window. Have not
    connected to rigCommander yet.

[33mcommit b6e4543ecf7f5ba0205707ff7af54fcb8fa4b5d0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 2 12:00:51 2021 +0100

    Add repeatersetup class/ui into Windows build

[33mcommit 11d3f2fc14a0fdcc6c363a2514e788ad0c45a207[m
Merge: a0b8abf d2dea24
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Apr 2 11:58:03 2021 +0100

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit d2dea240f572e2282573c39383c435e1b3dc4db6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 1 15:43:54 2021 -0700

    ui names changed to protect the innocent

[33mcommit 225e395d34654e3f657ca8cb5fedda0a2b978153[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 1 15:42:22 2021 -0700

    Added placeholders for attenuator, preamp, and antenna selection UI
    controls.

[33mcommit 36b6d9b6781ff6ee2981958d2034609dd1220f71[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 1 15:28:09 2021 -0700

    Moved some repeater-related things out from rig commander and into a
    shared header.

[33mcommit a0b8abf40c2c5d68d6077aa87d4e5c3504080a90[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Apr 1 11:07:30 2021 +0200

    added tooltip for sq

[33mcommit 8bf45ac5b2c3f8a94460f370489511a556e003e9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 31 00:29:22 2021 -0700

    Adjusting signals and slots for repeater duplex.

[33mcommit 69e302febadc6db7d1e33b306f8d56513bfc515f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 31 00:14:20 2021 -0700

    Basic duplex code copied from wfmain to the new repeatersetup window.

[33mcommit 864b2f491a8f8f8398dce5a542d19f8317438cfb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 30 23:37:35 2021 -0700

    Adding code to support a separate repeater setup window.

[33mcommit 09b88c548f6e25fe6def3c065eba4a1b1c684ee0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 30 21:54:27 2021 -0700

    Added conditional to debug on serial data write size.

[33mcommit 8a979d56ef5968f1525c44cdd617354980dc3338[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 29 09:38:31 2021 +0100

    Fix crash when radio is shutdown while wfview is connected.

[33mcommit ee4c8607d5fc3877faae9cc298f16f41c58d4deb[m
Merge: a8e895e afea24e
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 27 16:07:36 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit a8e895e654036cf47ddf061bc571e52e774a2a27[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 27 16:07:17 2021 +0000

    Allow user to configure client name

[33mcommit afea24ef87da52d7b7335c2b774df3e501deb600[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 24 08:01:13 2021 +0100

    updated build instructions for opensuse, sles, tumbleweed

[33mcommit 35e9a3b4080b5ae3bf3657d0f5f5ce53e024baab[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Mar 23 21:16:47 2021 +0100

    updated suse buidl instructions/finetuned

[33mcommit 37193db2659bed481e7d9db81bd3fda776a7edae[m
Merge: 6bba4eb bfdbf3f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 23 18:20:01 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 6bba4eb6dc6e3998c44ecaab942a5c3904c17b21[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 23 18:19:47 2021 +0000

    Set AFGain to 100% on LAN connections

[33mcommit bfdbf3f615135bc3f5aaec608e9b06334f838d3a[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Mon Mar 22 20:30:57 2021 +0000

    Update CHANGELOG

[33mcommit 09f464dfea4590835d6edb51f56de7747b7918fb[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Mon Mar 22 20:30:34 2021 +0000

    Update CHANGELOG

[33mcommit f936eb9167808b80d5b9e2d4f7abd372cbbeebd4[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Mon Mar 22 20:30:07 2021 +0000

    changelog

[33mcommit c2091e9c4ea6ea624dd1425a39aba7ff727ca3dd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 22 18:53:34 2021 +0000

    Add local volume control for UDP connections.

[33mcommit 54daaccdfaf1296301c96022023d4c4c31d48f30[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 22 16:02:22 2021 +0000

    add volume control to audiohandler

[33mcommit 7cb9ae9195af04b1bf237f4d508e7b0af7664aa0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 22 15:16:41 2021 +0000

    Small fixes to UDP server

[33mcommit 446ed4ef26e94bcc7974fc50bd502590a654d98f[m
Merge: 8583973 a26a352
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 22 09:11:52 2021 +0000

    Merge branch 'ui-enhance' into lan-alpha

[33mcommit 8583973ca8e5775cc6151b1421877ca4b9500b7a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 22 09:10:03 2021 +0000

    Add USB audio handling to UDP server

[33mcommit a26a3523896a0e5d30d0ce4965a4457b65ae6bf9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Mar 22 00:11:43 2021 -0700

    Changed frequency parameters to (mostly) unsigned 64-bit ints. This
    makes all the rounding code very simple and removes many annoying lines
    of code designed to handle errors induced by using doubles for the
    frequency.

[33mcommit 6ac94d08769ac0a1f6aa2a70f08dcf515c3af7d1[m
Merge: 41553be 274123a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 19 11:58:14 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 41553bed247e0cd9c0d6ddf38deed0957eb55e1f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 19 11:57:40 2021 +0000

    Revert "Add macos icns file"
    
    This reverts commit e27ea3f46c0ce0e7a8becb26075d8ca04304d7e2.

[33mcommit 274123a61d3fe5c179074445f3da2641807c31bf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 19 10:44:05 2021 +0000

    Add MacOS X Icon

[33mcommit e27ea3f46c0ce0e7a8becb26075d8ca04304d7e2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 19 09:07:05 2021 +0000

    Add macos icns file

[33mcommit 96949f590404a19c548dd1aa6624e4c6d971bdf1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Mar 14 20:06:20 2021 +0000

    Various changes to incoming audio handling to improve MacOS

[33mcommit 03de9b0dfdfedfc60c875fec2294652af5e016a6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Mar 14 08:44:30 2021 +0000

    Small fix for audio packet handling

[33mcommit 3187651098f47c858d27a3b6403a6bad7d9a15bb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 18:56:19 2021 +0000

    Revert changing hex to Qt:hex as Qt:hex not exposed in earlier Qt namespace

[33mcommit 4210e84e4a44f1a61b5932e3b2443ed577a34fc2[m
Merge: 7f4be42 c7430cd
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 16:57:12 2021 +0000

    Merge remote-tracking branch 'refs/remotes/origin/lan-alpha'

[33mcommit 7f4be4233186d6dd3ff12ea83521683b0065cd06[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 16:56:48 2021 +0000

    Fix some compile warnings and deprecation.

[33mcommit c7430cdc2dbc857c83ad23b67ca593130e623f5c[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Mar 13 16:05:32 2021 +0100

    removed the section: How to Apply These Terms to Your New Programs

[33mcommit 9e1580cb8682ebc8543d77b70bec48270bcf9b4a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Mar 13 15:20:32 2021 +0100

    added the original content of the license/copying info for the resampling code

[33mcommit d9b1fb2a5fd198621947dfd860a62eaa71720e8b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 10:20:48 2021 +0000

    Make stylesheet work on Mac OS

[33mcommit 39a5e51aa6a0bf0dc04b0b0542fe3cdf56336333[m
Merge: 607b3c4 4ddca5d
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 09:54:44 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 607b3c433379b6c0f90ddc34601e9d61d89ca6eb[m
Merge: 40d0c99 ade8c5b
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 09:53:47 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 40d0c9996480a1f4570853ce980ffabd6e208673[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 09:50:43 2021 +0000

    Some code tidying in udpserver/audiohandler

[33mcommit ade8c5b6799ec507f37a0a842f9133daf722d630[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 09:33:39 2021 +0000

    MacOS compile options added

[33mcommit 18fe0cba2e6175ede9375a8cae2a770e6c77af30[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 13 09:23:06 2021 +0000

    Fixes for MacOS

[33mcommit 4ddca5da3bf4ef2ce90c2d8f28e9a99d828ff1b9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 12 23:19:43 2021 -0800

    Minor neatness change.

[33mcommit 37ca39a8e76dab7506aa6b599c6ea2eadc6f779c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 12 23:16:57 2021 -0800

    Changed some debug messages in commhandler. I found it useful to print
    out the port name when connecting.

[33mcommit 01a2bf3ec22a7ac90864b20f2c71ddc4b388effc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 12 23:15:31 2021 -0800

    Quick cleanup of tuning dial code, mainly spacing.

[33mcommit 3b22ed2e113bb499c589399ac2e9f8cde7390f4b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 12 23:13:37 2021 -0800

    Fixed tuning step rounding code for the tuning knob. It is not elegant,
    but it works at all tuning steps quite well. Feel free to improve upon
    it, but take care that improvements work at each tuning step, tuning
    direction, and at the zero-crossings.

[33mcommit 60455d9d3761161d2696407b26235cdc43867e3b[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 10 16:54:49 2021 +0000

    Update README.md

[33mcommit 157f87e7b32f3506d204735e707f1c5235a9c012[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 10 13:37:39 2021 +0000

    Update README.md

[33mcommit 6e43fa4bd15233affee97fc79062eae860b3a41f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Mar 10 08:06:28 2021 +0100

    changelog

[33mcommit c610edd371c10d962bb4ba17c83d37ce120cd2fb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 19:53:28 2021 +0000

    Clear buffer so resampled 8-bit audio works correctly.

[33mcommit 7cdc432f48e7427838d9afcaebf49f4828e69859[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:42:06 2021 +0000

    Final buffer size fix for Linux (hopefully)

[33mcommit 84db35f9f732303d1ecab65c2ee5df9a33d0b348[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:38:13 2021 +0000

    Another linux buffer fix

[33mcommit 71d271957d592ce44d4b270534ad61bb49482c9b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:37:00 2021 +0000

    Fix buffersize in linux

[33mcommit 77b2a63b421a74a7fe145f9aa27c326dbe8a8f17[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:34:22 2021 +0000

    remove intermediate files so they don't get accidentally committed.

[33mcommit 343795184c4cd4043f5254f78c4aa075a1de82fe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:25:41 2021 +0000

    Fix error in qmake build file

[33mcommit a0f4a4deeb5a597ef31411c6fe7be55cb7ef2fae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 17:22:16 2021 +0000

    Add audio resampler

[33mcommit 998381ac00ca317fb156c873f3c9220527f5f1e7[m
Merge: 38fbc35 4df9b66
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 9 00:03:22 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 4df9b66d8b0cd61961dde962df254a3613885f23[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 7 15:53:34 2021 -0800

    Removed extra debug.

[33mcommit 38fbc3553524dbb20111ee3d74902f9cebaf5ee9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Mar 7 09:55:28 2021 +0000

    Better detection of transceive command

[33mcommit 3e2862e4aa1e0a73f8b68ddb0928596a7799b33f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 7 00:05:13 2021 -0800

    Better scrolling speed on UI sliders.

[33mcommit ce6cc451b93264f0974785c1ef67da6b9708cff5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 6 23:35:46 2021 -0800

    Added serial port drop-down population code. Untested.

[33mcommit 5a611f5c943c22b91b4a9caa4355097070abbb00[m
Merge: e6f2aa3 5911af1
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 6 22:29:27 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit e6f2aa35b9bb8b2bc2d6898a8cd436e1f6df7269[m
Merge: c490502 1e16059
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 6 22:29:06 2021 -0800

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit c490502f0153deb892b0efd6b81139d24e13aa79[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 6 22:28:13 2021 -0800

    bit of planning

[33mcommit 5911af17174f6bbe3689aa0fae29bb7907983956[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 6 14:40:43 2021 +0000

    Missing file from last commit!

[33mcommit 261794c1c8bcb1f0f29e6ed5aac647142270cc1a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 6 14:33:31 2021 +0000

    Change authseq to be 8bit which hopefully will fix the 4 hour disconnect issue.

[33mcommit c0c339d5c2451800bb87a2e576560fb40243a75a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 6 09:14:28 2021 +0000

    Fix silly error in audioinput/output comboboxes

[33mcommit 7f7e4b9d521106265a678b5b2bbb871f673d2a96[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Mar 6 09:07:23 2021 +0000

    Stop input combos growing every time rig is reconnected.

[33mcommit 80e0e808e29ada2821e14d9eaeea68282bd61625[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Mar 5 12:53:23 2021 +0000

    Fixes for pty

[33mcommit ce55c560af205fd7f72c2e24bd3aa54beee95dbe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 23:48:50 2021 +0000

    Update pttyhandler.cpp

[33mcommit 84017949c688d4cf55fd35d5b62329a16b78153c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 23:45:22 2021 +0000

    Update pttyhandler.cpp

[33mcommit 51883a4e0e22870c53cfb20d0a78c10a9673ca51[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 23:44:45 2021 +0000

    Revert "Update rigcommander.cpp"
    
    This reverts commit c3b8f32bb65a7d821bfb2bf0d83a3f0d5c8f959a.

[33mcommit c3b8f32bb65a7d821bfb2bf0d83a3f0d5c8f959a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 23:44:22 2021 +0000

    Update rigcommander.cpp

[33mcommit 07a6889d80a41129e2e1cfa20af4bf708a4eede9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:19:41 2021 +0000

    Update pttyhandler.cpp

[33mcommit 019bed282306cc27c3e84c41ef2fedf13787dc98[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:17:06 2021 +0000

    Update pttyhandler.cpp

[33mcommit 099e3561ed8c6c76a621063ad88177fb52ae5217[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:15:04 2021 +0000

    I must be getting tired!

[33mcommit f1bf68654fc1610923a711ad076eaed55253f3f6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:13:21 2021 +0000

    Update pttyhandler.cpp

[33mcommit 868e1105233a3ecaeb45c6423391e02f011b29e1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:11:59 2021 +0000

    Update pttyhandler.cpp

[33mcommit 4accf1b559833371635539b101a23f6ce49c5a1c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:10:00 2021 +0000

    Update pttyhandler.cpp

[33mcommit 229a2bc32fde109603da2c64a19f307d5a8cb6ee[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:06:25 2021 +0000

    More linux fixes

[33mcommit 4286504d48f6918ebf8d6b0d283304bf7b03eef0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 21:02:56 2021 +0000

    Add debugging

[33mcommit 6dbbfcd825b392ac37c64f450d80cb5e615fcdf4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 20:30:37 2021 +0000

    Another pty bug

[33mcommit 859164af575255b14e2a2432a9c1504184753a27[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 20:27:50 2021 +0000

    Fix linux pty issue

[33mcommit d31cf07c4713183b1cb8ae485b4c7d48dd799c8c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 20:23:06 2021 +0000

    Fix typo

[33mcommit 7c18c944b22af0afebefee95189c91d647a68d1c[m
Merge: adaabec 565c8c7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 20:19:30 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit adaabec5759a7c57a432fc6cb439a493203c1634[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Mar 4 20:19:05 2021 +0000

    Move pseudoterm into dedicated class

[33mcommit 565c8c7850abc02036c64676feca52749faf1ffa[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Mar 4 19:29:32 2021 +0100

    changelog

[33mcommit 1e16059a9eabc0ddd4223e0c9417587aed870eee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Mar 4 08:32:58 2021 +0000

    removed annoying message.

[33mcommit 145b8d45a5f845c3c64fb98f631b9c8f2ad3fc75[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Mar 4 00:02:53 2021 -0800

    Tuning steps! Still need to zero out those lower digits while scrolling
    if box is checked.

[33mcommit 16a73297376b77df9bc0e94be763cb6a5ae4a30e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 3 23:25:01 2021 -0800

    Added better destructor to commHandler.

[33mcommit 2af85f31b12c6bba326bc0f3d917bc5a672482e8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 3 10:36:43 2021 +0000

    Fix spectrum peaks as well.

[33mcommit 32d66d06893ccb7709973ddbfc503c7dcc3bb669[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 3 10:34:56 2021 +0000

    Fix spectrum 'flattening' with strong signal
    
    Issue was that internally QByteArray is stored as char but the actual value is unsigned char.

[33mcommit 334633a5e55fb21687d32a56624cdb156a223019[m
Merge: f5117fd b6440c7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 3 09:58:16 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit f5117fd9215aba98a4484df410aa6fd4c4c34b48[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 3 09:50:15 2021 +0000

    Add separate mutex for udp/buffers.

[33mcommit 449b017527c5a19f85705d2b3e0ec053b66741c1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Mar 3 09:49:49 2021 +0000

    remove redundant code from audiohandler

[33mcommit b6440c7973a1f4007724347285ea79857f2bd3d0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 2 23:26:20 2021 -0800

    Removed old testing code.

[33mcommit f1cb92c99690611079359e96e94a3bf5f0ae06a9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 2 22:11:01 2021 -0800

    Quieter debugging.

[33mcommit 60756480ce16f1e44ecb42cfab6f7c6aa3db5c46[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 2 21:55:33 2021 -0800

    Changed serial and LAN enable controls to "radio buttons" so that they
    are exclusive selections.

[33mcommit 260b48f07f01572850d27973c217da1cd5604000[m
Merge: 8a04c23 90584c5
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 2 20:47:56 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 8a04c23d7f700c8d19e1392b6e8aa8c2c5c65603[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 2 20:47:20 2021 -0800

    Started work on sending out the current span to the UI of the spectrum.

[33mcommit 90584c5db408bbb51bfef1db05426a022fe4af49[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 15:41:48 2021 +0000

    Update audiohandler.cpp

[33mcommit e5f1ac31a821436da3915d485193c4f536b6f9db[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 14:35:33 2021 +0000

    Identify if we have missing audio seq numbers.

[33mcommit 1edcd6cc59ca7e855cb2ce408fccd757a915600b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 12:06:46 2021 +0000

    Update audiohandler.cpp

[33mcommit 8eaaa014a01c73e1d0d69656c9061af77a00f8ac[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 12:00:13 2021 +0000

    Update audiohandler.cpp

[33mcommit c01fda2fa717960827203e6ff2dc4f61385b3d34[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 11:57:02 2021 +0000

    Update audiohandler.cpp

[33mcommit 8c48bf8f2ebfe616671b5a14636d35d6e36797c3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 11:53:48 2021 +0000

    Update audiohandler.cpp

[33mcommit d2246234cdb2554f02f0ada4ded23f9a303aa624[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 11:50:42 2021 +0000

    Audio debugging

[33mcommit af6131708ac19203337d62cd208a3b6fa7d5bf37[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 11:12:12 2021 +0000

    More audio fixes

[33mcommit e3ee4eddddec0cd0424cbdeb1964b8bab270ab55[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 10:26:08 2021 +0000

    Update audiohandler.cpp

[33mcommit 0a02ae06e14d9f345ca0543898e3d5cf16c4214f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 10:22:41 2021 +0000

    Update audiohandler.cpp

[33mcommit 8114944e9cac29ce617053edbeb2fa42fdff7244[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 10:19:08 2021 +0000

    Another attempt at improving Linux audio

[33mcommit d4d2c3f1e7378690005c023c69795e1acfcfc4d7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 10:07:08 2021 +0000

    Select default audio devices if init of configured ones fails.

[33mcommit cfed58e84eb1940b4d12f5822fd0fc42e2a32a8b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 09:49:10 2021 +0000

    Try delaying RX audio until latency/2

[33mcommit 3ca21161bf796c6495e3fe1c46f51f39f5d0e22c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Mar 2 09:48:26 2021 +0000

    Updated VS2019 project

[33mcommit fdad4fd6eaea0f319a3e90bb763cf41b75d74541[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 20:44:09 2021 +0000

    Select default input/output audio devices if configured ones not found.

[33mcommit a254a937ff28d40ede8c44125018183446ec7912[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 20:31:05 2021 +0000

    Allow selection of audio input and output devices for UDP Audio

[33mcommit 91a60bf9189e187cda43a7cb7ca243a2d63c1e31[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 19:53:12 2021 +0000

    Create udpPreferences struct and remove civPort and audioPort from UI

[33mcommit 7491c10c4d6486030f036d155c6b40d63121fb94[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 16:30:16 2021 +0000

    Fix rigname width on Linux

[33mcommit 400144a28e2a1e7d9cfd10e9ebf1807b6d9de984[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 16:28:15 2021 +0000

    Force 48K audio mode from server

[33mcommit 6b795e1d6dc99ebea0e78d0f10364bcb900a0977[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 16:26:59 2021 +0000

    Add PTT Led and rigname to Permanent Widgets on Status Bar

[33mcommit 18b287a047575cf0204e6d88d96c64bfd3cb4613[m
Merge: b4ea60e 5577aeb
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Mar 1 08:30:04 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 5577aebf8b5dc602d5df4f062a225de66f19e362[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 28 21:47:45 2021 -0800

    Removed the last annoying legs of the center/fixed two-mode system.

[33mcommit 5a673e256ca117dfa9089835b2ddc9766095a935[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 28 21:41:27 2021 -0800

    Added support for Scroll modes in latest firmware. Removed setCenter...
    signals and replaced with setScopeMode signals. Fixed a bug where we
    could not query the scope mode.

[33mcommit b4ea60e5d25a460a433e54c205b4928628a2dc4c[m
Merge: f82e2c9 c74d5d4
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 28 20:23:31 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit f82e2c97466796923f7439c129900937b23e5caf[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 28 20:15:34 2021 +0000

    Second attempt to fix Linux warnings

[33mcommit 844214126df94b574fb57900aed3d009f8487f5f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 28 20:13:45 2021 +0000

    Fix inevitable Linux compile warnings

[33mcommit 8455cd3dadcaf2ae9004e883de2eb5236bf69934[m
Merge: aef03a6 3b6e432
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 28 20:10:25 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit aef03a6b7ee8cf49a19761fa5d5691eaa1cfa959[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 28 20:10:07 2021 +0000

    Add audio handling to UDP server

[33mcommit c74d5d448dcdf8b8cc11576f4203de5b0ce3d0ce[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 28 00:54:37 2021 -0800

    Removed some older methods of command que.

[33mcommit 8c244e75d301000229ea959cdc9abd2920a325b3[m
Merge: c49c4d6 005b25a
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 27 23:47:26 2021 -0800

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit c49c4d687bcdaf47b61574e2e166c6df869e7f37[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 27 23:47:09 2021 -0800

    Fixed go to frequency button that didn't properly check results.

[33mcommit 2bd6231009549bf9083ba142224a9074524d1ffe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 27 13:35:19 2021 -0800

    Minor fix for combo box resize. This is needed since we dynamically
    update the combo boxes and don't want to see scroll arrows in the combo
    box.

[33mcommit 005b25a63562a04b508ced663d80709f735db379[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Feb 27 21:18:26 2021 +0100

    added 0 to model number in reference command to 7300

[33mcommit 3b6e432132fc19c03d11e6c3b89babcc9ea6ff32[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Feb 27 17:00:20 2021 +0100

    changelog

[33mcommit 6748a5130c60431042b6a7b6cf5faee243c50313[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:49:16 2021 +0000

    Update audiohandler.cpp

[33mcommit dd443a56697a60677018b15b63466d65899423df[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:43:53 2021 +0000

    Update audiohandler.cpp

[33mcommit a3f40f51bebca3d03cdfb6a7c3096a7960a2d538[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:34:52 2021 +0000

    Update audiohandler.cpp

[33mcommit 1e8337e318c83b2ec0263f1b8648fe2336c80264[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:31:34 2021 +0000

    Update audiohandler.cpp

[33mcommit ef1f4f1e974b61cef73db4b65ae175c90b57919c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:29:39 2021 +0000

    Update audiohandler.cpp

[33mcommit 296bf2aab7a5a28938d3dc8d84dadd0486e7bbfc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:26:59 2021 +0000

    Update audiohandler.cpp

[33mcommit 17a4ef07cd25f1df06e7ddc5b65874056e312b9b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:24:23 2021 +0000

    Update audiohandler.cpp

[33mcommit b9b1a9964add6e79fe72be011f413ff075f53ed2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:21:59 2021 +0000

    Update audiohandler.cpp

[33mcommit ed729f74c65f4251c5031bec69c1c65732fd3461[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:19:40 2021 +0000

    Update audiohandler.cpp

[33mcommit 3059c5ccf55577bf7e275a2107672edd672c99c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 13:13:27 2021 +0000

    Update audiohandler.cpp

[33mcommit d4c1a47a547ae1dc3795be36ca5f822a0031c133[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 12:52:21 2021 +0000

    another fix for linux txaudio

[33mcommit 9454759d9f7a2a46cad64b81a5dd9b48f0fcd5d6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 12:38:12 2021 +0000

    Update audiohandler.cpp

[33mcommit 12a45032f995d4caee71ba268f12ad88435297d6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 12:13:39 2021 +0000

    Update audiohandler.cpp

[33mcommit 1998e2898d3b225496fdd5fda75ded31c7e4b067[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 11:12:41 2021 +0000

    Update audiohandler.cpp

[33mcommit 36e34fa73cacbd2a38ddd21749fd90ba1d3546d2[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 11:05:32 2021 +0000

    Update audiohandler.cpp

[33mcommit e0c56ae447b5d7103536788d551dee8d0222f3ae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 11:04:08 2021 +0000

    Update audiohandler.cpp

[33mcommit 4a10c34f9088d44a6c49ff23dd895bf7589a9f26[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 11:00:44 2021 +0000

    Allow for the fact that Linux can send random length tx audio.

[33mcommit 00fa92aea3022b3dbf8aeee872eb79aac5ce9cd8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 10:13:59 2021 +0000

    another audio handler fix

[33mcommit de0f13c49a585f11633c8bd9cb148db63f0683a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 10:11:27 2021 +0000

    Hopefully fix linux issue with new tx audio

[33mcommit 1fae53f140b5f77515573e3cb324c58c696fd172[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 10:04:36 2021 +0000

    More TX audio fixes and fix latency slider error

[33mcommit f0809fbe1a52e53d4327faf7508904a5f96a904b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 09:43:22 2021 +0000

    Fixes to txaudio

[33mcommit 48b2f8fd944181cdb001a728f00e9da45c1d4bd7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 09:37:55 2021 +0000

    Change tx latency

[33mcommit e593e5e90a5bd602c44b443b2c00f4b5dcd39f0c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 09:34:56 2021 +0000

    Change TX audio to use timed buffer.

[33mcommit 8dcda65d1c87650a01d3a8eb645e483c61c8e0d8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 27 00:05:06 2021 -0800

    More changes for Edge 4 with regards to updated firmware.

[33mcommit 0b22ef27ab87729d7baa0224c6bdca5ed8d5613f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 20:12:17 2021 -0800

    Added support for 4 fixed scope edges. Untested.

[33mcommit c1dc6d3eb7435ed9c37edf9e76f4efea1cc328d2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 17:32:06 2021 -0800

    Fixed issue with tuning steps and scrolling. Note: there are two
    modifiers for the scrolling steps.
    
    By default, scrolling on the WF or plot produces 100 Hz steps. Hold down
    shift for 10 Hz steps, or hold down control for 1 khz steps. This works
    well for me tuning in stations I have clicked on but perhaps not clicked
    accurately.

[33mcommit f2f37db7bf446f8acf882a2f6b3baf53d0798843[m
Merge: d23ff35 5a54744
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 17:12:51 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit d23ff352ca1a41c68b2ba2379181e3a21f97ae2d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 17:11:41 2021 -0800

    Increased average and peak integration windows to sort of match DIN IEC
    60268-10 Type I.

[33mcommit 5a547440b375fee6c6df1699a35aa3ee273c5d5f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 00:49:33 2021 +0000

    Remove unnecessary debugging message!

[33mcommit 27eb855adb6b72ca2f7f15f71b567c2a4226e759[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 27 00:37:00 2021 +0000

    Create rx audio packets with timestamp
    
    Lots of other changes but if this works OK, I will update tx audio to use the same system.

[33mcommit 6d8d1df45e63bb3b45fe1b88f88a8d98b387c50a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 16:53:11 2021 +0000

    Mitigate potential empty buffer crash.

[33mcommit 01fd2f420d512413fee584378665d871abf79f86[m
Merge: bfc3bbf 72be94f
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 16:52:12 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit bfc3bbf25ba50cfd2e45c267feed48a51477c25a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 10:29:09 2021 +0000

    Update audiohandler.cpp

[33mcommit a3470c836ac57e650956c1769ba0704379d8fad1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 10:22:30 2021 +0000

    Update audiohandler.cpp

[33mcommit 4d9ba2af056ad48dfa9580cdc9f41cf0f1478508[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 10:20:55 2021 +0000

    Update audiohandler.cpp

[33mcommit aa2caae3b07cde3f3cbbbe7b9861e223d8cdc5ef[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 10:19:00 2021 +0000

    Update audiohandler.cpp

[33mcommit 8a5c912f47e7f354617ab4b4cccf277a39b69805[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 09:49:31 2021 +0000

    re-enable audio output buffer

[33mcommit f54e7e646a1a64b6981d1dfb42de1ef26e8dea52[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 26 09:44:01 2021 +0000

    Possible fix for Linux audio

[33mcommit 72be94f9cfd096a0a105bda25dfdbcb216a42e96[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 26 10:26:04 2021 +0100

    changelog

[33mcommit 5286b5f4129a97b033efd7b5187f40ac7e60386d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 00:24:02 2021 -0800

    Fixed minor bug where flock did not stop double-clicking on the
    spectrum.

[33mcommit 6b67c0ab2bb5a14d1270a151072dec99aaea5f85[m
Merge: 1a3a18c 93ef269
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 00:13:52 2021 -0800

    2-day old code merge: remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 1a3a18cf34f954a8e68de670e0f938e16703045e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 26 00:01:23 2021 -0800

    Turned up the speed. Once you see fast meters, there's no going back.
    Tested at 5ms without any issues, comitting at 10ms for now. Note that
    50ms in the first 'fixed' meter code is the same as 25ms now due to how
    the command queue is structured. So 10ms is only a bit faster than
    before.

[33mcommit af2191da165e447cfa421f769bf7e51b3d2cde43[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 25 23:57:37 2021 -0800

    Cleaned up meter code, we can now query any meter we wish very easily,
    either queued or directly.

[33mcommit b14c8392c7a46048feba0120f2a92423a9d9010f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 25 22:51:43 2021 -0800

    Fixed meter polling issue.

[33mcommit 93ef26995e2b44c8e315a6739bd3a4625f54c868[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 25 18:51:51 2021 +0000

    Fix audiohandler compile warning

[33mcommit 46789342df4992a6aefe23dbd4835a55d6ef4715[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 25 18:05:01 2021 +0000

    Reduce TX period to 10ms

[33mcommit ea32c7f0971c67e2b0c91fb5408e49a716525a98[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 25 17:53:01 2021 +0000

    Various fixes and add watchdog in case of loss of civ or audio.

[33mcommit 8eeeca07af586eed948e170c3ab8eb7651051aba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 25 17:49:48 2021 +0000

    Move uLaw encoding to a lookup table to match decoding

[33mcommit b1d174b0a17264f25004d86024f6b6ec1df74efd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 25 11:13:18 2021 +0000

    Send "are you there" packets on a timer until "I am here is received"

[33mcommit 44678a6f34282c68d4f9285454ed55ccf2d5aa7f[m
Merge: 6f6145f 707aba7
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 22:58:38 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 6f6145f726aa01f6a6de5bc034fc1977cf03ec07[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 22:56:40 2021 +0000

    Many changes to mitigate network instability

[33mcommit 30b9c72208f78265b50c772031f4865e99fc6aa1[m
Merge: 0e745b1 83eaffe
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 22:56:01 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 0e745b1ee5f5fd47d650df4002b10041e1f2fdc5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 22:55:29 2021 +0000

    Fix stylesheet in Windows

[33mcommit 707aba708ecf498363a51ad9a5a1be1f2b3b8cf2[m
Merge: 4c136dc 6816ac7
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 24 09:39:43 2021 -0800

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit 4c136dca3a8d3d041945f6c41f70d703da29f819[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 24 09:38:34 2021 -0800

    meter fix

[33mcommit 83eaffe3921c426349179e6a034f0e5e6ac9fafd[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 24 17:33:36 2021 +0100

    changelog

[33mcommit 6816ac789cb1c54018a7c6115c27179a7b4766eb[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 24 17:30:53 2021 +0100

    removed useless 9700 log

[33mcommit f98471991e3bced4c1b086304c30ea30bdd2be14[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 24 17:30:11 2021 +0100

    changelog

[33mcommit 7d1e2026639efeb631d958e4a8da01b2ce7f1b9b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 24 07:56:12 2021 -0800

    signed vs unsigned and meter math

[33mcommit ffc62213cd0f9061d292172e463d7614e39960c7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 11:00:23 2021 +0000

    Fix lockup to 785x

[33mcommit 098d3b891558fc2497af41bb2280edf0557843f1[m
Merge: 94d2cbb 90d4933
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 08:41:04 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 94d2cbbbc7e2c1797f49aa50a19cf127a65f1238[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 24 08:37:58 2021 +0000

    Fix logging compile error

[33mcommit 90d4933d02fc4fdb7726675baebb5515c6e3685d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 24 00:29:01 2021 -0800

    changed size of meter for less eye bleach.

[33mcommit 29d3893bb0153f3ed7c3b3de8c3c62330ff74aab[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 24 00:18:19 2021 -0800

    Added a nice s-meter. It isn't right yet, but it's a start.
    TODO: Change scale for power meter, make sure full-scale fits
    TODO: create static image from scale to overlay each time rather than
    drawing each time.

[33mcommit ef7980108147e9d02397c1ddc8c082fa2c4b9669[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 23 21:21:22 2021 +0000

    Add logging categories

[33mcommit 05c54ed3491aab7688e158db475adaf0fb1da361[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 23 20:49:26 2021 +0000

    Change UDP Server to use new packet format

[33mcommit 80148a84bf8ee05b37770fc83171d4275a621594[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 23 20:48:53 2021 +0000

    More fixes to retransmit logic

[33mcommit 84d558c08c04de67e0ff1f00ce023e4e1b862eec[m
Merge: ae3005b cecc7e4
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 22:25:43 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit ae3005b8f8f995fc748bf81ef3d279eb4b406561[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 22:25:09 2021 +0000

    Changes to retransmit code

[33mcommit 6c54421f168c30553959f2b5b23ee4ee017ff6b0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 11:11:08 2021 +0000

    Fix small Windows compile warnings

[33mcommit cecc7e470322d32c19116fc76082943d549f800a[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Feb 22 11:29:33 2021 +0100

    f-lock in changelog

[33mcommit 7112d23f5b0171c5cd6f9fc6b36b743c59de166c[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 10:25:19 2021 +0000

    Change server user types available.

[33mcommit edf85885f15c0bc211ba1a0de3e886a5a828b674[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 10:24:29 2021 +0000

    Change precision of some number literals

[33mcommit 5da631ab0a3055b8c7e1764714bbc0185b4d1565[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 21 23:58:23 2021 -0800

    Frequency lock works now.

[33mcommit 24d802597ad36ca4c4dcc9277b94f30a408afe14[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 21 22:13:21 2021 -0800

    Removed more unneeded debug and three unused functions. Just cleaning
    house.

[33mcommit c4113aea6feb0011ea5ba0ed41ce5f8427b49f67[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 21 21:54:25 2021 -0800

    Cleaned out some qDebug() that wasn't needed anymore. Output is now very
    quiet on "release" type builds.

[33mcommit 3796b00334851cc52485e4014fb8986e89e2eb00[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 22 00:24:48 2021 +0000

    Add usertype selection for Server

[33mcommit efd97ba1433190c213481d59bada53bb1925a49f[m
Merge: 3dd1f75 9fa2f52
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 21:06:17 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 3dd1f753fe42cd1439db4860e95f267605b35bce[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 21:05:58 2021 +0000

    More packet type changes

[33mcommit 7d5a0661087c09a683ba62bff9a8570f4063fd3a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 15:06:27 2021 +0000

    Small fix when requesting stream

[33mcommit ba0509ba61fa26a83ebabc280c5aa878929b14fb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 14:53:42 2021 +0000

    Fix txaudio and move udphandler into its own thread

[33mcommit 9fa2f5236964476570b34cdcd1eb985afd3d5976[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sun Feb 21 13:59:49 2021 +0100

    modified changelog

[33mcommit 4cb32896029b64040adca3ab8ad5140ca442ceeb[m
Merge: 4e6d4d5 549bf34
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 09:00:41 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 549bf3463e91d702ea91174b7480c53cbe533897[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 20 22:41:05 2021 -0800

    Changed modulation select combo box to auto-resize as items are added.

[33mcommit c3e0fa7dbd80855ff05e71add5e0e878b604452d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 20 20:41:19 2021 -0800

    S-Meter working! Power meter working!
    
    Needs more testing, but it's here! 100ms polling time.

[33mcommit 4e6d4d5faacad445af1cfd82191ccc102280d01d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 21 01:18:14 2021 +0000

    Some more packet type fixes and make it not disconnect other users from the radio.

[33mcommit 938a9f1d1b53427638d1426e2f924a7372e883d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 20:27:35 2021 +0000

    Add another missing break

[33mcommit 86d0443fac3d11d8d7a408f6bc0c20ec06de3ff8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 20:23:21 2021 +0000

    add break to case statement

[33mcommit 258b56fc580a47b5960a2a6ba31d751d29d3facb[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 20:19:18 2021 +0000

    Fix tx audio packet

[33mcommit 6e53e1e683751a5b51706d01da26d81edae4fff5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 18:30:46 2021 +0000

    Add visual studio 2019 solution

[33mcommit e08696665193777b0c8265cfe9869d6718b83df3[m
Merge: df2c765 7f71759
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 18:29:50 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit df2c76588d0c8d2c29b22b534fc6c601a56f484b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 20 18:29:23 2021 +0000

    Change the way that packets are handled
    
    Packets can now be cast to a union of structs that allows each part to be accessed individually. Still a work in progress!

[33mcommit 7f71759780f488b87195885e389431643457d94b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Sat Feb 20 13:42:57 2021 +0100

    until a more elegant idea comes up, changed the found rig message timeout from 1500 --> 0 so that the rigtype stays in view (testing multiple rigs, same time)

[33mcommit a9d430b271de4fdb1a44796b26da281d404bd769[m
Merge: d7c01f9 69db5e0
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 20 00:23:58 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit d7c01f95600fbe7b17eeb43292fa5f98e534b2a3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 20 00:15:47 2021 -0800

    Data checkbox now changes input source label and updates level of slider
    to last-known value for given MOD source.

[33mcommit 9793b9e1cba3bb1b648f711000414996ae899b0d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 20 00:05:23 2021 -0800

    Audio input gain slider now adjusts whatever the active MOD source is.
    Text follows drop-down menu selection.

[33mcommit 69db5e0efc462f87d7e5f32f9f9abd27d2d531a9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 19 09:32:30 2021 +0000

    Another fix for the server crash.

[33mcommit b9c49218024c91cb8e3c8f207284ec3263bca468[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 19 09:28:03 2021 +0000

    Hopefully fix crash on startup.

[33mcommit 3538e545d986cae5a6d47f8f22a3f280aef8e9c9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 19 00:14:40 2021 -0800

    We can now set the modulation input and read the modulation input for
    both data on and data off.
    
    Next step is to change the "Mic" slider to adjust whatever input is
    selected.

[33mcommit 224e73ae3d3bb0c29de552c7ae83733eed0ab1a2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 18 22:38:50 2021 -0800

    Added "Use System Theme" check box code. You can now toggle between the
    built-in dark stylesheet and the system theme, and can do so
    independently of the waterfall theme. Thus, one can enjoy using other
    tools to set the widget theme.

[33mcommit 98d9c470c5978ba96da5836f122a022a0bea4842[m
Merge: 3b828c9 654132d
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 18 21:48:09 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 3b828c9c47f4b00167fd77b44b5279920b52a0f8[m
Merge: 5c50b82 ba9d334
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 18 21:47:53 2021 -0800

    Merge remote-tracking branch 'refs/remotes/origin/ui-enhance' into ui-enhance

[33mcommit 5c50b82dbae619f8e6c3a28effe68372850d8a1d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 18 21:47:19 2021 -0800

    Duplex controls now function. NB: "Auto" mode works as follows: User
    selects "auto", rig does nothing, user changes frequency or band, and
    then, the rig will automatically select the correct duplex mode. This is
    the same way the rig works from the front panel.

[33mcommit 654132d2fb17b34dc20fdd13fcebc117685a366e[m
Merge: af31459 ba9d334
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 23:36:19 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit af314599e8c15c2377677a637a628f8d0abd2a0f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 19:12:27 2021 +0000

    Fix ping sending that I broke!

[33mcommit 8c50e9486ce30feca443a3a6ce9317fbb5ac0c74[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 18:47:47 2021 +0000

    Fix server compile warning

[33mcommit ba9d3340a589a8bc49b7840714d34a574d80c881[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Feb 18 19:27:35 2021 +0100

    added features to CHANGELOG

[33mcommit b7164d762ca0af8c9f2341e2930d08cdd86304ea[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 17:44:11 2021 +0000

    Fix for endian-ness of packets

[33mcommit 61d06213adf0336c29d160fed3168b120b7f1817[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 15:54:26 2021 +0000

    Create all UDP packets using a less error-prone method

[33mcommit 0a0eb031c621680f76c5e92a792ab0a916fab714[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 14:15:03 2021 +0000

    More fixes to server connection management

[33mcommit cf1ee895cf497717bea1687df6a73d34eef59df1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 13:49:19 2021 +0000

    Fix error when deleting server connections
    
    I still think that maybe this should be done better?

[33mcommit ba60181c69a2f8b4e74536bce608a2e9cd03ba04[m
Merge: 7966568 74c6d65
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 09:15:01 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 7966568f17f7a962d92f9c4189a3a8ad3ce72228[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 18 09:14:41 2021 +0000

    Connect Civ to/from server

[33mcommit 74c6d65e1deb8aef67b2fb9bf46b155def950d03[m
Merge: 6a3260b e3b3d2a
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 23:49:49 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit 6a3260bb8afdc736ef0545ace34021c1f23b5e22[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 23:47:34 2021 -0800

    Added void slot code to receive USB/LAN/ACC gain, Mod input selection,
    and duplex mode. Will connect up to UI elements later.

[33mcommit 3c93dc5fd0e2ec42af2fc5fc3ca3285af78d6c41[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 23:41:01 2021 -0800

    Added code to get and set split and duplex mode. Untested.

[33mcommit ce2f20753c98ac81e36a3393e781bd63b3a181d3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 23:26:10 2021 -0800

    Completed code to set and get modulation levels for ACC, USB, and LAN
    sources, as well as source selection/verification.

[33mcommit e3b3d2a3f4981adba7f46cc1a365b187bd0025c3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 17 23:10:26 2021 +0000

    server now accepting civ/audio connection.

[33mcommit 83a8f76e587c377ee659a972f74966c9d08626ee[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 00:14:30 2021 -0800

    Tweaking to udp server UI. Not finished yet.

[33mcommit c32cae03e1eec1f056ecfaa82150215ac8ca0ee1[m
Merge: a229fc9 20db57c
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 00:01:05 2021 -0800

    Merge branch 'ui-enhance' of gitlab.com:eliggett/wfview into ui-enhance

[33mcommit a229fc9ccb20cd8702a20d5036cf6b8571214123[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 17 00:00:28 2021 -0800

    Contains code to set modulation input (mic, acc, usb, lan), and to set
    the gain of each type. Is not tested.

[33mcommit d03d781713c6f28d16802f719a3b3446b9e362da[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Feb 16 22:32:05 2021 -0800

    Fixed busted keystroke assignments (mode and transmit).

[33mcommit 6b99d122a29733518cc90f7e028b8fe019cc526d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 16 20:57:48 2021 +0000

    Fix udpserver compile warnings

[33mcommit e3a3844f3edc8f9df6a8032bce4ea4cf6b8505da[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 16 20:55:30 2021 +0000

    Few fixes to server setup ui.

[33mcommit 4b6b73894b31228406be265f2bf1b7885a579984[m
Merge: b52629f 20db57c
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 16 20:26:26 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit b52629f72b7c9ea6df8ac00e8bc69b6d3795e3c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 16 16:42:56 2021 +0000

    Fix server user saving error.

[33mcommit 95028374b43fd106355c8009a641e39a805adbdd[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 16 16:16:46 2021 +0000

    First commit of server code
    
    Doesn't actually do anything at the moment other than allow clients to connect and login.

[33mcommit 20db57c5446b172ad36c1b50a06106976826f658[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Tue Feb 16 11:30:03 2021 +0100

    updated udpserver ui to have 10 instead of 2 user entries; also changed the scroll bar

[33mcommit 88d31063f903849beee8746845e9a05916d3b0c3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 23:55:59 2021 -0800

    Changed repeater button names

[33mcommit 2c9e46626ebfd68ab272e9935c801b9504f24139[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 23:52:14 2021 -0800

    Preliminary changes to UI for repeater duplex. Staged change in
    rigcommander.

[33mcommit 11fd572d212df521ca16d6797c2f7d996567f2b2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 23:34:19 2021 -0800

    Fixed switch case unintended drop-through

[33mcommit c67e4a2390fb1798d17054e7646e31eccdd3643a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 22:50:08 2021 -0800

    Minor UI cleanup

[33mcommit a421dda2649544162a9c5ad33355efee0f88a9fb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 22:46:26 2021 -0800

    Locking out satellite and reference adjust windows for rigs other than
    IC-9700.

[33mcommit 8460b09ce3016533cba45d9b088bdee0515ce94c[m
Merge: a95a957 87f41cc
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 22:40:20 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit a95a957b5912055da5de269a97815c4cfa55c1c0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 22:37:35 2021 -0800

    wfview now checks the current spectrum reference level on startup.

[33mcommit ca2a8e1235e9e2d94d58e2e41b10f44c27cc9fde[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 22:33:21 2021 -0800

    Added code to check the scope reference level. Will add a on-startup
    check next.

[33mcommit ac1c62ef435bd227e4924883e76575dbf8b0f408[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 21:48:19 2021 -0800

    Fixed bug where power and mic only went to 37% (this is value 99 instead
    of 255).

[33mcommit a0e93926173eebc52b07be9909eb10d1e633ec29[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 21:40:31 2021 -0800

    Added scope reference setting. Write-only for now. UI slider works.

[33mcommit 6938ed8a60b1dfbd28ae42481bf0584e08e72eaa[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 20:32:14 2021 -0800

    Full support for set and read mic gain and tx power. Code also exists to
    set and read microphone compressor level, vox gain, anti vox, and self
    audio monitor.

[33mcommit 91ce7f16d00025c0e8eb5dff059a83201e5aa56e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 17:15:28 2021 -0800

    Added more support for levels (TX power and Mic Gain). Does not change
    levels, just displays on debug button.

[33mcommit 7d0b926958903e767ad19ff7d278f0ae48a4949d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 15 15:17:48 2021 -0800

    Added request and parse functions for most level and metering data.

[33mcommit 87f41cc20adecbdc0ef7e9db92fcf6b6b0e4d388[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 15 19:28:17 2021 +0000

    Fix mixed endianness and change how password is encoded

[33mcommit 529b70b1158ddbb6e84325588ea87b1addb03601[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 23:58:50 2021 -0800

    Minor UI element changes.

[33mcommit 187b42cc349984cfb030543ae56a017360f5a918[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 23:24:21 2021 -0800

    Minor changes to calibration window

[33mcommit de09bac4c47831c20c9ccdffca41479501ff2e37[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 22:59:44 2021 -0800

    Fixed issue decoding reference adjust values.

[33mcommit 1f133344ba4322fca0aecd0352c958c8ba914426[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 22:40:04 2021 -0800

    Fixed reference issue where the slider and spinbox didn't track
    correctly. Re-arranged buttons for this dialog.

[33mcommit fdaadc0d2afd2840211a1002a49faab554b50758[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 20:51:18 2021 -0800

    Added issueDelayedCommand and issueDelayedCommandPriority functions.
    These are the methods by which we should issue queued commands to the
    radio. It is no longer necessary to re-start the timer when using these
    methods. These methods might get more advanced later, and this structure
    will allow for more growth.

[33mcommit 32f407020bb38eef5427c447763699ba33180969[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 20:45:48 2021 -0800

    Changed front panel UI to contain more of the core features. PTT button
    was interesting, now has function of Transmit on click, but if already
    transmitting, sets to receive.

[33mcommit 73218c8a3325ea6ae5b53e49189c0ea6a74cce4c[m
Merge: e1dd3e9 dce5d93
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 17:50:40 2021 -0800

    Merging contents from lan-alpha into ui-enhance
    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit dce5d93a6c91e67aa639f03d5f0718182414a8c3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 18:32:58 2021 +0000

    Starting to create udp server

[33mcommit e1dd3e9ca09f1484f74d3dfac3c9f3413642473b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 14 10:19:13 2021 -0800

    Added numerical input validator to network port UI text fields.

[33mcommit 4d4a688733f35e46c47b1b95e9d8bfa8edd2868b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 16:23:25 2021 +0000

    Change token message

[33mcommit c37a4e4206637068bc569a10fce3e505906b45e3[m
Merge: 192bcb0 66e02a0
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 16:20:15 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit 192bcb0741bba58d30d15f130cf85929f6f415b8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 16:14:56 2021 +0000

    Fix packet loss counter

[33mcommit 3de45a8f2cad9a7c780862b4d76fecd5b1d94997[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 15:30:34 2021 +0000

    Some code tidying and get rid of some Windows compile warnings

[33mcommit f62077eb2847b000d62fbadda7baf5fd5605b7ef[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 10:40:47 2021 +0000

    Add packet loss and remove retransmission warnings

[33mcommit 921bf2025b4c8ff25d76912a0ca2814fd90c845a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 07:53:55 2021 +0000

    Add connection speed detection and connection timeout

[33mcommit 66e02a045ace6b5493ed58a669919d50ad93c57d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 13 23:42:51 2021 -0800

    Filters are now read correctly from mode messages. Filter setup menu is
    still a placeholder.

[33mcommit 67d6d06a9ac4a26c8db20bac9456c6eab603cfba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 14 07:15:49 2021 +0000

    Try to fix token renewal failure

[33mcommit 7acbd378aa58a72c5ec908aa674537a243ed2b86[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 23:28:28 2021 +0000

    Update udphandler.h

[33mcommit d9b280cdcf5e8a7b460f688548755fa7337438d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 23:25:24 2021 +0000

    Complete rework of UdpHandler
    
    Only tested on Windows so far but should be more in-line with the native implementation

[33mcommit 5d3c386c874eb8eb455263f08a33375c7dbddd02[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 13 10:02:45 2021 -0800

    Added filter selection support.
    
    TODO: Read current filter from rig.

[33mcommit 27815a799472057a4879b5dcf3b2154316f017eb[m
Merge: cbc83e8 941c586
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 13 09:21:32 2021 -0800

    Merge remote-tracking branch 'origin/lan-alpha' into ui-enhance

[33mcommit cbc83e86b7628076ca990871c43bec73830edc60[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 13 09:20:38 2021 -0800

    Minor change to UI file for better spacing, more elements to come.

[33mcommit 941c58624328aa7b6073226ac579122be2041121[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 11:04:26 2021 +0000

    Yet more audio fixes

[33mcommit 47276bfaa3c07508d04b37a0279dc60827161f26[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 23:35:17 2021 -0800

    Fixed issue with DV and LSB modes.

[33mcommit be52cb49bb95c9e4120680d5bec551ad932e5d0a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 23:18:01 2021 -0800

    Fixed DD and DV mode selection.

[33mcommit a6e1d36feaa50fefc4c637e68272344c3afee8fe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 22:50:54 2021 -0800

    Comment cleanup

[33mcommit 951c2718ac98ffd31cbdfeb3e3ffcfc080dbd2e0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 22:49:42 2021 -0800

    Added data mode checkbox.

[33mcommit 9f2d64d943e9c774cac6d176b942f92aad9f1fc6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 22:42:34 2021 -0800

    Added per-rig customization to UI: Modes and ATU status.

[33mcommit 0f8c4b816e15bd252f618bd3b2b9fafec17841b1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 17:21:32 2021 -0800

    Changed spectrum to scale line plot to maximum amplitude of the rig.
    Prepared new window for satellite ops (not added to program yet).

[33mcommit 3ac1c004e36d62c27d2172d11e18da90bc9d56d1[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 00:45:59 2021 +0000

    More TX audio fixes...

[33mcommit 9cbd572d4a61dddc724d06c2b5f2a30b3fde9f20[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 00:05:38 2021 +0000

    Update audiohandler.cpp

[33mcommit 207fd5018e467aa4e9e06b1d2c737e52dbdd63fe[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 13 00:00:14 2021 +0000

    Update audiohandler.h

[33mcommit 7dc580ce9c81cccba8cb435eea3b22acc435b7ae[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 23:56:02 2021 +0000

    Another TX audio fix

[33mcommit f83f40bee832820785e13b6a25af26ff4a554f35[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 20:42:56 2021 +0000

    Test tx audio sending (again)

[33mcommit 50c638ed4b2cd733bc9e7bbf3f41cbbc882ba100[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 11:52:16 2021 -0800

    Added preliminary support for IC-R8600. Untested.

[33mcommit 5a55d5fd3d29d7f093d3038f8dc7f604c980fbb6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 14:46:02 2021 +0000

    Fix for remote connection IP detection

[33mcommit 62d0c1d681b009f131e0e2b7242db9bbfd40021d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 14:31:26 2021 +0000

    Update udphandler.cpp

[33mcommit dc151f526896ecfcf0a86d39b6cc304490e07893[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 14:28:55 2021 +0000

    Always use 16bit samples internally

[33mcommit 518b70c0e6328e856c9edea1be269be7c666100e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:59:33 2021 +0000

    Update audiohandler.cpp

[33mcommit cabc7cd16ad67ac0211e290ce2872a31d259a22e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:58:37 2021 +0000

    Fix linux compile warnings

[33mcommit 06105c0e6b97e48b01226c16affe082195697c16[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:53:35 2021 +0000

    Hopefully now fixed linux TX!

[33mcommit 3ea7a9b8ed112aa17b23d03778439dd9744a843b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:44:10 2021 +0000

    Update udphandler.cpp

[33mcommit d0462e06a51bd307e5e9da9cdfcd04921cca61d6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:42:42 2021 +0000

    Update audiohandler.cpp

[33mcommit 66f35dbe7fb27ffc5404769f1380085f98ca9a0a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:26:32 2021 +0000

    Update udphandler.cpp

[33mcommit 1b0160b18ceedd29c646f4d0c05d39b9cef62d91[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:24:45 2021 +0000

    Update udphandler.cpp

[33mcommit 50731d45c7970a46b5a079c5a081495aeac40468[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:23:00 2021 +0000

    More txaudio fixes

[33mcommit 955963042ea97e9e98b72d821d35a9ead086ab85[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:11:21 2021 +0000

    Update audiohandler.cpp

[33mcommit 98a3573ad3a1d79a6e50ac587918f0a1d9065878[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:08:50 2021 +0000

    Minor change

[33mcommit 9f27c3acb2fb34db1975273613219d8b56fdcb93[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 12:02:25 2021 +0000

    Maybe fix TX audio on linux?

[33mcommit be6c55bfde270212f9429033bdbaf046a71298a4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 11:04:42 2021 +0000

    Delete duplicate rxaudio signal

[33mcommit 00a55ef4b05e988fe017e00313ca88a45b5e6072[m
Merge: 9648211 55983bf
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 11:03:08 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 96482117390a7197b31f7f03b1b3419bb381824d[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 12 09:20:15 2021 +0000

    Add a bit more debugging for TX audio

[33mcommit 3c556e326539e712d654aeec2203eed0e936b236[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 12 07:32:14 2021 +0000

    Update INSTALL.md

[33mcommit 55983bfa669533620b5da810be0cb64f785fd823[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 11 18:49:56 2021 -0800

    Found two connections that were the same, commented out one of the two
    RxAudio setup connections.

[33mcommit ce5c6e3c7fa7dde9fa73f4ea21e84a7935238db3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 11 18:44:47 2021 -0800

    Minor change from setupAudio to setupRxAudio within the udpAudio signal
    slot connection.

[33mcommit c288a7f45b014383d6638792e2d2a9a99a946305[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 11 22:59:39 2021 +0000

    Remove ui_wfmain.h

[33mcommit 4b3005ba8cc1c5509af1eb1e78e214e61e96f2d4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 11 20:22:40 2021 +0000

    Update wfview.pro

[33mcommit a9971a1c4e46da90e61a9340f2d0eb83f9c52a64[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 11 19:55:51 2021 +0000

    Make uLawEncode static

[33mcommit 20e681dc916d762c793af05cd9574cfdd557aaf6[m
Merge: b7f8ad1 8dfc83a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 11 19:47:29 2021 +0000

    Merge remote-tracking branch 'origin/ui-enhance' into lan-alpha

[33mcommit b7f8ad1deea06fb242e9ab423d0c77834093b674[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 11 19:18:35 2021 +0000

    Add initial TX audio support

[33mcommit 8dfc83a3c99ebaecf2b81dbfc674ed7fbe6cb665[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 11 09:21:17 2021 -0800

    Added filter select combo box. Does not actually do anythig yet.

[33mcommit 37f242ea3970c9d3959644f1c2be68dc1e9b1ec5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 11 09:09:45 2021 -0800

    Added DD and DV modes for IC-705 and IC-9700. Untested.

[33mcommit 69114e261905b534d5fa366cf1c0bd8ff36fb4b9[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Feb 11 10:09:44 2021 +0100

    added items to the mode menu TODO-section

[33mcommit 13556f12d23d4f0ebe55c766e67de947e88ded38[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Feb 11 09:57:14 2021 +0100

    added newline at the start of help string as well

[33mcommit 9f9b032cf8b6b4e81005ff2b6df763eca72986af[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Thu Feb 11 09:51:40 2021 +0100

    added newline at the help string

[33mcommit 7b32bc97e645aad910fca6a75a3d90b4f56b38aa[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 11 00:33:25 2021 -0800

    Calibration UI has basic functions. Reference set/get methods are good.
    Still need to connect sliders to spinboxes so that the user can sense
    the value change.

[33mcommit fa504141388177fc0e44f668225522a672eb3023[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 10 21:22:38 2021 -0800

    Added UI elements with some supporting code: squelch and calibration
    window.

[33mcommit 7021cd935daa307ff8b35ee81fc9ca0845f4fe19[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 21:33:20 2021 +0100

    added info about working on a raspi

[33mcommit 700ce46b1ccabe325d747f9f22ebe3191b50df5b[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 21:19:25 2021 +0100

    added changelog

[33mcommit 615e193ea690d2f729b121e36cccaa7e82175c19[m
Merge: d7967e0 53fd008
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 17:32:56 2021 +0000

    Merge branch 'lan-alpha' into 'master'
    
    Lan alpha mergin into master
    
    See merge request eliggett/wfview!1

[33mcommit 53fd008f3ab16ff51c678e38b271adb151d9ee2e[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 17:32:56 2021 +0000

    Added support for various spectrum sizes and rig capability support.
    wfview has been tested with the following Icom radios over USB port:
    IC-7300, IC-7610, IC-7851 (and IC-7850), and IC-9700. It likely works
    fine with the IC-705 as well. At this time, the rig's CIV address must
    be changed in the preference file to indicate the rig you are using, and
    this must be in integer, not hex.

[33mcommit d7967e035613134c3e93f30c839c6da8a0fceb95[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 12:39:30 2021 +0000

    added logout/login after adding to dialout group

[33mcommit d0410e1b694f8045719b78523135824459043296[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 10 10:40:22 2021 +0000

    added command to get user in dialout group.

[33mcommit b8fe90ebc09ca25250783bc79272b18f9d200ad9[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 9 13:57:03 2021 +0000

    Fix location of received devname.

[33mcommit 5ec57cbc1bc26be94ecb9894348e1d9fb4948fe5[m
Merge: e6dee05 0b5559a
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 9 12:43:49 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit e6dee05ab15da8285fab8c2230876d2839b147a3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Tue Feb 9 12:43:28 2021 +0000

    Add support for codec/bitrate selection

[33mcommit 0b5559ac620810d1c1a02654c57a48910ed661af[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 8 23:31:44 2021 -0800

    Minor UI update: replaced two buttons with single checkbox.

[33mcommit a6ccec78a834e67b35a3f90965b1378a0fc7e3d0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 17:22:29 2021 +0000

    Change available sample rates.

[33mcommit 228fb07eed4aa0af54162b023bb2e3f08adcdf7b[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 16:53:26 2021 +0000

    Add audio buffersize slider and ability to select samplerate and number of channels.

[33mcommit b6ad6218e104a97efafe743872c3e78df53888cc[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 12:38:30 2021 +0000

    Fix for audio latency?

[33mcommit 9078ee79bb41858fc1337c93b5e693ccafbab311[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 11:14:17 2021 +0000

    Start the audio thread!

[33mcommit 563b7feb1c860338d0bcd6afe5ed9eeb00899f3f[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 10:23:23 2021 +0000

    Remove IP address from inuse detection

[33mcommit f1817874b5407de73829e44f59d825665ecd3e99[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Mon Feb 8 10:22:20 2021 +0000

    Fix audio thread crash on exit

[33mcommit c341efc11a4724f3c083e06d97f0c24f3a283cfd[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Feb 8 10:20:06 2021 +0100

    adding missing dash to the short form of --logfile

[33mcommit 9c88daa3c87c2bbc8986cc890fc53fd2ef264a77[m
Merge: 5acdc23 6fee18d
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Mon Feb 8 10:19:30 2021 +0100

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit 6fee18df9a177f33b99ef2bdcf9fe040df699056[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 8 00:31:48 2021 -0800

    Moved audio to separate thread.

[33mcommit 89fe648868f42d1acd547cf4578215de06de46e3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 20:07:26 2021 +0000

    Add logging to file.

[33mcommit 84f10fe238a32e16e507c87b94629b94f9091245[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 19:09:19 2021 +0000

    Ignore non-IPv4 addresses

[33mcommit 33821e0515426fc7deb032d669caaffe5228b695[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 18:46:47 2021 +0000

    Allow hostname or ip address for radio.

[33mcommit 3fe60deceab58dbe8caa00c450f85ce845265aa7[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 18:13:22 2021 +0000

    Add checking that IP address matches

[33mcommit 136c176cbb0fd2c4cdb5aa6361bf3de3eedff41a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 17:40:38 2021 +0000

    Don't connect if radio is in use

[33mcommit fdda23c21fb9eea2d9e26b4b03279223b58eb120[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 12:59:41 2021 +0000

    Allow connect/disconnect
    
    Not tested on serial connection!

[33mcommit 2cfea3e52c9588fcbddf42629a3ba5ec3a8358a0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 09:59:04 2021 +0000

    Delete file that got accidently committed!

[33mcommit cdb3d42d7e8175fd698fc091ed4e5ddca1645cde[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 09:56:11 2021 +0000

    Quick test, using Debug button as connect/disconnect

[33mcommit ab6192428f296b4bef8c4d2439dd719858415cb8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 09:50:44 2021 +0000

    Fix crash on exit and cleanly quit thread

[33mcommit 46b53f0f68d65efc50a7531f99dba53c77530e2d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 7 01:03:02 2021 -0800

    rigcommander now emits a signal once serial or network is instantiated.

[33mcommit 9dcfc3be70aa1a01e01053f2ea6d4a1f087e58bf[m
Merge: ad37495 8b6cc02
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 6 23:48:02 2021 -0800

    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit ad374957bbf4f318f2d32608af9428b66f86f09f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 6 23:47:46 2021 -0800

    fixed minor typo with somewhat major impact.

[33mcommit 8b6cc02b9e1842e0051ae39e1d0f2e45dcf911e4[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sun Feb 7 07:34:06 2021 +0000

    Fix the fixed port fix!

[33mcommit 050c516e5870dbba64ef6069fbca70b033e916ff[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 6 23:14:15 2021 -0800

    Moved serial and network into the process() function, thereby creating
    these new objects within the new thread of rigcommander.

[33mcommit e7685147ba63e75c09759373ea0ad21e7ab58946[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 6 20:08:58 2021 +0000

    Remove hard-coded ports in sendRequestSerialAndAudio()

[33mcommit ee9517159a758f42f31eef8719995f3403731f62[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Sat Feb 6 10:54:20 2021 +0000

    Fix for pkt7 (ping) requests and improve rtt counter

[33mcommit 8c20e1d94400fbb8d7510f848e4b6a8c65eef0f1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 5 23:58:17 2021 -0800

    Added a feature that converts the current center-mode spectrum display
    to fixed-mode. Squashed a bug related to determining the fixed/center
    mode (we were looking out of sequence).

[33mcommit dca22a3e3753d92c537aafc0f17389fbcf63bec7[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 5 21:53:05 2021 -0800

    Fixed a bug where if wfview started and the rig was in "center" spectrum
    mode, the center checkbox would be checked, triggering setting the
    center width per the pop-down menu. The code now disables the checkbox
    signal method during the modification of the checkbox.

[33mcommit 465c4506e2e7c5551aeb55258e7a0151cb320e71[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 5 21:44:50 2021 -0800

    Cleanup in openRig(): No longer searches for serial ports if using LAN,
    and has fallback for windows serial device.

[33mcommit fd08073880158c4514c87a105c58dc6137a7d3fc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 5 21:26:22 2021 -0800

    Added a few pixels to the frequency text box, should fix issue on L-Band
    frequencies.

[33mcommit f2d32b143f91e2368b60925201cedf61d5780066[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 21:25:03 2021 +0000

    Rename latency to rtt

[33mcommit 5daf846602973b13be07ea35ba097e357d081767[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 21:23:00 2021 +0000

    Add network latency measurement

[33mcommit fdceb06399e0e77d8de6f66ee6648e25f77c0bff[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 20:28:02 2021 +0000

    Delete MOC_COMMHANDLER.ipch

[33mcommit 8dd8b3a088cd7458d3d410dc3fe272aeacdd5ac3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 20:26:18 2021 +0000

    Add widget to rhs of statusBar

[33mcommit 163af0be660a123d6754b4efdb3158b4e58170c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 17:40:58 2021 +0000

    Add audio destructor to delete buffer and fix lots of missing braces

[33mcommit 90f8cb91c17ad7f14ed2f8e7ee88d7cef33eee60[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 15:49:08 2021 +0000

    Another try for retransmit!

[33mcommit 415c4992387e5fa159363d6f502b9538435883c5[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 14:38:30 2021 +0000

    Remove unnecessary buffer debug message

[33mcommit fc69c9d15101b3c8fe6f4e7d72dfc52dc5efa38a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 14:33:07 2021 +0000

    Put wf data back!

[33mcommit f7945bffd4a09bcfa75db8a3e6086aed792223b0[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 14:30:50 2021 +0000

    Stop infinite growth of audio buffer

[33mcommit c159cbfa37c0d3514a6190f268178215b4c1387e[m
Merge: 1ab4ad9 d673b7e
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 13:17:00 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 1ab4ad94f983dfb334477fd8c21d1653f2a72de6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 13:16:48 2021 +0000

    Fixes to retransmit feature

[33mcommit d673b7e9523eedc4b8bccef18287a9c50d5d084f[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 13:29:50 2021 +0100

    fixed typo

[33mcommit a74ea8f2a3a8b1d35fc25326f468220d7ebcc7ca[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 13:28:11 2021 +0100

    in the TODO section added links to the wikiwq

[33mcommit 14b68a15bc44fd5d12ea9d71b00124b895219b60[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 13:20:37 2021 +0100

    added feature info: ethernet support

[33mcommit e8cab5b3a3475722f8e19346a441479977c6d922[m
Author: Roeland Jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 13:17:08 2021 +0100

    added 7610, 785x, 9700 as supported/working radios

[33mcommit 2aaef41130635032f12d92f2d87cc34b9fee7931[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 10:59:39 2021 +0000

    added wfview.code-workspace for vscode

[33mcommit aa913167efa6f080dc4099e23429dfbfaae4f5c4[m
Merge: 112706e c427d93
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 10:45:37 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 112706e2b24a9e38a76d27c07325e58cfdff8700[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Fri Feb 5 10:45:19 2021 +0000

    Add credit for Kappanhang

[33mcommit c427d930084b5f4aa3497eda70606cc91944014c[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Fri Feb 5 10:41:32 2021 +0000

    changed the audio->setBufferSize from 6000 to 10000 as 6000/8000 gave too many pops, clicks and other dropouts of audio

[33mcommit 88edba70bedf907f190d487fb40776b632ce2805[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 4 22:44:15 2021 -0800

    Added fixed/center data from spectrum waterfall data.

[33mcommit 1a1b7d06734ff8a1fd5aad5fd99a87f99f39e42e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 4 22:06:48 2021 -0800

    Adopted closing scheme for linux by default (may remove #ifdef soon).
    Changed default buffer size on receive audio to 6ksamples, which is
    0.125 seconds. Added debug info for audio.

[33mcommit 437489647a487fd00db8495270e77223ff76e24b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 4 21:03:16 2021 -0800

    Fixed bug with fullscreen checkbox not following F11. Also changed
    default to not-full-screen.

[33mcommit cece02069a41397c9c1630c9f73c90de54a4093f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 4 20:54:34 2021 -0800

    Added default values for network prefs. Connected checkbox to network
    text boxes.

[33mcommit 3791c68f5dde1d3ab8fc4dd60d4d4da88dcee01a[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 21:12:38 2021 +0000

    Lots of <static_cast> additions!

[33mcommit ea1537a006248358f63ad55df6c549d3dbba24ba[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 20:20:23 2021 +0000

    More linux compile fixes after code tidy.

[33mcommit d172b2ec754264fe8d24267c054182f3423127d3[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 20:09:09 2021 +0000

    Fix linux compile error

[33mcommit fe7fc4630100dd059aebf26489c20c2a0498e4f6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 19:53:48 2021 +0000

    Better error messages and more udp code cleanup

[33mcommit 33b55f8bbf85f2959062ee1b2afa5a9a18521149[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 18:52:00 2021 +0000

    Remove duplicate code from DataReceived() functions

[33mcommit 7fd165aefd4002b6177c82df158d2454d1cb2139[m
Merge: 45c3adb 934edbe
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 16:07:08 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 45c3adb958544bfc94bd04b360ad602f280b63ec[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 16:06:55 2021 +0000

    More retransmit fixes

[33mcommit 934edbea925a8e97e27691e3e53fee063df8526d[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Thu Feb 4 15:44:28 2021 +0000

    fixing typo in debug message

[33mcommit 215c8863e9acb9668a62f870297a6d69b9600ba6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 13:33:58 2021 +0000

    More debugging for retransmit

[33mcommit 4d0ef4fdc035838dfaf9259569169af6c335b2d8[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 13:12:08 2021 +0000

    Implement single packet retransmit

[33mcommit 73d99023d77e894eb55470c8a2c69787046d0b78[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 10:27:30 2021 +0000

    Fix for ic7610 wf length and partial sound device selection

[33mcommit c9fdda4f93b53584a9e602b89b27bbc39c5cf41d[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 3 22:07:12 2021 -0800

    removed accidental merge garbage. oops.

[33mcommit a1a1a03580d4215adc315f222ec18832a082bd1e[m
Merge: fff8a6b f18d0f3
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 3 22:04:50 2021 -0800

    Merge of local changes, including rejection of brodcast echos.
    Merge branch 'lan-alpha' of gitlab.com:eliggett/wfview into lan-alpha

[33mcommit fff8a6b6a359e9aa2d4f4a03bf9b2f1b4e865ec4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 3 22:00:13 2021 -0800

    Fixed situation where broadcast commands from wfview that were echo'd
    back to wfview were parsed. Minor change to udphandler.cpp to remove
    _sleep() calls (they can be later replaced with either qt's threading
    library sleep call, or the unix-native usleep/msleep calls, if windows
    has those. Also did a minor thing with a type conversion in udphandler.

[33mcommit f18d0f3e2b7f1654d64ae9845d804f1dce4e56e6[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Thu Feb 4 00:01:05 2021 +0000

    Remove temporary ic9700 testing!

[33mcommit 65f5486c5b6af259204755463fd4631ba834cdab[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 23:58:11 2021 +0000

    Attempt to call rig destructor on exit, and remove windows specfic _sleep()

[33mcommit 0d75363214afbd1fef983d4cb45746015117f48e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 23:36:27 2021 +0000

    Remove first 12 bytes from LAN spectrum.

[33mcommit 41727a30ba520d88e8fac0c98fdccc0d42a0fe5e[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 23:14:01 2021 +0000

    Add #ifdef around pseudo terminal code so Windows compiles.

[33mcommit 399b334407355721c38e497cc32ad803595301d0[m
Merge: 5566518 3f5e7e5
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 23:08:30 2021 +0000

    Merge branch 'lan-alpha' of https://gitlab.com/eliggett/wfview into lan-alpha

[33mcommit 3f5e7e53cef44bcbc22696d3a8e023e360cef20e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Feb 3 22:10:37 2021 +0000

    Added camel case around #include <QtMultimedia/QAudioOutput>, so that linux can compile. Please test in windows to make sure I haven't broken it.

[33mcommit 5566518d44e11aff02e45d2a23c96d511383fced[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 20:10:21 2021 +0000

    Remove hard-coded local IP address!

[33mcommit 6e074375e76cb167c8b8548dbe9c0bb40fe1b5be[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 20:00:40 2021 +0000

    Add udphandler classes

[33mcommit 4bf23cea437cc19d724043c6c568a4b4a0ab8f40[m
Author: Phil Taylor <phil@m0vse.uk>
Date:   Wed Feb 3 19:36:35 2021 +0000

    First support for LAN connection
    
    Auto detection still not working so need to hard-code C-IV address, hope to fix this very soon!

[33mcommit 5acdc233226dfb9f5c69b1fbad7fc7f379f73303[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 3 19:07:25 2021 +0000

    Update INSTALL.md

[33mcommit 0c78472629a87b3bc0cd9c42b77d9a7624bd3991[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 3 18:57:22 2021 +0000

    Update INSTALL.md

[33mcommit 3acb02db3b4d98a8ecbc7823af31fc2b6661bfc2[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 3 18:48:31 2021 +0000

    Update CONTRIBUTING.md

[33mcommit 94f0034a42dc07cccdbbe1c71690a05055322d3f[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 3 18:48:16 2021 +0000

    Update CONTRIBUTING.md

[33mcommit 9133934c2ab26929b5f6b71dd814c26bd7de51e2[m
Author: roeland jansen <roeland.jansen69@gmail.com>
Date:   Wed Feb 3 18:47:15 2021 +0000

    Add CONTRIBUTING

[33mcommit 65f9a70ee9c4f6d5849b2d664fa1f373e1b6cc04[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Feb 2 01:05:59 2021 -0800

    Enhanced code that finds radio serial ports. Added error reporting to
    the user if serial port isn't opening. Added command-line arguments for
    serial port (example: "-p /dev/ttyUSB0" or "--port /dev/ttyUSB0").
    Cleaned up some of the debug messages. wfview also will now tell the
    user what radio was found in the status bar of the main window.
    
    Added the begininning of code to determine the waveform type (center or
    fixed). rigcommander now knows which is which. The next step is to emit
    a signal when this changes, which then causes the main window to check
    or uncheck the box (without triggering any other code).
    
    Added some TODO remarks on mode (DV, DD).
    
    Added some command line arguments that are not used at this time for --
    host and --civ.

[33mcommit add5c0d3b2475ed8fc41c0d630f552a5e74baa36[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jan 30 00:44:36 2021 -0800

    On startup, creates a constant queue searching for rig id until one is
    found.

[33mcommit d32ecde00168cdd5b7ba86ac7916a93ce694defd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jan 29 22:09:09 2021 -0800

    Added automatic rigID and automatic CIV finding. Also fixed frequency
    data to include more digits. Cleaned up some startup code, made some
    worse too probably. Tested with IC-9700 USB connection.

[33mcommit 655ea0ac0e2734b7dbde493f8dbd7e2d06ca986e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Jan 24 16:16:38 2021 -0800

    Added support for various spectrum sizes and rig capability support.
    wfview has been tested with the following Icom radios over USB port:
    IC-7300, IC-7610, IC-7851 (and IC-7850), and IC-9700. It likely works
    fine with the IC-705 as well. At this time, the rig's CIV address must
    be changed in the preference file to indicate the rig you are using, and
    this must be in integer, not hex.

[33mcommit eee5f6e31f699eeba08a45de6bec516569a282ec[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Oct 16 23:37:52 2020 +0000

    Update INSTALL.md

[33mcommit dc2f085535abb4473fe735e76cd0b7c5ffc74ffe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Apr 23 15:22:23 2020 -0700

    Changed scroll speed for AF and RF gain sliders.

[33mcommit 39dd6a04410cc464f92328214712ce9d1a1b6625[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 21 12:35:44 2020 -0700

    reversed scroll direction on the waterfall and plot scrolling.

[33mcommit e13c9bdf96db4c33863ba95d5ee74aa69c89cf78[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 20 21:23:34 2020 -0700

    Added control modifier to scroll on plot, causes faster changes.

[33mcommit 03a3ca423d3d508853fccd0a204e902aea99a4e9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 20 21:10:20 2020 -0700

    Added preliminary mouse scrolling support for the waterfall and plot
    area. As it stands, scrolling basically rotates the tuning knob. No
    modifier keys are supported yet, but we'll get there!

[33mcommit f6d11f76bcd36aa0d56f8bbe5db35d04c97473f3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Apr 20 20:45:25 2020 -0700

    Fixed minor bug in command 05. It is now parsed as a frequency instead
    of a mode.

[33mcommit 06b36ee79b7460021986d2694d87425c7cf7ef24[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 7 21:33:15 2020 +0000

    Changed install.md to reflect that we do not need to use the qmake --no-depends flag anymore -- because we removed the header from the UI file and this seems to work fine in Mint 19.x, Ubuntu 19, and Debian 10.

[33mcommit 9cd417b5972b27fd3dd2732ad0237d6c7f2b8c24[m
Merge: 2f69d85 9d12c21
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 7 14:13:22 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 2f69d85c9991afa593fec65299f9b2fdfd5d9e17[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Apr 7 13:15:21 2020 -0700

    Fixed issue with newer qmake.

[33mcommit 9d12c21e7f701da172702d9a72e820f2eeeaba87[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 5 06:39:30 2020 +0000

    Update INSTALL.md

[33mcommit 505cd8dcac668940597894c4d934ec1976db9ae3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Apr 5 00:50:47 2020 +0000

    Update INSTALL.md

[33mcommit fa7fa3a079afc51b8dd062a2e2fd257a9f1da2f6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 4 17:38:13 2020 -0700

    Moved exit button to make it more obvious.

[33mcommit 4e65a9bbb9b7b234ab436683b833c9a8ad329ebe[m
Merge: dd481af b53bdf0
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 4 16:52:00 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit dd481afbbc17b658aaebd815ca7e7e8c3d2406be[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 4 16:51:46 2020 -0700

    Added window position restore as a setting, worked around qt bug to
    retain window focus during resize.

[33mcommit 77428bd26c4cf856dd044628317e1c7a259e0f82[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Apr 4 15:39:59 2020 -0700

    Added splitter percent to saved settings, removed unitless X-axis from
    waterfall.

[33mcommit b53bdf07188bedc9e04fab4551784a2d17de9f89[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Apr 3 23:23:10 2020 +0000

    Update INSTALL.md

[33mcommit 86e4bf01a3f42938319ad18c9d2d9c138bfe6e4a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Mar 30 14:09:52 2020 -0700

    Added suport for for different rig baud rates via the [Radio] preference
    SerialPortBaud. Left unset, it defaults to 115200. Added support for
    status bar text with memory presets.

[33mcommit 907e7e4f0233f21cada64516589d001aa28aece1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 28 16:58:25 2020 +0000

    Update README.md

[33mcommit ed9182dfeb0bea5a53568222f1bb9a6d7ad3d7e4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 28 16:39:28 2020 +0000

    Update README.md

[33mcommit 8373903438013554ab8f3309d73d3da3956a25b9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 28 00:55:20 2020 +0000

    Update INSTALL.md

[33mcommit d0fd8b3bf8391943e4a69d4f10be25901e61ae6f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 28 00:53:04 2020 +0000

    Update INSTALL.md

[33mcommit 90f2350da3c8ff69d420f40da764ef38dc235568[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 27 16:34:32 2020 -0700

    Added support for QCustomPlot >= 2.0. Yet to be tested.

[33mcommit acfb1556bf18ddf6851c974c064a50815ea5ac13[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Mar 23 14:58:31 2020 -0700

    Automatic symlink of /tmp/rig to the pseudo-terminal device. Also, we
    now supply preference files for flrig to simplify setting up integration
    to fldigi.

[33mcommit 6a1f53887b2c4aa848a481c53111b225e4dfea6b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 17 15:15:01 2020 -0700

    Added pageup and pagedown keystrokes.

[33mcommit 2c104f9950275299e997c08121256ebd5d3f8b1c[m
Merge: ba558b8 38f991f
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 15 18:03:41 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit ba558b8e6f346b23eb76921523091a06f8d1709b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 15 18:03:35 2020 -0700

    about box change

[33mcommit 38f991f6846ba0d985964bef8e6a00b01c5f5c89[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 15 05:24:57 2020 +0000

    Update README.md

[33mcommit 70ba80a1fd6184e021fbc41f16eec71d3c6570cd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Mar 15 02:56:34 2020 +0000

    Update README.md

[33mcommit d79f6e2295a296b316c4b3e25893fcca70b06bd7[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Mar 14 19:50:57 2020 -0700

    Added install directions.

[33mcommit 48fcf9c44c98df465e5ea0f86c0f07b3adb0a657[m
Merge: 5df1f6e ea9c152
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 19:43:33 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 5df1f6e5b0b17599c0f7e4ea69fa076ad8f77261[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 19:43:12 2020 -0700

    minor typo correction

[33mcommit ea9c15276d64b74b04cf5623484e14a61d0f5eac[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 20:47:53 2020 +0000

    Update README.md TODO list

[33mcommit d6be430e06da2477bd507eff4d3eebe8069c5b65[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Mar 12 20:13:29 2020 -0700

    Minor change to install script.

[33mcommit fb30ed8cd8ea8d2060862172c4e7bba140ebb958[m
Merge: 797798d 4b35cc4
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Mar 12 20:06:41 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 797798d3b62c33d6d1b6b44ea6ebdfd971da5650[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Mar 12 20:06:33 2020 -0700

    Added install script and various enhancements to wfview.pro to make
    installing easier, including an icon and a desktop launcher.

[33mcommit 4b35cc459277a6949854a4dd70828078088cbc87[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 00:55:57 2020 +0000

    Update README.md

[33mcommit 234ebf99f6788e368f183c07b1805a591fd99a57[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 00:51:38 2020 +0000

    Update README.md

[33mcommit 6df768f467daad61a10ba0f68b1768ebf6ff6aa4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Mar 13 00:49:24 2020 +0000

    Update README.md

[33mcommit 32c930788b59c75a59330cf949dad7d5746b4e83[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 20:58:31 2020 -0700

    Minor updates, copyright, comments.

[33mcommit e8501467895e39d6376c2b24a49946d85509b3ca[m
Merge: 125c0af 328ac9a
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 17:45:07 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 125c0aff62ee135714b6a086d11a3ef77cee78b9[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 17:44:47 2020 -0700

    modified about box

[33mcommit 328ac9a2bb2c8a5c0c0ecf8ab7363bedb4d8187b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 23:47:30 2020 +0000

    Update README.md

[33mcommit ee07db50023a53a4ec2cec4a07a9cb3d40b53e4a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 23:42:54 2020 +0000

    Update README.md

[33mcommit ea95fe469cd243989d4300cfea4176388cf9e99a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 23:42:09 2020 +0000

    Update README.md

[33mcommit fb7c37f513720eba0eb4a0808e727cce82ad5991[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 23:41:51 2020 +0000

    Update README.md

[33mcommit 86d522a3adbff3a18b88b3e15c17b1bd667f97cb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Mar 11 23:40:39 2020 +0000

    Update README.md

[33mcommit e79c5de55032a7f1045cb5b96f2c4c39838fddaf[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 10 11:08:36 2020 -0700

    Added control+q for quit.

[33mcommit 54fd7fff86f717a789ac6b20b174cfa35e39ee1f[m
Merge: 0bdba44 5772036
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 10 10:47:58 2020 -0700

    Merge branch 'master' of gitlab.com:eliggett/wfview

[33mcommit 0bdba4494c8bde05a9f1dea6ad99b2005572f708[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Mar 10 10:17:39 2020 -0700

    Minor change to wording.

[33mcommit 5772036b2a9932190005ef236540fcfbcb5a41cd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 29 02:33:15 2020 +0000

    Update README.md

[33mcommit 828de59080b76012137aae3531c2120f3aeb7823[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jan 4 17:12:35 2020 -0800

    Changed program name and various file locations.

[33mcommit 7510775350bf4b247a0bfd6e5fae56dbca0c88b0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jan 3 23:08:06 2020 +0000

    Update README.md

[33mcommit 7c189e8e81a8bcf0ec8a6f494fa4df752e8f8380[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jan 3 03:13:13 2020 +0000

    Update README.md

[33mcommit 8951b37ccff50ccd94b9ccf3c4244ecec8886560[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 26 08:47:57 2019 +0000

    Updated README.md

[33mcommit 3414ef46ae3a7ffd62992f655a7a480ec8f5d0f6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 26 00:27:14 2019 -0800

    Removed extra debug calls and status text updates.

[33mcommit d6e5fdb362ed65485cdfed04b6f5d4c1338f7f22[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Feb 9 23:13:18 2019 -0800

    Fixed PTT and pttEnable preference.

[33mcommit 056d6444deeb439fdbaa7ebdd078b1b9b727df9e[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 8 16:19:36 2019 -0800

    Added proper application and window name.

[33mcommit e0bc2912b016fbcf4f4b82ee235b260aa8f603bb[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 8 16:05:45 2019 -0800

    Added custom stylesheet path to preferences.

[33mcommit 8d131208e258200c4a582d1c67f1f24d05d7bdae[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Feb 7 10:15:10 2019 -0800

    Added "F" and "M" shortcuts to speak frequency and mode.

[33mcommit 73700645004f6f7903a675e45e0ad5170c2a4535[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Feb 5 16:11:25 2019 -0800

    Control and shift modifiers now accepted for tuning step-size modifier

[33mcommit 8a530588d4411cdaa4c1f75eab401cee45661b90[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Feb 4 10:32:16 2019 -0800

    Added control key modifier to +/- for course tuning.

[33mcommit 0ecc2b658bcd9cd845eb29a671fcaa5d4389f7e5[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 3 22:26:47 2019 -0800

    Linker opt flags tuned.

[33mcommit dce977db76c0b91218703e1d0c58c05de7ded61c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 3 21:51:30 2019 -0800

    Added +/- key support for tuning. Added additional compiler flags to
    lower executable size.

[33mcommit 5dfeebdbaea203dc7c1eb119b731a344230f7aa2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Feb 3 14:09:06 2019 -0800

    Changed F-key to mode functions to call activated slot. Clarified
    comments in activated slot.

[33mcommit 279c8e1a918752eed45847369f66474c1d5a8e0a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 1 13:51:28 2019 -0800

    Enhanced key commands

[33mcommit 47b7a99490e870f1d03c41a10a1a88cb2e85c0d0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Feb 1 12:21:54 2019 -0800

    Added control key shortcuts and function key F12 to cause the 7300 to
    speak current status.

[33mcommit a361b330d34c3b2768b7afb6ce096d9dfedd8f7c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jan 28 15:46:14 2019 -0800

    Added catch for slash key, does nothing so far, plan to cycle modes.

[33mcommit f2fc2fbcadbf0aa35063395914be01131f27ccb3[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jan 28 15:36:17 2019 -0800

    Added F5-F10 buttons for mode switching, seems to work. Added debug
    detect to disable UI elements for release builds.

[33mcommit 3a37032a9a59af5c463775859fb6d3d85951fa7c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jan 28 10:06:17 2019 -0800

     Minor changes

[33mcommit 2f16c6c2bb468a52c936f7cea81913598de8bb14[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Jan 23 17:19:35 2019 -0800

    UI cleanup and PTT, F5 functions.

[33mcommit 0da6babb7a6f106194fe09c03c3fe96eed7cbb38[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jan 14 14:30:28 2019 -0800

    Fixed comparison between char and int.

[33mcommit 4c056fe61b5df3272017bc912567f110195fb923[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jan 10 16:10:17 2019 -0800

    Added more freq and mode parsing.

[33mcommit a3f4dd9ec4bc99e0a72102a7d234dedf32c2ef9f[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jan 10 12:33:33 2019 -0800

    Removed whitepace so as to cause an extra update

[33mcommit 016e91d36761d9d2ecce186b8b36633723d33ef1[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Jan 10 12:31:04 2019 -0800

    pseudo-terminal speed increase. Added E1 and E0 return procesing to
    rigcommander. Added gitignore file.

[33mcommit f30d300fe669c9572288a5abfc144d13e155461a[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Jan 9 22:44:48 2019 -0800

    Added pseudo-terminal capability. TODO: Make sure wfview will parse 0xE0
    data.

[33mcommit 815f8965f0ffe792714238647384ce5e7afce6dd[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 20 11:22:52 2018 -0800

    Additional ATU status fix, now queries on start.

[33mcommit 9e50b2e037ff1169be6eae096780c84d81a3b9e6[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Dec 20 10:56:54 2018 -0800

    Fixed ATU status return.

[33mcommit c6f73fee7a62f014a77e12adf7b62ab0f5666e32[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Wed Dec 19 12:31:44 2018 -0800

    More ATU modifications

[33mcommit 55b0e15afd3188bc6c9cf271988a3794afcf6cff[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Dec 18 14:34:04 2018 -0800

    Squashed CIV bug. Added preliminary scope mode functions (not working
    yet).

[33mcommit 24a51074d61634fd2f179be0ec709da57c8a68b7[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Dec 2 23:16:08 2018 -0800

    Added additional mode command return support because we seem to
    sometimes get the other formats.

[33mcommit 62ae4abe5b13973b317d4ef7f4f6d69209f5b2bc[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 22:31:20 2018 -0800

    Fixed bug with default preference initialization. Removed mode label.

[33mcommit 5610f44dcd704712d44cbade5fd58a28fc607fb8[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 22:20:27 2018 -0800

    More line edit focus cleanup

[33mcommit c946410fe179a3bd8f4a7d83ba5c496063847bfe[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 22:19:23 2018 -0800

    Fixed line edit focus for frequency input with star command

[33mcommit 091872705845ce4fdb3d22a53e435034446ae5da[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 14:32:59 2018 -0800

    Cleanup

[33mcommit b5715d6024b2ae80f13f712b9b43db3d7f55d5ae[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 13:57:15 2018 -0800

    Cleaned up some debug output

[33mcommit 0cee949dcf9050e8c12c2f4df9998432c17b6042[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Thu Nov 29 13:41:42 2018 -0800

    Added automatic IC-7300 serial port search

[33mcommit 5f3561020787d11af9ecbbb9b61855e95beec98b[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Nov 27 22:02:37 2018 -0800

    Added shortcuts F1-F4, F11, Asterisk.

[33mcommit b432482e0c282ca13721b76ea449c835fbe7a9e4[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Nov 26 23:10:23 2018 -0800

    Fixed annoying data mode bug.

[33mcommit 973dec0b25cdae3860e85af893367dfeb78c96af[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 24 22:21:36 2018 -0800

    Now using preferences, read and write. Current settings are not written
    at this time.

[33mcommit 59c5113f436cffaf6b0aa43dd521fcfa5acce27c[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Nov 24 00:10:05 2018 -0800

    Added preference file capability, additional radio identity functions.

[33mcommit da4b526f815f81f744dc276e5fed58e39854bd47[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Nov 19 23:42:34 2018 -0800

    Added exit button and rig ID (was not useful)

[33mcommit 908e92883800b979543779e1313fb49ca2a76545[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sun Nov 18 00:01:20 2018 -0800

    Fixed AF/RF/SQL level bug

[33mcommit ab477db944d6d59b030b51a2603a3152a1215fa0[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 16 23:13:59 2018 -0800

    Added some ATU structure

[33mcommit 36b10d41001e6cce7d8c6208524a2473adb96335[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 16 22:19:44 2018 -0800

    Added AF and RF (RX) gain and ATU basics

[33mcommit 54881ddd513132ae68ac85c8dd5a1966ef1c1e36[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Nov 16 14:08:21 2018 -0800

    Squashed a number of bugs and added the band stacking register
    functions.

[33mcommit 208e2017eb0a60cc14c6a0f9a722b63c69990320[m
Author: Elliott Liggett <eliggett@crasher>
Date:   Wed Nov 7 15:54:03 2018 -0800

    Numerious modifications

[33mcommit 3f7edad0748b274db56a6f9ac5fae6b6016f9a14[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Tue Jul 3 17:20:03 2018 -0700

    Implimented tuning knob, enhanced communication handling.

[33mcommit 0b8de1a08b85d8384ce4920be0f1be84bf7b2e24[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Mon Jun 25 00:34:33 2018 -0700

    Freq knob and beginnings of com port setup.

[33mcommit 989588f9e167bd68069766e6c55392918910d9fa[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Sat Jun 23 00:31:41 2018 -0700

    Added command que to main window. Good for commands that need to happen
    in order and not immediately. Consider setting quicker timer.

[33mcommit 98f1538cca89c54fdacfdf65634508edaf0934e1[m
Merge: 85f46b8 0c65ca7
Author: Elliott Liggett <eliggett@crasher>
Date:   Fri Jun 22 16:34:19 2018 -0700

    Merge branch 'master' of https://github.com/eliggett/wfview

[33mcommit 85f46b816e2363cb46579a82fae3996267abb4c4[m
Author: Elliott Liggett <eliggett@crasher>
Date:   Fri Jun 22 16:33:25 2018 -0700

    Added missing file

[33mcommit 1fd6ce531084d4e1ee99ed9bc6cb548b31aa1fe2[m
Author: Elliott Liggett <kilocharlie8@gmail.com>
Date:   Fri Jun 22 16:31:52 2018 -0700

    Finally a good command parsing technique yeilds loads more usable data.
    Dark mode is in too!

[33mcommit 0c65ca7932c6af014b8a98bbf1955243f25c1aef[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Thu Jun 21 00:20:48 2018 -0700

    typo.

[33mcommit b2d8ea6f4e3fecf7434642ac26911697f23629d5[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Thu Jun 21 00:18:37 2018 -0700

    more readme updates

[33mcommit 413a9849d30c372835466b644e5f10565fc537ee[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Thu Jun 21 00:18:00 2018 -0700

    further readme updates

[33mcommit 4fe2d8a871aeaef0614875ae41294dfd54cc4f71[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Thu Jun 21 00:12:53 2018 -0700

    updates to readme

[33mcommit 8b4745801302d1401492d239894cf01dd5dcd24e[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Wed Jun 20 19:33:27 2018 -0700

    updated readme, again

[33mcommit cba80df0c5d1a3ebbc56c8e55b6fa2e1462ea355[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Wed Jun 20 19:10:18 2018 -0700

    update to readme

[33mcommit 6ff0d40cb60344dfa35103ea2175dc653470d92c[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Wed Jun 20 19:09:48 2018 -0700

    updates to readme

[33mcommit 28a592e9cbaa1ce9cc6b1aeaf4f8a6456895c780[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Wed Jun 20 19:06:15 2018 -0700

    Added README

[33mcommit 8416633f72ea38dfb36c31ec6fa778527afbec2f[m
Author: Elliott Liggett <38051311+eliggett@users.noreply.github.com>
Date:   Wed Jun 20 18:45:50 2018 -0700

    Added LICENSE

[33mcommit 24c8ec5a09adbc32b074f373190caf1bde82c6d7[m
Author: Elliott Liggett <eliggett@jpl.nasa.gov>
Date:   Tue Jun 19 23:45:56 2018 -0700

    Fixed mode select bug. Added some preliminary hooks for changing the
    theme to dark.

[33mcommit 4d86ab11cccdb44bd9300cc6cc0b0b1a3da047ea[m
Author: Elliott Liggett <eliggett@jpl.nasa.gov>
Date:   Tue Jun 19 12:58:52 2018 -0700

    Initial commit.
